﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dashboard_Microtech.Models
{
    public class AnalyticsAgent
    {
        public string agent { get; set; }
        public List<Customer> customers = new List<Customer>();
        public List<Product> products = new List<Product>();
        public List<Customer> customersOrdered = new List<Customer>();
        public string causal { get; set; }
        public int orderCustomers = 0;
        public int preCustomers = 0;
        public double orderValue = 0;
        public double preValue = 0;
        public List<int> dates = new List<int>();

    }

    public class Customer
    {
        public string name { get; set; }
        public string surname { get; set; }
        public int delivery { get; set; }
        public int invoice { get; set; }
        public List<Order> orders = new List<Order>();
    }

    public class Order
    {
        public string causal { get; set; }
        public DateTime date { get; set; }
        public int year { get; set; }
        public int documentNumber { get; set; }
        public double orderValue = 0;
        public List<Product> products = new List<Product>();
    }
    public class Product
    {
        public string sku { get; set; }
        public double net { get; set; }
        public double qty { get; set; }
        public double cumage { get; set; }
        public double deliveredValue { get; set; }
        public int year { get; set; }
    }
}
