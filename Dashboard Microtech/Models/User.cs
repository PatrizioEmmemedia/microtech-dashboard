﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dashboard_Microtech.Models
{
    public class User
    {
        public int PK { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string Indirizzo { get; set; }
        public string CAP { get; set; }
        public string Nazione { get; set; }
        public string Regione { get; set; }
        public string Provincia { get; set; }
        public string Comune { get; set; }
        public string Telefono { get; set; }
        public string Cellulare { get; set; }
        public string EMail { get; set; }
        public string VAT { get; set; }
        public string Codice_Fiscale { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Ragione_Sociale { get; set; }
        
    }
}
