﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microtech_Core.Database.Models;
using Microtech_Core.WebServices.Services;
using Microtech_Core.WebServices.Services.Pojo;
using Newtonsoft.Json;
using Microsoft_Core.Database;
using Microtech_Core.Database;
using Microsoft_Core.Database.Models;
using System.Data.SqlClient;
using Microtech_Core.Database.Functions;
using System.Globalization;
using Microtech_Core.WebServices.Services;
using Microtech_Core.WebServices.Services.Misc.Utils;
using Microtech_Core.WebServices.Services.Pojo;
using Newtonsoft.Json;
namespace Dashboard_Microtech.Controllers
{
    public class ArchiveController : Controller
    {
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("Logged") == null)
            {
                return View("../Login/Index");
            }
            return View();
        }
        public IActionResult SearchOrderClients()
        {
            if (HttpContext.Session.GetString("Logged") == null)
            {
                return View("../Login/Index");
            }
            return View(JsonConvert.DeserializeObject<Agent>(HttpContext.Session.GetString("Logged")));
        }
        [HttpGet]
        public IActionResult ShowResultList([FromQuery]string dateBeg, [FromQuery]string dateEnd)
        {
            if (HttpContext.Session.GetString("Logged") == null)
            {
                return View("../Login/Index");
            }

            int itemPerPage = 10;
            string cli = HttpContext.Request.Query["cli"];
            string pre = HttpContext.Request.Query["pref"];
            string ido = HttpContext.Request.Query["ido"];
            DateTime Beg = default(DateTime);
            DateTime End = default(DateTime);
            if (dateBeg != null)
            {
                Beg = DateTime.ParseExact(dateBeg, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            if(dateEnd != null)
            {  
                End = DateTime.ParseExact(dateEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            
            
            //art=70102&cli=8305&dto=20180716
            var getLsOrd = new GetLsOrdService(cli, pre, ido, "http://2.229.69.115:8015/www/dacgi/WSGetLsOrd.pgm").GetLsOrdAsync();
            Customer customer = Utils.GetCustomerNameByCode(getLsOrd[0].customerSped.ToString());

            List<GetLsOrd> filteredList = null;
            if (getLsOrd != null)
            {
                if(dateBeg != null && dateEnd == null)
                {
                    filteredList = getLsOrd.Where(i => DateTime.ParseExact(i.getDate(), "dd/MM/yyyy", CultureInfo.InvariantCulture) > Beg).ToList();
                }
                if(dateBeg == null && dateEnd != null)
                {
                    filteredList = getLsOrd.Where(i => DateTime.ParseExact(i.getDate(), "dd/MM/yyyy", CultureInfo.InvariantCulture) < End).ToList();
                }
                if(dateBeg != null && dateEnd != null)
                {
                    filteredList = getLsOrd.Where(i => DateTime.ParseExact(i.getDate(), "dd/MM/yyyy", CultureInfo.InvariantCulture) >= Beg && DateTime.ParseExact(i.getDate(), "dd/MM/yyyy", CultureInfo.InvariantCulture) <= End).ToList();
                }

                if(dateBeg == null && dateEnd == null)
                {
                    filteredList = getLsOrd;
                }
                filteredList = filteredList.OrderBy(x => int.Parse(x.ordDate)).ToList();

                filteredList.Reverse();

                //System.Diagnostics.Debug.WriteLine("DEBUG: " + JsonConvert.SerializeObject(filteredList));
            }

            foreach(var ord in getLsOrd)
            {
                ord.customerCleanName = customer.CompanyName;
            }

            if(cli != "undefined")
            {
                var getLsVen = new GetLSVenService(cli);

                var listVen = getLsVen.getDocAsync();
                if(listVen != null)
                {
                    foreach (var Ven in listVen)
                    {
                        if (Ven.desc != "FA Fattura Acconto" && Ven.desc != "NA NOTA DI ACCREDITO")
                        {
                            if(Ven.ordNumber != "")
                            {
                                var single = filteredList.SingleOrDefault(s => s.ordNum == Ven.ordNumber.Remove(0, 1));
                                if (single != null)
                                {
                                    single.VenDocs.Add(Ven);
                                }
                            }
                            
                        }

                    }
                }
                

            }


            return View(model:filteredList);
        }
        [HttpGet]
        public IActionResult ShowResultOrder()
        {
            if (HttpContext.Session.GetString("Logged") == null)
            {
                return View("../Login/Index");
            }
            string pre = HttpContext.Request.Query["pre"];
            pre = "000";
            string ordId = HttpContext.Request.Query["ordId"];
            string date = HttpContext.Request.Query["date"];

            var getOrder = new GetOrderService(pre, date, ordId, "http://2.229.69.115:8015/www/dacgi/WSGetOrd.pgm").GetOrdAsync();
            for(int i = 0; i < getOrder.ordini.Length; i++)
            {
                Product product = GetProduct(getOrder.ordini[i].idArt);
                getOrder.ordini[i].productName = product.Name;
                getOrder.ordini[i].sku = product.SKU;

            }
            return View(model:getOrder);
            
        }

        public string GetOrderAsync([FromQuery]string pre, [FromQuery]string ordId, [FromQuery]string date)
        {
            pre = "000";
            var getOrder = new GetOrderService(pre, date, ordId, "http://2.229.69.115:8015/www/dacgi/WSGetOrd.pgm").GetOrdAsync();
            for(int i = 0; i < getOrder.ordini.Length; i++)
            {
                Product product = GetProduct(getOrder.ordini[i].idArt);
                getOrder.ordini[i].productName = product.Name;
                getOrder.ordini[i].sku = product.SKU;
            }

            return JsonConvert.SerializeObject(getOrder);
        }

        public IActionResult ListByProduct([FromQuery]string cli, [FromQuery]string art, [FromQuery]string dateBeg, [FromQuery]string dateEnd)
        {

            var getLsOrd = new GetLSOrdProdService(cli, art, "http://2.229.69.115:8015/www/dacgi/WSGOrClArt.pgm").GetLSOrdAsync();

            List<GetLsOrd> filteredList = null;
            DateTime Beg = default(DateTime);
            DateTime End = default(DateTime);

            if (dateBeg != null)
            {
                Beg = DateTime.ParseExact(dateBeg, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            if (dateEnd != null)
            {
                End = DateTime.ParseExact(dateEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            if (getLsOrd != null)
            {
                if (dateBeg != null && dateEnd == null)
                {
                    filteredList = getLsOrd.Where(i => DateTime.ParseExact(i.getDate(), "dd/MM/yyyy", CultureInfo.InvariantCulture) > Beg).ToList();
                }
                if (dateBeg == null && dateEnd != null)
                {
                    filteredList = getLsOrd.Where(i => DateTime.ParseExact(i.getDate(), "dd/MM/yyyy", CultureInfo.InvariantCulture) < End).ToList();
                }
                if (dateBeg != null && dateEnd != null)
                {
                    filteredList = getLsOrd.Where(i => DateTime.ParseExact(i.getDate(), "dd/MM/yyyy", CultureInfo.InvariantCulture) >= Beg && DateTime.ParseExact(i.getDate(), "dd/MM/yyyy", CultureInfo.InvariantCulture) <= End).ToList();
                }

                if (dateBeg == null && dateEnd == null)
                {
                    filteredList = getLsOrd;
                }

                filteredList = filteredList.OrderBy(x => int.Parse(x.ordDate)).ToList();

                filteredList.Reverse();
            }
            
            return View("ShowResultList", model: filteredList);
        }

        public IActionResult ListSearch()
        {
            return View(model: JsonConvert.DeserializeObject<Agent>(HttpContext.Session.GetString("Logged")));
        }

        public IActionResult ListDetails([FromQuery]string cli)
        {
            SqlService service = new SqlService();
            service.init();

            SqlDataReader reader = service.query("SELECT Listini.*, Product.Name, Product.SKU FROM Listini INNER JOIN Product ON Listini.AS400_ID = " + cli + " AND Product.AS400_ID = Listini.ProductKey ORDER BY PK");

            List <Listino> list = new List<Listino>();

            while (reader.Read())
            {
                Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
                var product = SQLMapping.MapListino(column);
                list.Add(product);
            }
            System.Diagnostics.Debug.WriteLine(JsonConvert.SerializeObject(list));
            return View("ShowDepartList", model:list);
        }
        public async Task<String> ListDetailsAsync([FromQuery] string cli)
        {
            SqlService service = new SqlService();
            service.init();

            SqlDataReader reader = service.query("SELECT Listini.*, Product.Name, Product.SKU FROM Listini INNER JOIN Product ON Listini.AS400_ID = " + cli + " AND Product.AS400_ID = Listini.ProductKey ORDER BY PK");
            List<Listino> list = new List<Listino>();
            while (reader.Read())
            {
                Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
                var product = SQLMapping.MapListino(column);
                list.Add(product);
            }

            return JsonConvert.SerializeObject(list);
        }
        public Product GetProduct(string idArt)
        {
            SqlService service = new SqlService();
            service.init();
            
            SqlDataReader reader = service.query("SELECT * FROM Product WHERE AS400_ID = '" + idArt + "'");

            Product product = null;

            while (reader.Read())
            {
                Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
                product = SQLMapping.MapProduct(column);
                
            }
            service.close();
            return product;
        }

        public bool insertList([FromBody]List<InsertListModel> body)
        {
            foreach(var list in body)
            {
                
                SqlService service = new SqlService();

                service.init();

                bool response = service.insert("INSERT INTO Listini (ProductKey, Price, CustomerKey, AS400_ID, Date, Agent) VALUES ('" + list.ProductKey + "', '" + list.Price + "', '" + list.CustomerKey + "', '" + list.AS400_ID + "', '" + DateTime.Now + "', '" + list.Agent + "')");


                service.close();
            }
            
            return false;
        }

        public IActionResult ShowListByAgent()
        {
            var agent = JsonConvert.DeserializeObject<Agent>(HttpContext.Session.GetString("Logged"));

            SqlService sql = new SqlService();
            sql.init();

            SqlDataReader reader = sql.query("SELECT Customer.CompanyName, Customer.PK, Listini.Price, Listini.Date, Listini.CustomerKey, Listini.Agent, Customer.AS400_CODE, Customer.AS400_ID FROM Listini INNER JOIN Customer ON Customer.PK = Listini.CustomerKey WHERE Listini.Agent = '" + agent.Codice_ACG + "'");
            List<ListByAgent> listini = new List<ListByAgent>();
            while (reader.Read())
            {
                Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
                ListByAgent listino = SQLMapping.MapList(column);
                listini.Add(listino);
            }

            List<Dictionary<String, object>> model = new List<Dictionary<String, object>>();
            List<SelectResult> lists = listini.GroupBy(x => x.PK).Select(list => new SelectResult {
                PK = list.First().PK,
                value = list.Sum(c => c.Price),
                companyName = list.First().CompanyName,
                date = list.First().Date,
                code = list.First().AS400_CODE,
                id = list.First().AS400_ID
            }).ToList().OrderBy(x => x.date).ToList();

            sql.close();
            lists.Reverse();
            return View(model: lists);
        }

        public IActionResult ShowVenDetail([FromQuery]string doc, [FromQuery]string aad)
        {
            GetVenService service = new GetVenService(doc, aad);

            var VenDetail = service.getDocAsync();

            return View(model: VenDetail);
        }
      
    }
}