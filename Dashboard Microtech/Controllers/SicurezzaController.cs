﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microtech_Core.Database.Models;
using Newtonsoft.Json;
using Microtech_Core.Database;
using Microtech_Core.Database.Functions;

namespace Dashboard_Microtech.Controllers
{
    public class SicurezzaController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Password()
        {
            return View();
        }

        public IActionResult Cambiapassword([FromForm]string password)
        {
            Agent agent = JsonConvert.DeserializeObject<Agent>(HttpContext.Session.GetString("Logged"));

            string encrypted = Hashing.Hash(password);

            string query = "UPDATE Agenti_as400 SET Password = '" + encrypted + "' WHERE Codice_ACG = '" + agent.Codice_ACG + "'";

            SqlService service = new SqlService();
            service.init();
            if (service.insert(query))
            {
                service.close();
                return View("../Home/Index", model:JsonConvert.DeserializeObject<Agent>(HttpContext.Session.GetString("Logged")));
            }
            else
            {
                return View("Password");
            }  
        }

        
    }
}