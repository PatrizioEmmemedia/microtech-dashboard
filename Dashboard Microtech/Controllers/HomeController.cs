﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Dashboard_Microtech.Models;

namespace Dashboard_Microtech.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("Logged") == null)
            {
                return View("../Login/Index");
            }
            return View(JsonConvert.DeserializeObject<Microtech_Core.Database.Models.Agent>(HttpContext.Session.GetString("Logged")));
        }

        public IActionResult Catalog()
        {
            if (HttpContext.Session.GetString("Logged") == null)
            {
                return View("../Login/Index");
            }
            return View();
        }

        public async Task<IActionResult> About()
        {
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";
            if (HttpContext.Session.GetString("Logged") == null)
            {
                return View("../Login/Index");
            }
            return View();
        }

        public IActionResult Privacy()
        {
            if (HttpContext.Session.GetString("Logged") == null)
            {
                return View("../Login/Index");
            }
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            if (HttpContext.Session.GetString("Logged") == null)
            {
                return View("../Login/Index");
            }
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
