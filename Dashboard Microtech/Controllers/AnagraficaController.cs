﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microtech_Core.Database;
using Microtech_Core.Database.Functions;
using Microtech_Core.Database.Models;
using Newtonsoft.Json;

namespace Dashboard_Microtech.Controllers
{
    public class AnagraficaController : Controller
    {
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("Logged") == null)
            {
                return View("../Login/Index");
            }
            return View("DeliveryCustomers", JsonConvert.DeserializeObject<Agent>(HttpContext.Session.GetString("Logged")));
        }
        public IActionResult Fiscali()
        {
            if (HttpContext.Session.GetString("Logged") == null)
            {
                return View("../Login/Index");
            }
            return View("DeliveryCustomers", JsonConvert.DeserializeObject<Agent>(HttpContext.Session.GetString("Logged")));
        }
        public IActionResult Spedizione()
        {
            if (HttpContext.Session.GetString("Logged") == null)
            {
                return View("../Login/Index");
            }
            return View("DeliveryCustomers", JsonConvert.DeserializeObject<Agent>(HttpContext.Session.GetString("Logged")));
        }
        [HttpGet]
        public async Task<string> GetUserById([FromQuery]int AS400_ID)
        {
            SqlService dbInstance = new SqlService();
            dbInstance.init();
            SqlDataReader reader;
            List<Customer> customers = new List<Customer>();

            string queryString = ("SELECT * FROM Customer WHERE Customer.AS400_ID = '" + AS400_ID + "'");
            reader = dbInstance.query(queryString);

            if (reader != null)
            {
                while (reader.Read())
                {
                    Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
                    
                    return JsonConvert.SerializeObject(SQLMapping.MapCustomer(column));
                }
            }

            return JsonConvert.SerializeObject(customers);

        }
        [HttpGet]
        public async Task<string> GetUserByAgentAsync([FromQuery]string agentId)
        {
            SqlService dbInstance = new SqlService();
            dbInstance.init();
            SqlDataReader reader;
            List<Customer> customers = new List<Customer>();

            string queryString = ("SELECT * FROM Customer WHERE Customer.AGENT_CODE_AS400 = '" + agentId + "'");
            reader = dbInstance.query(queryString);

            if(reader != null)
            {
                while (reader.Read())
                {
                    Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
                    customers.Add(SQLMapping.MapCustomer(column));
                }
            }

            return JsonConvert.SerializeObject(customers);
        } 
        [HttpGet]
        public IActionResult Result([FromQuery]CustomerQuery query)
        {
            if (HttpContext.Session.GetString("Logged") == null)
            {
                return View("../Login/Index");
            }
            int type = int.Parse(HttpContext.Request.Query["type"]);

            SqlService dbInstance = new SqlService();
            dbInstance.init();
            SqlDataReader reader;
            List<Customer> customers = new List<Customer>();

            
            switch (type)
            {
                case 0:
                    string queryString;

                    SqlCommand command = new SqlCommand("SelectClient", dbInstance.getConnection());
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.Add("@CompanyName", System.Data.SqlDbType.VarChar).Value = query.name.ToUpper();
                    reader = command.ExecuteReader();

                    break;
                case 1:
                    queryString = ("SELECT * FROM Customer WHERE Customer.AS400_CODE = '" + query.name.ToUpper() + "'");
                    reader = dbInstance.query(queryString);
                    break;
                default:
                    reader = null;
                    break;

            }

            if (reader != null)
            {
                while (reader.Read())
                {
                    Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
                    Customer customer = SQLMapping.MapCustomer(column);
                    
                    if(query.provincia != null && query.name != null && query.CLI == null)
                    {
                        if(query.name != null)
                        {

                            if (customer.Province == query.provincia && customer.City.ToLower() == query.citta.ToLower())
                            {
                                customers.Add(customer);
                            }
                        }

                    }
                    else
                    {
                        customers.Add(customer);
                    }
                    
                }
            }
            CustomerSearchModel settings = new CustomerSearchModel();
            settings.citta = query.citta;
            settings.provincia = query.provincia;
            settings.type = query.type;
            //Scremando
            if(query.CLI == null)
            {
                if (query.cap != null)
                {
                    customers = customers.Where(x => x.CAP == query.cap).ToList<Customer>();
                }
                if (query.provincia != null)
                {
                    customers = customers.Where(x => x.Province == query.provincia).ToList<Customer>();
                }
                if (query.citta != null)
                {
                    customers = customers.Where(x => x.City.ToLower() == query.citta.ToLower()).ToList<Customer>();
                }
            }
            
            
            settings.customers = customers;
            dbInstance.close();
            return View(settings);
        }
        [HttpGet]
        public async Task<string> getCustomersAsync([FromQuery]CustomerQuery query)
        {
            int type = int.Parse(HttpContext.Request.Query["type"]);

            SqlService dbInstance = new SqlService();
            dbInstance.init();
            SqlDataReader reader;
            List<Customer> customers = new List<Customer>();

            switch (type)
            {
                case 0:
                    string queryString;
                    if (query.societa != null)
                    {
                        queryString = ("SELECT * FROM Customer WHERE Customer.CompanyName LIKE '%" + query.societa.ToUpper() + "%'");
                    }
                    else
                    {
                        if (query.name != null)
                        {
                            queryString = ("SELECT * FROM Customer WHERE Customer.Name = '" + query.name.ToUpper() + "'");
                            if (query.surname != null)
                            {
                                queryString += "AND Customer.Surname = '" + query.surname.ToUpper() + "'";
                            }
                        }
                        else if (query.name == null && query.surname != null)
                        {
                            queryString = ("SELECT * FROM Customer WHERE Customer.Surname = '" + query.surname.ToUpper() + "'");
                        }
                        else if (query.provincia != null)
                        {
                            queryString = ("SELECT * FROM Customer WHERE Customer.Province = '" + query.provincia + "'");
                            if (query.citta != null)
                            {
                                queryString += "AND Customer.City = '" + query.citta.ToUpper() + "'";

                            }
                            queryString += "ORDER BY PK OFFSET " + query.startItem + " ROWS FETCH NEXT 20 ROWS ONLY";

                        }
                        else
                        {
                            queryString = null;
                        }
                    }

                    reader = dbInstance.query(queryString);
                    break;
                default:
                    reader = null;
                    break;

            }

            if (reader != null)
            {
                while (reader.Read())
                {
                    Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
                    Customer customer = SQLMapping.MapCustomer(column);

                    if (query.provincia != null && query.name != null)
                    {
                        if (query.name != null)
                        {

                            if (customer.Province == query.provincia && customer.City.ToLower() == query.citta.ToLower())
                            {
                                customers.Add(customer);
                            }
                        }

                    }
                    else
                    {
                        customers.Add(customer);
                    }

                }
            }
            CustomerSearchModel settings = new CustomerSearchModel();
            settings.citta = query.citta;
            settings.provincia = query.provincia;

            //Scremando
            if (query.cap != null)
            {
                customers = customers.Where(x => x.CAP == query.cap).ToList<Customer>();
            }
            if (query.provincia != null)
            {
                customers = customers.Where(x => x.Province == query.provincia).ToList<Customer>();
            }
            if (query.citta != null)
            {
                customers = customers.Where(x => x.City.ToLower() == query.citta.ToLower()).ToList<Customer>();
            }
            dbInstance.close();
            return JsonConvert.SerializeObject(customers);
        }

        public async Task<string> GetInvoiceCustomers()
        {
            Agent agent = JsonConvert.DeserializeObject<Agent>(HttpContext.Session.GetString("Logged"));
            string query = "SELECT DISTINCT b.AS400_CODE as Fatturazione, a.CompanyName, b.AS400_ID as ID, b.CompanyName as Dipartimento, b.PK, b.Email,b.Address, b.CAP, b.Province, b.City FROM Customer as a INNER JOIN Customer as b ON b.AS400_CODE = a.InvoiceCustomer WHERE a.AGENT_CODE_AS400 = '" + agent.Codice_ACG + "'";
            SqlService service = new SqlService();
            service.init();
            SqlDataReader reader = service.query(query);
            List<InvoiceCustomer> customers = new List<InvoiceCustomer>();

            while (reader.Read())
            {
                Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
                InvoiceCustomer customer = SQLMapping.MapInvoiceCustomer(column);
                
                if(!customers.Any(x => x.ID == customer.ID))
                {
                    customers.Add(customer);
                }
                
            }
            service.close();
            return JsonConvert.SerializeObject(customers);
        }

        public async Task<string> GetAllCustomers()
        {
            var query = "SELECT PK, CompanyName, AS400_CODE, AS400_ID FROM Customer WHERE Active = 1 ";
            SqlService service = new SqlService();

            service.init();

            SqlDataReader reader = service.query(query);

            List<SyntheticCustomer> customers = new List<SyntheticCustomer>();

            while (reader.Read())
            {
                Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
                SyntheticCustomer customer = SQLMapping.MapSyntheticCustomer(column);
                if(!customers.Any(x => x.AS400_ID == customer.AS400_ID))
                {
                    customers.Add(customer);
                }
            }

            service.close();


            return JsonConvert.SerializeObject(customers);
        }

        public async Task<string> GetAS400Code(string as400)
        {
            var query = "SELECT * FROM Customer WHERE AS400_CODE = " + as400;
            SqlService service = new SqlService();

            service.init();

            SqlDataReader reader = service.query(query);

            List<Customer> customers = new List<Customer>();

            while (reader.Read())
            {
                Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
                Customer customer = SQLMapping.MapCustomer(column);
                if (!customers.Any(x => x.AS400_ID == customer.AS400_ID))
                {
                    customers.Add(customer);
                }
            }

            service.close();


            return JsonConvert.SerializeObject(customers);
        }
    }
}