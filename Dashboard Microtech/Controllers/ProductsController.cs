﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft_Core.Database.Models;
using Microtech_Core.Database;
using Microtech_Core.Database.Functions;
using Microtech_Core.Database.Models;
using Microtech_Core.WebServices.Services;
using Microtech_Core.WebServices.Services.Pojo;
using Newtonsoft.Json;

namespace Dashboard_Microtech.Views
{
    public class ProductsController : Controller
    {
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("Logged") == null)
            {
                return View("../Login/Index");
            }
            return View("Search");
        }
        public IActionResult Search()
        {
            if (HttpContext.Session.GetString("Logged") == null)
            {
                return View("../Login/Index");
            }
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Result([FromQuery]int onlyAvailable, [FromQuery]int search, [FromQuery]int page)
        {
            if (HttpContext.Session.GetString("Logged") == null)
            {
                return View("../Login/Index");
            }
            SqlService dbInstance = new SqlService();
            dbInstance.init();
            int typeSearch = int.Parse((String)HttpContext.Request.Query["type"]);

            string sku = HttpContext.Request.Query["sku"];
            string filter;
            switch (search)
            {
                case 0:
                    filter = "SKU";
                    break;
                case 1:
                    filter = "Name";
                    break;
                default:
                    filter = "SKU";
                    break;
            }

            int offset = int.Parse(HttpContext.Request.Query["startItem"]);
            SqlDataReader reader;
            List<Product> records = new List<Product>();
            string queryString;

            if (filter == "Name")
            {
                string queryPart = "";
                if (sku.Contains(" "))
                {
                    string[] terms = sku.Split(' ');
                    for (int i = 0; i < terms.Length; i++)
                    {
                        if (i < terms.Length - 1)
                        {
                            queryPart += "\"" + terms[i] + "\" AND ";
                        }
                        else
                        {
                            queryPart += "\"" + terms[i] + "\"";
                        }

                    }
                }
                else
                {
                    queryPart = sku;
                }


                queryString = "SELECT P.*, A.GIACENZA FROM Product as P LEFT JOIN ProductAvailabilityMapping as A ON P.AS400_ID = A.AS400_ID WHERE CONTAINS(" + filter + ", '" + queryPart + "') AND DELETED = 1 ORDER BY A.GIACENZA DESC OFFSET " + offset + " ROWS FETCH NEXT 20 ROWS ONLY";
                //queryString = "SELECT P.*, A.GIACENZA FROM Product as P LEFT JOIN ProductAvailabilityMapping as A ON P.AS400_ID = A.AS400_ID WHERE FREETEXT(" + filter + ", '" + sku + "') AND DELETED = 1 ORDER BY A.GIACENZA DESC OFFSET " + offset + " ROWS FETCH NEXT 20 ROWS ONLY";
                System.Diagnostics.Debug.WriteLine(queryString);
            }
            else
            {
                switch (typeSearch)
                {
                    case 0:
                        queryString = "SELECT P.*, A.GIACENZA FROM Product as P LEFT JOIN ProductAvailabilityMapping as A ON P.AS400_ID = A.AS400_ID WHERE " + filter + " = '" + sku + "' AND DELETED = 1 ORDER BY A.GIACENZA DESC";
                        break;
                    case 1:
                        queryString = "SELECT P.*, A.GIACENZA FROM Product as P LEFT JOIN ProductAvailabilityMapping as A ON P.AS400_ID = A.AS400_ID WHERE " + filter + " LIKE '" + sku + "%' AND DELETED = 1 ORDER BY A.GIACENZA DESC";
                        break;
                    case 2:
                        queryString = "SELECT P.*, A.GIACENZA FROM Product as P LEFT JOIN ProductAvailabilityMapping as A ON P.AS400_ID = A.AS400_ID WHERE " + filter + " LIKE '%" + sku + "' AND DELETED = 1 ORDER BY A.GIACENZA DESC";
                        break;
                    case 3:
                        queryString = "SELECT P.*, A.GIACENZA FROM Product as P LEFT JOIN ProductAvailabilityMapping as A ON P.AS400_ID = A.AS400_ID WHERE " + filter + " LIKE '%" + sku + "%' AND DELETED = 1 ORDER BY A.GIACENZA DESC";
                        break;
                    case 4:
                        queryString = "SELECT ProductCategoryMapping.ProductFK, ProductCategoryMapping.CategoryFK, Product.* FROM ProductCategoryMapping JOIN Product ON ProductCategoryMapping.ProductFK = Product.PK WHERE CategoryFK = " + sku;
                        break;
                    default:
                        queryString = null;
                        break;
                }
            }
            reader = dbInstance.query(queryString);
            if (reader != null)
            {
                while (reader.Read())
                {
                    Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());

                    Product product = SQLMapping.MapProduct(column);

                    if (!CheckSample(product.SKU).Result)
                    {
                        records.Add(product);
                    }


                }

                reader.Close();
            }

            records = records.OrderBy(x => x.GIACENZA).ToList();
            records.Reverse();
            dbInstance.close();
            return View(records);
        }

        public PartialViewResult getProductView()
        {
            return PartialView("Search");
        }
        public async Task<String> GetProducts([FromQuery]int startItem, [FromQuery]string param, [FromQuery]int searchtype)
        {
            if (searchtype == 0)
            {
                var queryString = "SELECT P.*, A.GIACENZA FROM Product as P LEFT JOIN ProductAvailabilityMapping as A ON P.AS400_ID = A.AS400_ID WHERE SKU = '" + param + "' AND DELETED = 1 ORDER BY A.GIACENZA DESC OFFSET " + startItem + " ROWS FETCH NEXT 20 ROWS ONLY";

                SqlDataReader reader;
                SqlService dbInstance = new SqlService();
                dbInstance.init();
                List<Product> records = new List<Product>();
                reader = dbInstance.query(queryString);
                System.Diagnostics.Debug.WriteLine(queryString);
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());

                        Product product = SQLMapping.MapProduct(column);

                        if (!CheckSample(product.SKU).Result)
                        {
                            records.Add(product);
                        }


                    }

                    reader.Close();
                }

                records = records.OrderBy(x => x.GIACENZA).ToList();
                records.Reverse();

                return JsonConvert.SerializeObject(records);
            }
            else
            {
                string queryPart = "";
                if (param.Contains(" "))
                {
                    string[] terms = param.Split(' ');
                    for (int i = 0; i < terms.Length; i++)
                    {
                        if (i < terms.Length - 1)
                        {
                            queryPart += "\"" + terms[i] + "\" AND ";
                        }
                        else
                        {
                            queryPart += "\"" + terms[i] + "\"";
                        }

                    }
                }
                else
                {
                    queryPart = param;
                }

                string query = "SELECT DISTINCT P.*, A.GIACENZA FROM Product as P LEFT JOIN ProductAvailabilityMapping as A ON P.AS400_ID = A.AS400_ID WHERE CONTAINS(Name, '" + queryPart + "') AND DELETED = 1 ORDER BY A.GIACENZA DESC OFFSET " + startItem + " ROWS FETCH NEXT 20 ROWS ONLY";

                SqlService dbInstance = new SqlService();
                dbInstance.init();
                SqlDataReader reader = dbInstance.query(query);
                List<Product> records = new List<Product>();
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
                        Product product = SQLMapping.MapProduct(column);
                        if(!CheckSampleSync(product.SKU)) records.Add(product);


                    }

                    reader.Close();
                }
                dbInstance.close();

                return JsonConvert.SerializeObject(records);
            }



        }
        public async Task<String> getResultAsync([FromQuery]int itemPage, [FromQuery]int onlyAvailable, [FromQuery]int search)
        {
            SqlService dbInstance = new SqlService();
            dbInstance.init();
            string sku = HttpContext.Request.Query["sku"];
            int typeSearch = int.Parse((String)HttpContext.Request.Query["type"]);
            int offset = int.Parse(HttpContext.Request.Query["startItem"]);
            string filter;
            switch (search)
            {
                case 0:
                    filter = "SKU";
                    break;
                case 1:
                    filter = "Name";
                    break;
                default:
                    filter = "SKU";
                    break;
            }
            string queryString;
            SqlDataReader reader;
            List<Product> records = new List<Product>();
            if (itemPage == 0)
            {
                itemPage = 15;
            }



            if (filter == "Name")
            {

                string queryPart = "";
                if (sku.Contains(" "))
                {
                    string[] terms = sku.Split(' ');
                    for (int i = 0; i < terms.Length; i++)
                    {
                        if (i < terms.Length - 1)
                        {
                            queryPart += "\"" + terms[i] + "\" AND ";
                        }
                        else
                        {
                            queryPart += "\"" + terms[i] + "\"";
                        }

                    }
                }
                else
                {
                    queryPart = sku;
                }


                queryString = "SELECT TOP 500 P.*, A.GIACENZA FROM Product as P LEFT JOIN ProductAvailabilityMapping as A ON P.AS400_ID = A.AS400_ID WHERE CONTAINS(" + filter + ", '" + queryPart + "') AND DELETED = 1 ORDER BY A.GIACENZA DESC";
                System.Diagnostics.Debug.WriteLine(queryString);
            }
            else
            {
                switch (typeSearch)
                {
                    case 0:
                        queryString = "SELECT P.*, A.GIACENZA FROM Product as P LEFT JOIN ProductAvailabilityMapping as A ON P.AS400_ID = A.AS400_ID WHERE " + filter + " = '" + sku + "' AND DELETED = 1 ORDER BY A.GIACENZA DESC";
                        break;
                    case 1:
                        queryString = "SELECT P.*, A.GIACENZA FROM Product as P LEFT JOIN ProductAvailabilityMapping as A ON P.AS400_ID = A.AS400_ID WHERE " + filter + " LIKE '" + sku + "%' AND DELETED = 1 ORDER BY A.GIACENZA DESC";
                        break;
                    case 2:
                        queryString = "SELECT P.*, A.GIACENZA FROM Product as P LEFT JOIN ProductAvailabilityMapping as A ON P.AS400_ID = A.AS400_ID WHERE " + filter + " LIKE '%" + sku + "' AND DELETED = 1 ORDER BY A.GIACENZA DESC";
                        break;
                    case 3:
                        queryString = "SELECT P.*, A.GIACENZA FROM Product as P LEFT JOIN ProductAvailabilityMapping as A ON P.AS400_ID = A.AS400_ID WHERE " + filter + " LIKE '%" + sku + "%' AND DELETED = 1 ORDER BY A.GIACENZA DESC";
                        break;
                    case 4:
                        queryString = "SELECT ProductCategoryMapping.ProductFK, ProductCategoryMapping.CategoryFK, Product.* FROM ProductCategoryMapping JOIN Product ON ProductCategoryMapping.ProductFK = Product.PK WHERE CategoryFK = " + sku;
                        break;
                    default:
                        queryString = null;
                        break;
                }
            }



            //queryString = "SELECT * FROM Product WHERE FREETEXT(" + filter + ", '" + sku + "') AND Deleted = 1";
            /* queryString += " AND Deleted = 1";
             queryString += " ORDER BY PK OFFSET " + offset + " ROWS FETCH NEXT " + 20 + " ROWS ONLY;";*/

            reader = dbInstance.query(queryString);
            if (reader != null)
            {
                while (reader.Read())
                {
                    Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());

                    records.Add(SQLMapping.MapProduct(column));

                }

                reader.Close();
            }
            dbInstance.close();

            return JsonConvert.SerializeObject(records);

        }

        public async Task<List<Category>> getCategories()
        {
            SqlService dbInstance = new SqlService();
            dbInstance.init();
            SqlDataReader reader;
            List<Category> records = new List<Category>();
            reader = dbInstance.query("SELECT * FROM Menu");
            if (reader != null)
            {
                while (reader.Read())
                {
                    Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());

                    Category cat = SQLMapping.MapCategory(column);
                    if (cat.AS400_CODE.Length == 4)
                    {
                        records.Add(cat);
                    }
                }
            }
            dbInstance.close();
            return records;
        }
        [HttpGet]
        public async Task<string> getProductPrice()
        {
            string idart = HttpContext.Request.Query["idart"];
            string idcli = HttpContext.Request.Query["idcli"];
            string date = DateTime.Today.ToString("yyyyMMdd");
            GetPriceService service = new GetPriceService(idart, idcli, date, "http://2.229.69.115:8015/www/dacgi/WSGetPrice.pgm");
            GetPrice model = service.GetPriceAsync();

            return JsonConvert.SerializeObject(model);
        }

        [HttpGet]
        public async Task<bool> CheckSample([FromQuery]string sku)
        {
            string query = "SELECT Product.PK, Product.Name, ProductCategoryMapping.CategoryFK FROM Product INNER JOIN ProductCategoryMapping ON ProductCategoryMapping.ProductFK = Product.PK WHERE Product.SKU = '" + sku + "'";
            SqlService service = new SqlService();
            service.init();
            SqlDataReader reader = service.query(query);

            while (reader.Read())
            {
                Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
                var sample = SQLMapping.MapSample(column);

                if (sample.CategoryFK == 332 || sample.CategoryFK == 331 || sample.CategoryFK == 318)
                {
                    service.close();
                    return true;
                }
                else
                {
                    service.close();
                    return false;
                }
            }
            service.close();
            return false;
        }

        public async Task<int> GetProductPieces([FromQuery]string AS400_ID)
        {
            string query = "SELECT * FROM Product WHERE AS400_ID = " + AS400_ID;

            SqlService service = new SqlService();
            service.init();
            SqlDataReader reader = service.query(query);

            while (reader.Read())
            {
                Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());

                var quantity = SQLMapping.MapProductQuantity(column);

                service.close();

                return quantity.Confezionamento;
            }

            service.close();
            return 0;
        }

        public bool CheckSampleSync(string sku)
        {
            string query = "SELECT Product.PK, Product.Name, ProductCategoryMapping.CategoryFK FROM Product INNER JOIN ProductCategoryMapping ON ProductCategoryMapping.ProductFK = Product.PK WHERE Product.SKU = '" + sku + "'";
            SqlService service = new SqlService();
            service.init();
            SqlDataReader reader = service.query(query);

            while (reader.Read())
            {
                Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
                var sample = SQLMapping.MapSample(column);

                if (sample.CategoryFK == 332 || sample.CategoryFK == 331 || sample.CategoryFK == 318)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            service.close();
            return false;
        }
    }
}