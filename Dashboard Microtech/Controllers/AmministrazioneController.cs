﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microtech_Core.Database.Models;
using Microtech_Core.WebServices.Services.Misc.Utils;
using Microtech_Core.Database;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using Dashboard_Microtech.Models;
using Microtech_Core.Database.Functions;

namespace Dashboard_Microtech.Controllers
{
    public class AmministrazioneController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Agenti()
        {
            List<Agent> agents = Utils.GetAgents();
            List<Agent> filteredAgents = agents.FindAll(x=>x.Codice_ACG.Contains("A") && !x.Codice_ACG.Contains("AN") && x.Codice_ACG != "AXX" && x.Codice_ACG != "ATO" && int.Parse(x.Codice_ACG.Split('A')[1]) < 30).OrderBy(x=>x.Codice_ACG).ToList();
            return View(filteredAgents);
        }

        public async Task<bool> DeleteAgent([FromQuery]string code)
        {
            var queryString = "DELETE FROM Agenti_as400 WHERE Codice_ACG = '" + code + "'";

            SqlService service = new SqlService();
            service.init();

            var success = service.insert(queryString);
            service.close();

            return success;

        }

        public async Task<bool> EditAgent([FromQuery]string code, [FromQuery]string desc, [FromQuery]string email, [FromQuery]string telefono, [FromQuery]string password)
        {
            

            var queryString = "UPDATE Agenti_as400 SET Descrizione = '" + desc.Replace("'", "''") + "', Email = '" + email + "', Cellulare = '" + telefono + "'";
            if (password != null)
            {
                string pwd = Hashing.Hash(password);
                queryString += ", Password = '" + pwd + "'";

            }

            queryString += "WHERE Codice_ACG = '" + code + "'";

            SqlService service = new SqlService();
            service.init();

            var success = service.insert(queryString);

            service.close();

            return success;

        }
        
        public IActionResult Discount()
        {
            string discountText = System.IO.File.ReadAllText(Environment.CurrentDirectory + "/wwwroot/sconti.json");

            JObject discountDict = (JObject)JsonConvert.DeserializeObject(discountText);
            
            return View(discountDict);
           
        }

        public async Task<bool> SaveDiscount([FromBody]JObject json)
        {

            System.IO.File.WriteAllText(Environment.CurrentDirectory + "/wwwroot/sconti.json", JsonConvert.SerializeObject(json["json"]));

            return true;
        }

        public IActionResult statisticaAgenti([FromQuery]string agent, [FromQuery]string causal, [FromQuery]string dateBegin, [FromQuery]string dateEnd)
        {
            var query = "select * from Analytics where agent = '" + agent + "' ";

            

            if (causal != null) query += "AND causal = '" + causal + "'";


            SqlService service = new SqlService();
            service.init();

            AnalyticsAgent analytics = new AnalyticsAgent();

            analytics.agent = agent;

            if (causal != null) analytics.causal = causal;

            int index = 0;

            DateTime begin = dateBegin != null ? DateTime.Parse(dateBegin) : DateTime.MinValue;
            DateTime end = dateEnd != null ? DateTime.Parse(dateEnd) : DateTime.MinValue;

            bool editBegin = false;
            bool editEnd = false;
            if (begin == DateTime.MinValue && end != DateTime.MinValue) editBegin = true;
            if (begin != DateTime.MinValue && end == DateTime.MinValue) editEnd = true;

            if (editBegin) begin = DateTime.Today.AddYears(-2);
            if (editEnd) end = DateTime.Today;

            using (SqlDataReader reader = service.query(query))
            {



                while (reader.Read())
                {
                    if((begin == DateTime.MinValue && end == DateTime.MinValue) || ((DateTime)reader["actualDocumentDate"] > begin && (DateTime)reader["actualDocumentDate"] < end))
                    {
                        Models.Customer customer;
                        Models.Order order;
                        Models.Product product = new Models.Product();

                        if (analytics.customers.Count == 0 || !analytics.customers.Any(x => x.delivery == (int)reader["delivery"]))
                        {
                            customer = new Models.Customer();

                            customer.delivery = (int)reader["delivery"];
                            customer.invoice = (int)reader["invoice"];
                            customer.name = (string)reader["name"];
                            customer.surname = (string)reader["surname"];
                            analytics.customers.Add(customer);

                        }
                        else
                        {
                            customer = analytics.customers.Where(x => x.delivery == (int)reader["delivery"]).First();
                        }

                        if (customer.orders.Count == 0 || !customer.orders.Any(x => x.documentNumber == (int)reader["actualDocumentNumber"]))
                        {
                            order = new Models.Order();
                            order.causal = (string)reader["causal"];
                            order.date = (DateTime)reader["actualDocumentDate"];
                            order.documentNumber = (int)reader["actualDocumentNumber"];
                            order.year = (int)reader["actualDocumentYear"];
                            order.orderValue += (double)reader["deliveredValue"];
                            

                        }
                        else
                        {
                            order = customer.orders.Where(x => x.documentNumber == (int)reader["actualDocumentNumber"]).First();
                            order.orderValue += (double)reader["deliveredValue"];
                        }

                        product.sku = (string)reader["sku"];
                        product.qty = (double)reader["deliveredQuantity"];
                        product.year = (int)reader["actualDocumentYear"];
                        product.net = (double)reader["deliveredValue"];

                        if (product.sku == "H 0030 120 086")
                        {
                            System.Diagnostics.Debug.WriteLine((double)reader["deliveredValue"]);
                        }
                        product.cumage = (double)reader["costInWarehouse"] * product.qty;
                        analytics.products.Add(product);

                        if (causal != null)
                        {
                            if (causal == "PR") analytics.preCustomers++;
                            else analytics.orderCustomers++;

                            order.products.Add(product);
                            customer.orders.Add(order);

                        }
                        else
                        {
                            if (order.causal == "OC" || order.causal == "F2" || order.causal == "NA")
                            {
                                analytics.orderCustomers++;
                                order.products.Add(product);
                                customer.orders.Add(order);
                                if(!analytics.dates.Any(x => x == order.year))
                                {
                                    analytics.dates.Add(order.year);
                                }


                            }
                            else if (order.causal == "PR")
                            {
                                analytics.preValue += order.orderValue;
                                analytics.preCustomers++;
                            }
                        }

                        index++;
                    }
                    
                }
            }

            return View(analytics);
        }

    }
}