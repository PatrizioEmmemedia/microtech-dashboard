﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microtech_Core.Database;
using Microtech_Core.Database.Functions;
using Microtech_Core.Database.Models;
using Newtonsoft.Json;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Dashboard_Microtech.Controllers
{
    public class LoginController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }
        
        [HttpPost]
        public IActionResult Login([FromForm]LoginModel form)
        {
            string password = Hashing.Hash(form.password);
            string queryString = "SELECT TOP 1 * FROM Agenti_as400 WHERE Email = '" + form.code + "' AND Password = '" + password + "'";

            SqlService service = new SqlService();
            service.init();
            SqlDataReader reader = service.query(queryString);
            Agent agent;
            while (reader.Read())
            {
                Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
                agent = SQLMapping.mapAgent(column);

                HttpContext.Session.SetString("Logged", JsonConvert.SerializeObject(agent));
                if (agent != null)
                {
                    service.close();
                    return View("../Home/Index", JsonConvert.DeserializeObject<Agent>(HttpContext.Session.GetString("Logged")));
                }
                else
                {
                    service.close();
                    return View("Index");
                }

            }

            return View("Index");

        }
        
        public IActionResult Logout()
        {
            HttpContext.Session.Remove("Logged");

            return View("Index");
        }
    }
}
