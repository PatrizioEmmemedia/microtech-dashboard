﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft_Core.Database.Models;
using Microtech_Core.Database;
using Microtech_Core.Database.Functions;
using Microtech_Core.Database.Models;
using Microtech_Core.WebServices.Services;
using Microtech_Core.WebServices.Services.Misc.Utils;
using Microtech_Core.WebServices.Services.Pojo;
using Newtonsoft.Json;

namespace Dashboard_Microtech.Controllers
{
    public class PreventiviController : Controller
    {
        public IActionResult Index()
        {
            return View("Search", model: JsonConvert.DeserializeObject<Agent>(HttpContext.Session.GetString("Logged")));
        }

        public IActionResult ShowResultList([FromQuery]string cli, [FromQuery]string dateBeg, [FromQuery]string dateEnd)
        {
            var getLsOrd = new GetLsPreService(cli, "http://2.229.69.115:8015/www/dacgi/WSGetLsPre.pgm").getLSPreAsync();
            if (getLsOrd != null)
            {
                List<GetLsPre> filteredList = null;
            DateTime Beg = default(DateTime);
            DateTime End = default(DateTime);

            Customer customer = Utils.GetCustomerNameByCode(getLsOrd[0].delId.ToString());
            if (dateBeg != null)
            {
                Beg = DateTime.ParseExact(dateBeg, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            if (dateEnd != null)
            {
                End = DateTime.ParseExact(dateEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }


            

                if (dateBeg != null && dateEnd == null)
                {
                    filteredList = getLsOrd.Where(i => DateTime.ParseExact(i.getDate(), "dd/MM/yyyy", CultureInfo.InvariantCulture) > Beg).ToList();
                }
                if (dateBeg == null && dateEnd != null)
                {
                    filteredList = getLsOrd.Where(i => DateTime.ParseExact(i.getDate(), "dd/MM/yyyy", CultureInfo.InvariantCulture) < End).ToList();
                }
                if (dateBeg != null && dateEnd != null)
                {
                    filteredList = getLsOrd.Where(i => DateTime.ParseExact(i.getDate(), "dd/MM/yyyy", CultureInfo.InvariantCulture) >= Beg && DateTime.ParseExact(i.getDate(), "dd/MM/yyyy", CultureInfo.InvariantCulture) <= End).ToList();
                }

                if (dateBeg == null && dateEnd == null)
                {
                    filteredList = getLsOrd;
                }
                filteredList = filteredList.OrderBy(x => int.Parse(x.date)).ToList();

                filteredList.Reverse();
                foreach (var order in filteredList)
                {
                    order.name = customer.CompanyName;
                }
                return View(model: filteredList);
            }
            else
            {
                return View(model: getLsOrd);
            }
         
            
        }
        public IActionResult ShowListByArt([FromQuery]string cli, [FromQuery]string art, [FromQuery]string dateBeg, [FromQuery]string dateEnd)
        {
            List<GetLSPreBP> getList = new GetLsPreClArt(cli, art).getLSPreAsync();

            var agentCode = JsonConvert.DeserializeObject<Agent>(HttpContext.Session.GetString("Logged")).Codice_ACG;

            var userList = Utils.getCustomers(agentCode);

            System.Diagnostics.Debug.WriteLine(userList.Count);
            List<GetLSPreBP> filteredList = getList.Where(x => userList.Any(y => y.AS400_CODE.ToString() == x.delId)).ToList();
            DateTime Beg = default(DateTime);
            DateTime End = default(DateTime);

            if (dateBeg != null)
            {
                Beg = DateTime.ParseExact(dateBeg, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            if (dateEnd != null)
            {
                End = DateTime.ParseExact(dateEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }

            if (dateBeg != null && dateEnd == null)
            {
                filteredList = filteredList.Where(i => DateTime.ParseExact(i.getDate(), "dd/MM/yyyy", CultureInfo.InvariantCulture) > Beg).ToList();
            }
            if (dateBeg == null && dateEnd != null)
            {
                filteredList = filteredList.Where(i => DateTime.ParseExact(i.getDate(), "dd/MM/yyyy", CultureInfo.InvariantCulture) < End).ToList();
            }
            if (dateBeg != null && dateEnd != null)
            {
                filteredList = filteredList.Where(i => DateTime.ParseExact(i.getDate(), "dd/MM/yyyy", CultureInfo.InvariantCulture) >= Beg && DateTime.ParseExact(i.getDate(), "dd/MM/yyyy", CultureInfo.InvariantCulture) <= End).ToList();
            }


            filteredList = filteredList.OrderBy(x => int.Parse(x.date)).ToList();
            filteredList.Reverse();

            foreach(var order in filteredList)
            {
                var customer = userList.FirstOrDefault(x => x.AS400_CODE == int.Parse(order.delId));
                order.name = customer.CompanyName;
            }

            return View("ShowResultList", model: filteredList);
        }
        public IActionResult ShowResultPre([FromQuery]string pre)
        {
            List<GetPre> getPre = new GetPreService(pre, "http://2.229.69.115:8015/www/dacgi/WSGetPre.pgm").getPreAsync();
            if(getPre != null)
            {
                for (int i = 0; i < getPre.Count; i++)
                {
                    Product product = GetProduct(getPre[i].idProduct);
                    if(product != null)
                    {
                        getPre[i].productName = product.Name;
                    }
                   
                }
            }
            else
            {
                return View(model: null);
            }
            
            return View(model: getPre);
        }

        public string GetPreAsync([FromQuery]string pre)
        {
            List<GetPre> getPre = new GetPreService(pre, "http://2.229.69.115:8015/www/dacgi/WSGetPre.pgm").getPreAsync();
            
            for(int i = 0; i < getPre.Count; i++)
            {
                Product product = GetProduct(getPre[i].idProduct);
                if(product != null && getPre != null)
                {
                    getPre[i].productName = product.Name.Replace("\"", "");
                }
               

            }
            return JsonConvert.SerializeObject(getPre);
        }
        public Product GetProduct(string idArt)
        {
            SqlService service = new SqlService();
            service.init();
            try
            {
                SqlDataReader reader = service.query("SELECT dbo.GetProductForPDF(Product.Name) as Name FROM Product WHERE AS400_ID = '" + idArt + "'");

                Product product = null;

                while (reader.Read())
                {
                    Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
                    product = SQLMapping.MapProduct(column);

                }
                service.close();
                return product;
            }
            catch(Exception ex)
            {
                service.close();
                throw new System.ArgumentException(ex.Message);
            }
            
            
        }
        [HttpPost]
        public string GetInvoice([FromBody]PDFBody order)
        {
            using (var reader = new StreamReader(Request.Body))
            {
                var body = reader.ReadToEnd();
                PDFService service = new PDFService(order);

                return service.GetPDF().Result;
            }

            
        }
    }
}