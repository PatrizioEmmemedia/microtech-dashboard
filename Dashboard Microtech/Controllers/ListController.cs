﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft_Core.Database.Models;
using Microtech_Core.Database;
using Microtech_Core.Database.Functions;
using Microtech_Core.Database.Models;
using Microtech_Core.WebServices.Services;
using Microtech_Core.Mail;
using Newtonsoft.Json;
using System.Web;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Dashboard_Microtech.Controllers
{
    public class ListController : Controller
    {
       

        public IActionResult Index([FromQuery]string type)
        {

            if (HttpContext.Session.GetString("Logged") == null)
            {
                return View("../Login/Index");
            }
            return View("Riepilogo", model: type);
        }
        public async Task<IActionResult> AddToList(Product product)
        {
            if (HttpContext.Session.GetString("Logged") == null)
            {
                return View("../Login/Index");
            }
            return PartialView();
        }
        public IActionResult Crea()
        {
            if (HttpContext.Session.GetString("Logged") == null)
            {
                return View("../Login/Index");
            }
            return View(JsonConvert.DeserializeObject<Agent>(HttpContext.Session.GetString("Logged")));
        }
        public async Task<string> SendOrder([FromQuery]AddOrderQuery query)
        {

            string queryString = "azi=" + query.azi;
            queryString += "&pre=" + query.pre;
            queryString += "&ido=" + query.ido;
            queryString += "&dto=" + query.dto;
            queryString += "&cli=" + query.cli;
            queryString += "&cig=" + query.cig;
            queryString += "&cup=" + query.cup;
            queryString += "&dti=" + query.dti;
            queryString += "&dtf=" + query.dtf;
            queryString += "&nrg=" + query.nrg;
            queryString += "&nrc=" + query.nrc;
            queryString += "&tpr=" + query.tpr;
            queryString += "&art=" + query.art;
            queryString += "&qty=" + query.qty;
            queryString += "&prz=" + query.prz;
            queryString += "&tpm=" + query.tpm;

            queryString += queryString.Replace(" ", "").Trim();

            SendOrdService ordService = new SendOrdService(queryString);
           
            string result = ordService.sendOrderAsync();

            string type;

            switch (query.pre)
            {
                case "WPR":
                    type = "Estimates";
                    break;
                case "WLS":
                    type = null;
                    break;
                default:
                    type = "Orders";
                    break;
            }
            System.Diagnostics.Debug.WriteLine(queryString);
            if(query.nrg == 1 && type != null && query.azi != 'S' && query.azi != 'C')
            {
                SqlService services = new SqlService();
                services.init();
                bool CopyOnDb = services.insert("INSERT INTO "+ type +" (AS400_USER) VALUES ('" + query.cli + "')");
                services.close();
                System.Diagnostics.Debug.WriteLine("INSERT INTO "+ type +" (AS400_USER) VALUES ('" + query.cli + "')");
            }



            return result;
        }

        public int GetLastOrder([FromQuery]string type)
        {
            string query = "SELECT TOP 1 * FROM Orders ORDER BY PK DESC";
            if (type == "WPR" || type == "WLS")
            {
                query = "SELECT TOP 1 * FROM Estimates ORDER BY PK DESC";
            }
            
            SqlService services = new SqlService();
            services.init();
            SqlDataReader reader = services.query(query);
            if(reader != null)
            {
                while (reader.Read())
                {
                    Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
                    System.Diagnostics.Debug.WriteLine(JsonConvert.SerializeObject(column));
                    return int.Parse(column["PK"]);
                }
            }
            services.close();
            return 0;
        }
        public async Task<string> GetCustomersAsync([FromQuery]string agentid)
        {
            string queryString = "SELECT * FROM Customer WHERE AGENT_CODE_AS400 = '" + agentid + "'";

            SqlService services = new SqlService();
            services.init();
            SqlDataReader reader = services.query(queryString);
            List<Customer> customers = new List<Customer>();
            if(reader != null)
            {
                while (reader.Read())
                {
                    Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
                    customers.Add(SQLMapping.MapCustomer(column));
                }
            }

            return JsonConvert.SerializeObject(customers);
        }
        [HttpPost]
        public async Task<bool> sendMail()
        {
            HttpContext.Request.EnableRewind();
            string jsonData = new StreamReader(HttpContext.Request.Body).ReadToEnd();
            System.Diagnostics.Debug.WriteLine(jsonData);
            Dictionary<String, String> JSON = JsonConvert.DeserializeObject<Dictionary<String, String>>(jsonData);
            var agent = JsonConvert.DeserializeObject<Agent>(HttpContext.Session.GetString("Logged"));
            Components components = new Components();
            
            components.ToMail = agent.Email;
            components.toName = agent.Descrizione;

            if (JSON.ContainsKey("email"))
            {
                components.ToMail = JSON["email"];
                components.toName = JSON["name"];
            }

            components.subject = "RIEPILOGO TRANSAZIONE";
            
            components.body = components.MailBodyBuilder(JSON["printUrl"].Replace("+", " "));

            System.Diagnostics.Debug.WriteLine(components.ToMail);
            //Mail mail = new Mail(components);
            //mail.SendMail();

            return true;
        }
    }
}
