﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace  Microtech_Core.Database
{
    public class SqlService
    {
        SqlConnection sqlConnection;

        public SqlService() {}
        public void init()
        {
            sqlConnection = new SqlConnection("Data Source=(local)\\sqldeveloper;Initial Catalog=Microtech_Ecommerce;Min Pool Size=5;Max Pool Size=60;Connect Timeout=30;User ID=sa;Password=Emm3.75!");
            sqlConnection.Open();
        }

        public void close()
        {
            sqlConnection.Close();
        }

        public SqlDataReader query(string query)
        {
            SqlCommand command = new SqlCommand(query, sqlConnection);
            SqlDataReader dataReader = command.ExecuteReader();

            return dataReader;
        }

        public bool insert(string query)
        {
            SqlCommand command = new SqlCommand(query, sqlConnection);

            if (command.ExecuteNonQuery() < 0)
            {
                return false;
            }
            else
            {
                return true;
            }

        }
        public SqlConnection getConnection()
        {
            return sqlConnection;
        }

    }
}
