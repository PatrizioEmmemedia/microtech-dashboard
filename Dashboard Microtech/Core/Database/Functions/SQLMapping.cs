﻿using Microsoft_Core.Database.Models;
using Microtech_Core.Database.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.Database.Functions
{
    public class SQLMapping
    {
        public static Product MapProduct(Dictionary<string, string> sourceObject)
        {
           
            string jsonColumn = JsonConvert.SerializeObject(sourceObject, Formatting.Indented);
            if (jsonColumn != null)
            {
                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };
                Product product = JsonConvert.DeserializeObject<Product>(jsonColumn, settings);
                return product;
            }
            return null;
        }
        public static Category MapCategory(Dictionary<string, string> sourceObject)
        {

            string jsonColumn = JsonConvert.SerializeObject(sourceObject, Formatting.Indented);
            if (jsonColumn != null)
            {
                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };
                // System.Diagnostics.Debug.WriteLine(jsonColumn);
                Category category = JsonConvert.DeserializeObject<Category>(jsonColumn, settings);
                return category;
            }
            return null;
        }
        public static Customer MapCustomer(Dictionary<string, string> sourceObject)
        {
            string jsonColumn = JsonConvert.SerializeObject(sourceObject, Formatting.Indented);
            if (jsonColumn != null)
            {
                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };
                Customer customer = JsonConvert.DeserializeObject<Customer>(jsonColumn, settings);
                return customer;
            }
            return null;
        }
        public static Agent mapAgent(Dictionary<string, string> sourceObject)
        {
            string jsonColumn = JsonConvert.SerializeObject(sourceObject, Formatting.Indented);
            if (jsonColumn != null)
            {
                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };
                Agent agent = JsonConvert.DeserializeObject<Agent>(jsonColumn, settings);
                return agent;
            }
            return null;
        }

        public static Listino MapListino(Dictionary<String, String> sourceObject)
        {
            string jsonColumn = JsonConvert.SerializeObject(sourceObject, Formatting.Indented);
            System.Diagnostics.Debug.WriteLine("LISTINO " + jsonColumn);
            if (jsonColumn != null)
            {
                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };
                Listino listino = JsonConvert.DeserializeObject<Listino>(jsonColumn, settings);
                return listino;
            }
            return null;
        }

        public static ListByAgent MapList(Dictionary<String, String> sourceObject)
        {
            string jsonColumn = JsonConvert.SerializeObject(sourceObject, Formatting.Indented);
            if(jsonColumn != null)
            {
                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };
                ListByAgent listino = JsonConvert.DeserializeObject<ListByAgent>(jsonColumn, settings);
                return listino;
            }
            return null;
        }

        public static CheckCategory MapSample(Dictionary<string, string> sourceObject)
        {
            string jsonColumn = JsonConvert.SerializeObject(sourceObject, Formatting.Indented);
            if (jsonColumn != null)
            {
                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };
                CheckCategory listino = JsonConvert.DeserializeObject<CheckCategory>(jsonColumn, settings);
                return listino;
            }
            return null;
        }
        public static InvoiceCustomer MapInvoiceCustomer(Dictionary<string, string> sourceObject)
        {
            string jsonColumn = JsonConvert.SerializeObject(sourceObject, Formatting.Indented);
            if (jsonColumn != null)
            {
                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };
                InvoiceCustomer customer = JsonConvert.DeserializeObject<InvoiceCustomer>(jsonColumn, settings);
                return customer;
            }
            return null;
        }

        public static SyntheticCustomer MapSyntheticCustomer(Dictionary<string, string> sourceObject)
        {
            string jsonColumn = JsonConvert.SerializeObject(sourceObject, Formatting.Indented);
            if (jsonColumn != null)
            {
                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };
                SyntheticCustomer customer = JsonConvert.DeserializeObject<SyntheticCustomer>(jsonColumn, settings);
                return customer;
            }
            return null;
        }

        public static ProductQuantity MapProductQuantity(Dictionary<string, string> sourceObject)
        {
            string jsonColumn = JsonConvert.SerializeObject(sourceObject, Formatting.Indented);
            if (jsonColumn != null)
            {
                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };
                ProductQuantity productQuantity = JsonConvert.DeserializeObject<ProductQuantity>(jsonColumn, settings);
                return productQuantity;
            }
            return null;
        }
    }
}
