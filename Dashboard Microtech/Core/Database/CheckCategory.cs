﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.Database
{
    public class CheckCategory
    {
        public int PK{ get; set; }
        public int CategoryFK { get; set; }
        public string Name { get; set; }
        
    }
}
