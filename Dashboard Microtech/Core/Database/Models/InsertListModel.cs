﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.Database.Models
{

    public class InsertListModel
    {
        public int CustomerKey { get; set; }
        public int AS400_ID { get; set; }
        public string Agent { get; set; }
        public int ProductKey { get; set; }
        public string Price { get; set; }
    }
}
