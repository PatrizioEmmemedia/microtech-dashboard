﻿using Microtech_Core.WebServices.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microsoft_Core.Database.Models
{
    public class Product
    {
        public int PK { get; set; }
        public string SKU { get; set; }
        public string EAN { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string FullDescription { get; set; }
        public string ProductSheet { get; set; }
        public int ProductTemplateFK { get; set; }
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
        public string BrandPartNumber { get; set; }
        public string AlternativePartNumber { get; set; }
        public decimal AdditionalShippingCharge { get; set; }
        public int TaxCategoryFK { get; set; }
        public int StockQuantity { get; set; }
        public int StockOrder { get; set; }
        public bool ShowOnHomePage { get; set; }
        public bool IsShipEnabled { get; set; }
        public bool IsFreeShipping { get; set; }
        public bool AllowCustomerReviews { get; set; }
        public bool IsTaxExempt { get; set; }
        public bool Outlet { get; set; }
        public bool ExportAmazon { get; set; }
        public bool ExportEbay { get; set; }
        public bool ExportTrovaprezzi { get; set; }
        public bool DisplayStockAvailability { get; set; }
        public bool DisplayStockQuantity { get; set; }
        public bool DisableBuyButton { get; set; }
        public bool DisableWishlistButton { get; set; }
        public bool HasDiscountsApplied { get; set; }
        public float Weight { get; set; }
        public float Length { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }
        public string AvailableStartDateTime { get; set; }
        public string AvailableEndDateTime { get; set; }
        public bool Warranty { get; set; }
        public bool WarrantyDOA { get; set; }
        public string PurchasePrice { get; set;}
        public string StreetPrice { get; set; }
        public string RAEE { get; set; }
        public bool Published { get; set; }
        public bool Deleted { get; set; }
        public int Status { get; set; }
        public string CreatedDate { get; set; }
        public string UpdatedDate { get; set; }
        public string AS400_ID { get; set; }
        public string Confezionamento { get; set; }
        public string Fornitori { get; set; }
        public string GIACENZA { get; set; }
    }
    
}
