﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.Database.Models
{
    public class CustomerQuery
    {
        public int type { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string citta { get; set; }
        public string provincia { get; set; }
        public int page { get; set; }
        public int startItem { get; set; }
        public string cap { get; set; }
        public string indirizzo { get; set; }
        public string societa { get; set; }
        public string piva { get; set; }
        public string CLI { get; set; }
    }
}
