﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microtech_Core.Database.Models
{
    public class Listino
    {
        private string _Name;
        public int PK { get; set; }
        public int ProductKey { get; set; }
        public string Price { get; set; }
        public int CustomerKey { get; set; }
        public string AS400_ID { get; set; }
        public string Date { get; set; }
        public string Name {
            get
            {
               return _Name;
            }

            set
            {
                //_Name = Encoding.ASCII.GetString(Encoding.ASCII.GetBytes(value)).Replace(';
               _Name = value.Replace("\"", "").Replace('■','?');
            }
        }
        
        public string SKU { get; set; }

        public string Agent { get; set; }
        public DateTime GetDate()
        {
            DateTime date = DateTime.Parse(Date);
            return date;
        }

        
    }
}
