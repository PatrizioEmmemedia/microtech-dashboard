﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.Database.Models
{
    public class Agent
    {
        public string ATA02 { get; set; }
        public string Codice_ACG { get; set; }
        public string Descrizione { get; set; }
        public string CodiceFornitore { get; set; }
        public string Email { get; set; }
        public string Cellulare { get; set; }
    }
}
