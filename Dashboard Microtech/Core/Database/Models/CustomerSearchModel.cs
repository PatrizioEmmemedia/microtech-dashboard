﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.Database.Models
{
    public class CustomerSearchModel
    {
        public CustomerSearchModel() { }
        public List<Customer> customers { get; set; }
        public string provincia { get; set; }
        public string citta { get; set; }
        public int type { get; set; }

        public int startItem { get; set; }
    }
}
