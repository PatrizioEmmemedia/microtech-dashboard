﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.Database.Models
{
    public class SyntheticCustomer
    {
        public string PK { get; set; }
        public string AS400_ID { get; set; }
        public string AS400_CODE { get; set; }
        public string CompanyName { get; set; }
    }
}
