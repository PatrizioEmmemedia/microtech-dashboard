﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.Database.Models
{
  public class SelectResult
        {
            public int PK { get; set; }
            public decimal value { get; set; }
            public string companyName { get; set; }
            public string date { get; set; }
            public string code { get; set; }
            public string id { get; set; }
        }
}
