﻿using Microtech_Core.WebServices.Services.Misc.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.Database.Models
{
    public class Customer
    {
        public int PK { get; set; }
        public int AS400_ID { get; set; }
        public int AS400_CODE { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string CityFK { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string CAP { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
        public string FiscalCode { get; set; }
        public string VATNumber { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Fax { get; set; }
        public string Newsletter { get; set; }
        public int Discount { get; set; }
        public int CustomerType { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public string LockoutEndDateUtc { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public string DateAdd { get; set; }
        public string DateLastAccess { get; set; }
        public bool Active { get; set; }
        public bool isItalian { get; set; }
        public string AGENT_CODE_AS400 { get; set; }
        public string InvoiceCustomer { get; set; }
        public string Descrizione { get; set; }

        
    }
}
