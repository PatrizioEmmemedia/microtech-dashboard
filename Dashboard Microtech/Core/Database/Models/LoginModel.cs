﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.Database.Models
{
    public class LoginModel
    {
        public string code { get; set; }
        public string password { get; set; }
    }
}
