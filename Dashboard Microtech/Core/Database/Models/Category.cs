﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.Database.Models
{
    public class Category
    {
        public int PK { get; set; }
        public string ParentID { get; set; }
        [JsonProperty(PropertyName = "Name")]

        public string Name { get; set; }
        public string Description { get; set; }
        public string AS400_CODE { get; set; }
    }
}
