﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.Database.Models
{
    public class AddOrderQuery
    {
        public char azi { get; set; }
        public string pre { get; set; }
        public int ido { get; set; }
        public string dto { get; set; }
        public string cli { get; set; }
        public string cig { get; set; }
        public string cup { get; set; }
        public string dti { get; set; }
        public string dtf { get; set; }
        public int nrg { get; set; }
        public int nrc { get; set; }
        public char tpr { get; set; }
        public string art { get; set; }
        public int qty { get; set; }
        public string prz { get; set; }
        public char tpm { get; set; }
    }
}
