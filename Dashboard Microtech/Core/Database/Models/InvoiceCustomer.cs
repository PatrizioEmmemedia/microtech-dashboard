﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.Database.Models
{
    public class InvoiceCustomer
    {
        public int Fatturazione { get; set; }
        public string CompanyName { get; set; }
        public int ID { get; set; }
        public string Dipartimento { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string CAP { get; set; }
        public string Province { get; set; }
        public string City { get; set; }
        public int PK { get; set; }
    }
}
