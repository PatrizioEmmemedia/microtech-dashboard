﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.Database.Models
{
    public class ListByAgent
    {
        public string CompanyName { get; set; }
        public int PK { get; set; }
        public decimal Price { get; set; }
        public string Date { get; set; }
        public string Agent { get; set; }
        public string AS400_CODE { get; set; }
        public string AS400_ID { get; set; }
    }
}
