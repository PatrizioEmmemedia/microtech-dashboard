﻿using Microtech_Core.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.Mail
{
    public class Components
    {
        public Components() { }
        public string ToMail { get; set; }
        public string toName { get; set; }
        public string subject { get; set; }

        public string body { get; set; }

        public Agent agent { get; set; }
        public string MailBodyBuilder(string cta)
        {
            String body = @"<!doctype html>
<html xmlns=""http://www.w3.org/1999/xhtml"" xmlns:v=""urn:schemas-microsoft-com:vml"" xmlns:o=""urn:schemas-microsoft-com:office:office"">
<head>
    <meta charset=""UTF-8"">
    <meta http-equiv=""X-UA-Compatible"" content=""IE=edge"">
    <meta name=""viewport"" content=""width=device-width, initial-scale=1"">
    <title>Microtech</title>
    <link href=""http://fonts.googleapis.com/css?family=Ubuntu:regular,italic,700"" rel=""stylesheet"" type=""text/css"">
</head>
<body style=""padding:0; margin:0;"">
<center>
    <table bgcolor=""#ffffff"" align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" height=""100%"" width=""100%"" style=""font-family:'Ubuntu', Arial, Sans-serif; font-size:14px; line-height:1.6; color:#333333;"">
        <tr>
            <td align=""center"" valign=""top"">

                <!-- Header -->
                <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" bgcolor=""#ffffff"">
                    <tr>
                        <td align=""center"" valign=""top"">
                            <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""600"">
                                <tr align=""left"">
                                    <td align=""top"" style=""padding-top:50px; padding-bottom:90px;"">
                                        <a href=""http://www.microtech.eu/"" target=""_blank"" title=""Microtech"">
											<img src=""https://microtechdesk.emmemedia.net/css/custom/dist/img/logo.svg"" alt=""Microtech"" width=""180"" />
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

				<!-- Body -->
				<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" bgcolor=""#ffffff"">
					<tr>
						<td align=""center"" valign=""top"">

							<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""600"">
								<tr align=""left"">
									<td>

										<span style=""font-family:'Ubuntu', Arial, Sans-serif; color:#333333;"">
											<span style=""font-size:18px;""><strong>Microtech ti informa</strong></span><br/>
											<span style=""font-size:14px; line-height:1.6;"">
												che il tuo preventivo è stato registrato con successo. 
											</span>
										</span>

										<!-- Hr divider -->
										<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""min-width:100%;"">
											<tbody>
											<tr>
												<td style=""min-width:100%; padding:50px 0;"">
													<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""min-width:100%; border-top:1px solid #cccccc;"">
														<tbody><tr><td><span></span></td></tr></tbody>
													</table>
												</td>
											</tr>
											</tbody>
										</table>

										<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"">
											<tr>
												<td style=""padding-top:20px; padding-bottom:40px;"">
													<span style=""font-family:'Ubuntu', Arial, Sans-serif; color:#333333; font-size:14px; line-height:1.6;"">
														Adesso puoi stampare il preventivo cliccando sul bottone in basso
													</span>
												</td>
											</tr>
										</table>

										<!-- Button -->
										<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"">
											<tr>
												<td style=""padding-bottom:20px;"">
													<table align=""left"" border=""0"" cellpadding=""0"" cellspacing=""0"" style=""border-collapse:separate !important; background-color:#D2D946;"">
														<tbody>
														<tr>
															<td align=""center"" valign=""middle"" style=""font-family:'Ubuntu', Arial, Sans-serif; font-size:12px; padding:13px;"">
																<a target=""_blank"" style=""font-weight:normal; text-transform:uppercase; letter-spacing:0.1em; line-height:100%; text-align:center; text-decoration:none; color:#04446C;"" href=""" + cta + @""">Stampa</a>
															</td>
														</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</table>

									</td>
								</tr>
							</table>

						</td>
					</tr>
				</table>

				<!-- footer -->
				<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" bgcolor=""#ffffff"">
					<tr>
						<td align=""center"" valign=""top"">

							<!-- Hr divider -->
							<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""600"">
								<tbody>
								<tr>
									<td style=""min-width:100%; padding:50px 0;"">
										<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""min-width:100%; border-top:1px solid #cccccc;"">
											<tbody><tr><td><span></span></td></tr></tbody>
										</table>
									</td>
								</tr>
								</tbody>
							</table>

							<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""600"">
								<tr align=""left"">
									<td align=""left"" width=""75%"">
										<span style=""font-family:'Ubuntu', Arial, Sans-serif; color:#333333; font-size:14px; line-height:1.6;"">
											Grazie,<br>
											<strong>il team di Microtech</strong>.
										</span>
									</td>
								</tr>
							</table>

							<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""600"">
								<tr align=""center"">
									<td align=""center"" style=""padding-top:80px; padding-bottom:50px;"">
										<span style=""font-family:'Ubuntu', Arial, Sans-serif; color:#999999; font-size:12px; line-height:1.5;"">
											Microtech S.R.L.
										</span>
									</td>
								</tr>
							</table>

						</td>
					</tr>
				</table>

			</td>
		</tr>
	</table>
</center>
</body>
</html>";
            return body;
        }
    }

}
