﻿
using MailKit.Net.Smtp;
using MimeKit;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.Mail
{
    public class Mail
    {
        Components mailComp;
        public Mail(Components mailComp)
        {
            this.mailComp = mailComp;

        }
        async public void SendMail()
        {
            var message = new MimeMessage();
            
            message.From.Add(new MailboxAddress("Microtech Management", "no-reply@microtech.eu"));
            message.To.Add(new MailboxAddress(mailComp.toName, mailComp.ToMail));
            message.To.Add(new MailboxAddress(mailComp.agent.Descrizione, mailComp.agent.Email));
            message.Subject = mailComp.subject;
            var bodyBuilder = new BodyBuilder();
            bodyBuilder.HtmlBody = mailComp.body;

            message.Body = bodyBuilder.ToMessageBody();
            using (var client = new SmtpClient())
            {
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                client.Connect("smtps.aruba.it", 465, true);
                client.Authenticate("clienti@emmemedia.com", "727clienti");
                await client.SendAsync(message);
            }
            

        }

        
    }
}
