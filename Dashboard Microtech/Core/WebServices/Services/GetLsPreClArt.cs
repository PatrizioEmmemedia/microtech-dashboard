﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.WebServices.Services
{
    public class GetLsPreClArt
    {
        public string cli { get; set; }
        public string art { get; set; }

        public GetLsPreClArt(string cli, string art)
        {
            this.cli = cli;
            this.art = art;
        }
        public List<Pojo.GetLSPreBP> getLSPreAsync()
        {
            WebService ws = new WebService("http://2.229.69.115:8015/www/dacgi/WSGPrClArt.pgm", "ListPre");
            ws.Params.Add("cli", cli);
            ws.Params.Add("art", art);

            string wsResult = ws.InvokeGet(false);

            System.Diagnostics.Debug.WriteLine(wsResult);
            if(wsResult == "[")
            {
                return null;
            }
            List<Pojo.GetLSPreBP> getLsOrd = JsonConvert.DeserializeObject<List<Pojo.GetLSPreBP>>(wsResult);

            return getLsOrd;

        }
    }
}
