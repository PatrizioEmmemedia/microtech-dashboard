﻿using Microtech_Core.WebServices.Services.Pojo;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.WebServices.Services
{
    public class GetVenService
    {
        public string doc { get; set; }
        public string aad { get; set; }

        public GetVenService(string doc, string aad)
        {
            this.doc = doc;
            this.aad = aad;
        }

        public List<Pojo.GetVen> getDocAsync()
        {
            //http://2.229.69.115:8015/www/dacgi/WSGetLsVen.pgm?cli=3610
            WebService ws = new WebService("http://2.229.69.115:8015/www/dacgi/WSGetVen.pgm", "Ven");
            ws.Params.Add("doc", doc);
            ws.Params.Add("aad", aad);
            string wsResult = ws.InvokeGet(false);

            System.Text.StringBuilder reformatJSON = new System.Text.StringBuilder(wsResult);
            reformatJSON[0] = '[';
            reformatJSON[reformatJSON.Length - 1] = ']';
            reformatJSON.Replace("\"Sales Document\",", "");

            List<GetVen> obj = JsonConvert.DeserializeObject<List<GetVen>>(reformatJSON.ToString());


            return obj;
        }
    }
}
