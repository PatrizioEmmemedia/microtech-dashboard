﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.WebServices.Services
{
    public class GetLsPreService
    {
        public string cli;
        public string apiUrl;
        public GetLsPreService(string cli, string apiUrl)
        {
            this.cli = cli;
            this.apiUrl = apiUrl;
        }
        public List<Pojo.GetLsPre> getLSPreAsync()
        {
            WebService ws = new WebService(apiUrl, "ListPre");
            ws.Params.Add("cli", cli);

            string wsResult = ws.InvokeGet(false);
            wsResult = wsResult.Replace("\"Quotations\",", "");
            System.Diagnostics.Debug.WriteLine(wsResult);
            if (wsResult == "[\"Quotations\"]")
            {
                return null;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(wsResult);
                List<Pojo.GetLsPre> getLsOrd = JsonConvert.DeserializeObject<List<Pojo.GetLsPre>>(wsResult.ToString());

                return getLsOrd;
            }
            
        }
    }
}
