﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Microtech_Core.WebServices.Services
{
    public class GetOrderService
    {
        public string pre, date, ordId, apiUrl;

        public GetOrderService(string pre, string date, string ordId, string apiUrl)
        {
            this.pre = pre;
            this.date = date;
            this.ordId = ordId;
            this.apiUrl = apiUrl;
        }
        public Pojo.GetOrd GetOrdAsync()
        {
            WebService ws = new WebService(apiUrl, "ListOrd");

            var year = date.Substring(0, 4);
            var month = date.Substring(4, 2);
            System.Diagnostics.Debug.WriteLine(year + " " + month);
            /*if (int.Parse(month) >= 2 && int.Parse(year) >= 2019)
            {
                switch (pre)
                {
                    case "000":
                        pre = "WOS";
                        break;
                    case "001":
                        pre = "WOP";
                        break;
                    default:
                        break;
                }
            }*/
            ws.Params.Add("pre", "000");
            ws.Params.Add("ido", ordId);
            ws.Params.Add("dto", date);

            string wsResult = ws.InvokeGet(false);

            System.Diagnostics.Debug.WriteLine(wsResult);

            int Place = wsResult.IndexOf("\"Riga Ordine\":");

            wsResult = wsResult.Remove(Place, "\"Riga Ordine\":".Length).Insert(Place, "\"Righe Ordini\":[");

            System.Text.StringBuilder reformatJSON = new System.Text.StringBuilder(wsResult);

            reformatJSON.Remove(reformatJSON.Length - 1, 1).Insert(reformatJSON.Length - 1, "}]");

            reformatJSON.Replace("\"Riga Ordine\":", "");

            
            //System.Diagnostics.Debug.WriteLine(reformatJSON);
            Pojo.GetOrd getOrd = JsonConvert.DeserializeObject<Pojo.GetOrd>(reformatJSON.ToString());

            return getOrd;
        }

    }
}
