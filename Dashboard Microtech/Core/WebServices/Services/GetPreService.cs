﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.WebServices.Services
{
    public class GetPreService
    {
        public string pre;
        public string apiUrl;
        public GetPreService(string pre, string apiUrl)
        {
            this.pre = pre;
            this.apiUrl = apiUrl;
        
        }
        public List<Pojo.GetPre> getPreAsync()
        {
            WebService ws = new WebService(apiUrl, "ListPre");
            ws.Params.Add("pre", pre);

            string wsResult = ws.InvokeGet(false);
            wsResult = wsResult.Replace("\"Price Quotations\",", "");
            wsResult = wsResult.Replace("\"Price Quotations\"", "");
            System.Diagnostics.Debug.WriteLine(wsResult);
            if(pre != "undefined" && wsResult != "[]")
            {
                List<Pojo.GetPre> getPre = JsonConvert.DeserializeObject<List<Pojo.GetPre>>(wsResult.ToString());
                return getPre;
            }
            else
            {
                return null;
            }

            
        }
    }
}
