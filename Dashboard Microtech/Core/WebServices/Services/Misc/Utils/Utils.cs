﻿using Microtech_Core.Database;
using Microtech_Core.Database.Functions;
using Microtech_Core.Database.Models;
using Microtech_Core.WebServices.Services.Pojo;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;

namespace Microtech_Core.WebServices.Services.Misc.Utils
{
    public class Utils
    {
        public static Pojo.GetPrice getPrice(string productId, string clid)
        {
            string date = DateTime.Today.ToString("yyyyMMdd");
            System.Diagnostics.Debug.WriteLine(date);

            clid = "8305";
            GetPriceService service = new GetPriceService(productId, clid, date, "http://2.229.69.115:8015/www/dacgi/WSGetPrice.pgm");
            GetPrice price = service.GetPriceAsync();

            return price;
        }

        public static string getPre(string productId)
        {
            GetPreService service = new GetPreService(productId, "http://2.229.69.115:8015/www/dacgi/WSGetPre.pgm");
            List<GetPre> pre = service.getPreAsync();
            return JsonConvert.SerializeObject(pre);
        }
        public static string getOrd(string productId, string date, string pre)
        {
            GetOrderService service = new GetOrderService(pre, date, productId, "http://2.229.69.115:8015/www/dacgi/WSGetOrd.pgm");
            GetOrd order = service.GetOrdAsync();
            
            return JsonConvert.SerializeObject(order);
        }
        public static string getList(string cli)
        {
            SqlService service = new SqlService();
            service.init();

            SqlDataReader reader = service.query("SELECT Listini.*, Product.Name, Product.SKU FROM Listini INNER JOIN Product ON Listini.AS400_ID = " + cli + " AND Product.PK = Listini.ProductKey");

            List<Listino> list = new List<Listino>();

            while (reader.Read())
            {
                Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
                var product = SQLMapping.MapListino(column);
                list.Add(product);
            }
            service.close();
            return JsonConvert.SerializeObject(list);
        }
        public static List<Customer> getCustomers(string aCode)
        {
            SqlService service = new SqlService();
            service.init();

            SqlDataReader reader = service.query("SELECT * FROM Customer WHERE AGENT_CODE_AS400 = '" + aCode + "'");

            List<Customer> list = new List<Customer>();

            while (reader.Read())
            {
                Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
                var customer = SQLMapping.MapCustomer(column);
                list.Add(customer);
            }
            service.close();
            return list;
        }
        public static Customer GetCustomer(string id)
        {
            SqlService service = new SqlService();
            service.init();

            SqlDataReader reader = service.query("SELECT * FROM Customer WHERE AS400_ID = '" + id + "'");
            System.Diagnostics.Debug.WriteLine("SELECT * FROM Customer WHERE AS400_ID = '" + id + "'");
            while (reader.Read())
            {
                Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
                var customer = SQLMapping.MapCustomer(column);
                System.Diagnostics.Debug.WriteLine(JsonConvert.SerializeObject(customer));
                service.close();
                return customer;
            }
            return null;
        }

        public static Customer GetCustomerNameByCode(string as400_code)
        {
            SqlService service = new SqlService();
            service.init();

            SqlDataReader reader = service.query("SELECT CompanyName FROM Customer WHERE AS400_CODE = '" + as400_code + "'");

            while (reader.Read())
            {
                Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
                var customer = SQLMapping.MapCustomer(column);
                service.close();

                return customer;
            }
            
            return null;
        }
        public static Customer GetCustomerNameById(string as400_id)
        {
            SqlService service = new SqlService();
            service.init();

            SqlDataReader reader = service.query("SELECT CompanyName FROM Customer WHERE AS400_ID = '" + as400_id + "'");

            while (reader.Read())
            {
                Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
                var customer = SQLMapping.MapCustomer(column);
                service.close();

                return customer;
            }

            return null;
        }
        public static string getDate(string dateString)
        {
            DateTime date = DateTime.ParseExact(dateString, "yyyyMMdd", CultureInfo.InvariantCulture);
            return date.ToString("dd/MM/yyyy");
        }
        public static List<Agent> GetAgents()
        {
            SqlService service = new SqlService();
            service.init();

            SqlDataReader reader = service.query("SELECT * FROM Agenti_as400");
            List<Agent> agents = new List<Agent>();
            while (reader.Read())
            {
                Dictionary<String, String> column = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
                Agent agent = SQLMapping.mapAgent(column);
                agents.Add(agent);
            }

            service.close();

            return agents;
        }
    }
}
