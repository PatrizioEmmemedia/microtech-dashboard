﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.WebServices.Services
{
    public class SendOrdService
    {
            
            string apiUrl { get; set; }
            String[] split1;
            public SendOrdService(string apiUrl)
            {
                
                this.apiUrl = apiUrl;
                split1 = apiUrl.Split('&');
                
                
            }

            public string sendOrderAsync()
            {

                WebService ws = new WebService("http://2.229.69.115:8015/www/dacgi/WSPostDoc.pgm", "WSPostDoc");
                List<String> addedPre = new List<String>();
                foreach (var value in split1)
                {
                    
                    var splittedValue = value.Split('=');
                    if (!addedPre.Contains(splittedValue[0]))
                    {
                        ws.Params.Add(splittedValue[0], splittedValue[1]);
                        addedPre.Add(splittedValue[0]);
                        System.Diagnostics.Debug.WriteLine(value);
                    }
                    
                }

            System.Diagnostics.Debug.WriteLine("URL: " + JsonConvert.SerializeObject(ws.Params));
                string wsResult = ws.InvokeGet(false);
                System.Diagnostics.Debug.WriteLine("RISULTATO: " + wsResult);
                return wsResult;

            }
        }
   
}
