﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microtech_Core;
using Newtonsoft.Json;

namespace Microtech_Core.WebServices.Services
{
    public class GetPriceService
    {
        string idart { get; set; }
        string idcli { get; set; }
        string dta { get; set; }

        string apiUrl { get; set; }

        public GetPriceService(string idart, string idcli, string dta, string apiUrl)
        {
            this.idart = idart;
            this.idcli = idcli;
            this.dta = dta;
            this.apiUrl = apiUrl;

        }

        public Pojo.GetPrice GetPriceAsync()
        {
            WebService ws = new WebService(apiUrl, "GetPrice");
            ws.Params.Add("art", idart);
            ws.Params.Add("cli", idcli);
            ws.Params.Add("dto", dta);
            System.Diagnostics.Debug.WriteLine("DATA: " + dta);
            string wsResult = ws.InvokeGet(false);

            Pojo.GetPrice getPrice = JsonConvert.DeserializeObject<Pojo.GetPrice>(wsResult);
            return getPrice;

        }
    }
}
