﻿using Microtech_Core.WebServices.Services.Pojo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Microtech_Core.WebServices.Services
{
    public class PDFService
    {
        PDFBody order;
        public PDFService(PDFBody order)
        {
            this.order = order;
        }

        public async Task<string> GetPDF()
        {
            /*WebService ws = new WebService("http://138.68.100.112:3999/", "PDF");

            ws.Params.Add("order", order);

            ws.InvokePost(false);*/

            using (var client = new HttpClient())
            {
                var request = await client.PostAsJsonAsync(
                    "http://46.254.39.88:3999/", order
                    );

                System.Diagnostics.Debug.WriteLine(request.Content.ReadAsStringAsync());
                return await request.Content.ReadAsStringAsync();

            }
        }
    }
}

