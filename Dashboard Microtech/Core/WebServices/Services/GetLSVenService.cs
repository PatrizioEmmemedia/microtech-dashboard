﻿using Microtech_Core.WebServices.Services.Pojo;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.WebServices.Services
{
    public class GetLSVenService
    {
        public string cli { get; set; }

        public GetLSVenService(string cli)
        {
            this.cli = cli;
        }

        public List<Pojo.GetLSVen> getDocAsync()
        {
            //http://2.229.69.115:8015/www/dacgi/WSGetLsVen.pgm?cli=3610
            WebService ws = new WebService("http://2.229.69.115:8015/www/dacgi/WSGetLsVen.pgm", "ListOrd");
            ws.Params.Add("cli", cli);
            string wsResult = ws.InvokeGet(false);

            System.Text.StringBuilder reformatJSON = new System.Text.StringBuilder(wsResult);
            reformatJSON[0] = '[';
            reformatJSON[reformatJSON.Length - 1] = ']';
            reformatJSON.Replace("\"Customer Documents\",", "");
            reformatJSON.Replace("\"Customer Documents\"", "");
            System.Diagnostics.Debug.WriteLine(reformatJSON);
            try
            {
                List<GetLSVen> obj = JsonConvert.DeserializeObject<List<GetLSVen>>(reformatJSON.ToString());
                return obj;
            } catch(Exception e)
            {
                return null;
            }
            


            
        }
    }
}
