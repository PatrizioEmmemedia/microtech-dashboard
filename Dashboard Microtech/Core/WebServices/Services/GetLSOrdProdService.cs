﻿using Microtech_Core.WebServices.Services.Pojo;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.WebServices.Services
{
    public class GetLSOrdProdService
    {
        public string cli;
        public string art;
        public string apiUrl;
        public GetLSOrdProdService(string cli, string art, string apiUrl)
        {
            this.cli = cli;
            this.art = art;
            this.apiUrl = apiUrl;
        }

        public List<GetLsOrd> GetLSOrdAsync()
        {
            WebService ws = new WebService(apiUrl, "ListOrd");
            ws.Params.Add("cli", cli);
            ws.Params.Add("art", art);

            string wsResult = ws.InvokeGet(false);
            wsResult = wsResult.Replace("Price", "Value Ordered");
            System.Diagnostics.Debug.WriteLine(wsResult);
            if(wsResult.Length > 1)
            {
                List<Pojo.GetLsOrd> getLsOrd = JsonConvert.DeserializeObject<List<Pojo.GetLsOrd>>(wsResult);
                return getLsOrd;
            }
            else
            {
                return null;
            }
            
        }
    }
}
