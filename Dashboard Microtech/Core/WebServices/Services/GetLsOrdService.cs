﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.WebServices.Services
{
    public class GetLsOrdService
    {
        public string idcli { get; set; }
        public string pref { get; set; }
        public string apiUrl { get; set; }
        public string ido { get; set; }

        public GetLsOrdService(string idcli, string pref, string ido, string apiUrl)
        {
            this.idcli = idcli;
            this.pref = pref;
            this.apiUrl = apiUrl;
            this.ido = ido;
        }

        public List<Pojo.GetLsOrd> GetLsOrdAsync()
        {
            WebService ws = new WebService("http://2.229.69.115:8015/www/dacgi/WSGetLsOrd.pgm", "ListOrd");
            if(idcli == "undefined")
            {
                ws.Params.Add("cli", "");
            }
            else
            {
                ws.Params.Add("cli", idcli);
            }
            
            ws.Params.Add("ido", ido);
            ws.Params.Add("pre", pref);


            System.Diagnostics.Debug.WriteLine(ws.Url + "?" + "cli=" + ws.Params["cli"] + "&" + "pre=" + ws.Params["pre"] + "&" + "ido=" + ws.Params["ido"]);
            string wsResult = ws.InvokeGet(false);

            System.Text.StringBuilder reformatJSON = new System.Text.StringBuilder(wsResult);
            reformatJSON[0] = '[';
            reformatJSON[reformatJSON.Length - 1] = ']';
            reformatJSON.Replace("\"Order\",", "");

            System.Diagnostics.Debug.WriteLine(reformatJSON);
            if(reformatJSON.ToString().Contains("ORDER NOT FOUND"))
            {
                return null;
            }
            List<Pojo.GetLsOrd> getLsOrd = JsonConvert.DeserializeObject<List<Pojo.GetLsOrd>>(reformatJSON.ToString());

            return getLsOrd;
        }
    }
    
}
