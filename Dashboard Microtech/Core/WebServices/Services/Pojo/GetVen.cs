﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.WebServices.Services.Pojo
{
    public class GetVen
    {
        [JsonProperty(PropertyName = "Id interno")]
        public string id { get; set; }
        [JsonProperty(PropertyName = "Data")]
        public string date { get; set; }
        [JsonProperty(PropertyName = "Valore Totale")]
        public string price { get; set; }
        [JsonProperty(PropertyName = "Codice")]
        public string sku { get; set; }
        [JsonProperty(PropertyName = "Descriz")]
        public string name { get; set; }
        [JsonProperty(PropertyName = "Order Number")]
        public string orderId { get; set; }
        [JsonProperty(PropertyName = "Nome cli sped")]
        public string customer { get; set; }
        
    }
}
