﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.WebServices.Services.Pojo
{
    public class GetLSPreBP
    {
        [JsonProperty(PropertyName = "Sales Description")]
        public string desc { get; set; }
        [JsonProperty(PropertyName = "Quote Number")]
        public string id { get; set; }
        [JsonProperty(PropertyName = "Quote Date")]
        public string date { get; set; }
        [JsonProperty(PropertyName = "Quote Row Number")]
        public string rowNum { get; set; }
        [JsonProperty(PropertyName = "Articolo")]
        public string sku { get; set; }
        [JsonProperty(PropertyName = "Desc.Art.")]
        public string descrArt { get; set; }
        [JsonProperty(PropertyName = "Cod.cli sped")]
        public string delId { get; set; }
        [JsonProperty(PropertyName = "Price")]
        public string price { get; set; }
        [JsonProperty(PropertyName = "Nome cli sped")]
        public string name { get; set; }

        public string status = "";
        public string getDate()
        {
            DateTime parsedDate = DateTime.Parse(date.Insert(4, "-").Insert(7, "-"));
            return parsedDate.ToString("dd/MM/yyyy");
        }
        public string getValue()
        {
            decimal value = decimal.Parse(price.Replace('.',','));
            return value.ToString("#.00", CultureInfo.CreateSpecificCulture("it-IT"));
        }
        
    }
}
