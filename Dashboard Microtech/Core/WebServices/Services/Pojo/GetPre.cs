﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Microtech_Core.WebServices.Services.Pojo
{
    public class GetPre
    {
        [JsonProperty(PropertyName = "Id interno")]
        public int id { get; set; }
        [JsonProperty(PropertyName = "Data")]
        public string data { get; set; }
        [JsonProperty(PropertyName = "Causale")]
        public string causale { get; set; }
        [JsonProperty(PropertyName = "Listino")]
        public string listino { get; set; }
        [JsonProperty(PropertyName ="Id Cli Spediz.")]
        public string idDel { get; set; }
        [JsonProperty(PropertyName = "Id Cli Fattura")]
        public string idInv { get; set; }
        [JsonProperty(PropertyName = "Divisa")]
        public string divisa { get; set; }
        [JsonProperty(PropertyName = "Valore Totale")]
        public string priceTot { get; set; }
        [JsonProperty(PropertyName = "Riga")]
        public string nRiga { get; set; }
        [JsonProperty(PropertyName = "Id Articolo")]
        public string idProduct { get; set; }
        [JsonProperty(PropertyName = "Quantita")]
        public string qty { get; set; }
        [JsonProperty(PropertyName = "Valore riga")]
        public string price { get; set; }
        [JsonProperty(PropertyName = "Prezzo")]
        public string prezzo { get; set; }
        [JsonProperty(PropertyName = "Descr")]
        public string productName { get; set; }
        [JsonProperty(PropertyName = "Codice")]
        public string sku { get; set; }
        [JsonProperty(PropertyName = "Cod.cli sped")]
        public string codSped { get; set; }
        [JsonProperty(PropertyName = "Cod.cli fattura")]
        public string codFatt { get; set; }
        [JsonProperty(PropertyName = "Nome cli sped")]
        public string cliSped { get; set; }
        public string GetTotal()
        {
            return Decimal.Parse(priceTot.Replace('.', ',')).ToString(CultureInfo.CreateSpecificCulture("it-IT"));
        }
    }
}
