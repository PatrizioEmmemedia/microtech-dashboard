﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.WebServices.Services.Pojo
{
    public class GetLSVen
    {
        [JsonProperty(PropertyName = "Doc.Description")]
        public string desc { get; set; }
        [JsonProperty(PropertyName = "Doc.Number")]
        public string number { get; set; }
        [JsonProperty(PropertyName = "Doc.Date")]
        public string date { get; set; }
        [JsonProperty(PropertyName = "Ship_to_id")]
        public string shipId { get; set; }
        [JsonProperty(PropertyName = "Ship_to_code")]
        public string shipCode { get; set; }
        [JsonProperty(PropertyName = "Ship_to_name")]
        public string shipName { get; set; }
        [JsonProperty(PropertyName = "Bill_to_id")]
        public string billId { get; set; }
        [JsonProperty(PropertyName = "Bill_to_code")]
        public string billCode { get; set; }
        [JsonProperty(PropertyName = "Bill_to_name")]
        public string billName { get; set; }
        [JsonProperty(PropertyName = "Doc.Value")]
        public string value { get; set; }
        [JsonProperty(PropertyName = "Delivery Group")]
        public string delGroup { get; set; }
        [JsonProperty(PropertyName = "Delivery Date")]
        public string delDate { get; set; }
        [JsonProperty(PropertyName = "Delivery Number")]
        public string delNumber { get; set; }
        [JsonProperty(PropertyName = "Invoice Number")]
        public string invNumber { get; set; }
        [JsonProperty(PropertyName = "Invoice Date")]
        public string invDate { get; set; }
        [JsonProperty(PropertyName = "Order Prefix")]
        public string prefix { get; set; }
        [JsonProperty(PropertyName = "Order Number")]
        public string ordNumber { get; set; }
        [JsonProperty(PropertyName = "Currency")]
        public string currency { get; set; }

        public string getDate(string dateString)
        {
            try
            {
                DateTime date = DateTime.ParseExact(dateString, "yyyyMMdd", CultureInfo.InvariantCulture);
                return date.ToString("dd/MM/yyyy");
            } catch(Exception e)
            {
                return null;
            }
        }
    }
}
