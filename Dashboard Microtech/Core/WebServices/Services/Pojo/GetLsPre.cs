﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microtech_Core.Database.Models;
using Microtech_Core.WebServices.Services.Misc.Utils;
using Newtonsoft.Json;
namespace Microtech_Core.WebServices.Services.Pojo
{
    public class GetLsPre
    {
        [JsonProperty(PropertyName = "Quot.Description")]
        public string desc { get; set; }
        [JsonProperty(PropertyName = "Quot.Number")]
        public string id { get; set; }
        [JsonProperty(PropertyName = "Quot.Date")]
        public string date { get; set; }
        [JsonProperty(PropertyName ="Quot.Status")]
        public int status { get; set; }
        [JsonProperty(PropertyName = "Ship_to_code")]
        public int delId { get; set; }
        [JsonProperty(PropertyName = "Quot.Value")]
        public string price { get; set; }
        [JsonProperty(PropertyName = "Currency")]
        public string currency { get; set; }
        [JsonProperty(PropertyName = "Nome cli sped")]
        public string name { get; set; }
        public string getDate()
        {
            DateTime parsedDate = DateTime.Parse(date.Insert(4, "-").Insert(7, "-"));
            return parsedDate.ToString("dd/MM/yyyy");
        }
        public string getValue()
        {
            return price.ToString(CultureInfo.CreateSpecificCulture("it-IT"));
        }

       
    }
}
