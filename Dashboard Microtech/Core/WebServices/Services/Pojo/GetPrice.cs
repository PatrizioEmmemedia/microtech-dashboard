﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microtech_Core.WebServices.Services.Pojo
{
    public class GetPrice
    {
        [JsonProperty(PropertyName = "codice articolo")]
        public string idart { get; set; }
        [JsonProperty(PropertyName = "codice cliente")]
        public string idcli { get; set; }
        [JsonProperty(PropertyName = "descrizione articolo")]
        public string articleName { get; set; }
        [JsonProperty(PropertyName = "disponibilità")]
        public string disponibilita { get; set; }
        public string giacenza { get; set; }
        [JsonProperty(PropertyName = "prz listino cliente")]
        public float prezzo { get; set; }
        [JsonProperty(PropertyName = "prz listino C1")]
        public float prezzo_c1 { get; set; }
        [JsonProperty(PropertyName = "prz listino 01")]
        public float prezzo_01 { get; set; }
        [JsonProperty(PropertyName = "prz listino E1")]
        public float prezzo_e1 { get; set; }
        [JsonProperty(PropertyName = "data next arrivo")]
        public string dtnext { get; set; }
        [JsonProperty(PropertyName = "qty next arrivo")]
        public string qtynext { get; set; }
        [JsonProperty(PropertyName = "lead time")]
        public string leadTime { get; set; }
        
        public string getNextDate()
        {
            DateTime date = DateTime.Today;
            return date.AddDays(int.Parse(leadTime)).ToString("dd/MM/yyyy");
        }
    }


}
