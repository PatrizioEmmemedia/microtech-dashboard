﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.WebServices.Services.Pojo
{
    public class ProductList
    {
        public string qty { get; set; }
        public string sku { get; set; }
        public string name { get; set; }
        public string price { get; set; }
        public string valore { get; set; }
        public string pz { get; set; }
    }

    public class PDFBody
    {
        public string customer { get; set; }
        public string address { get; set; }
        public string company { get; set; }
        public int idOrd { get; set; }
        public List<ProductList> products { get; set; }
        public string agent { get; set; }
    }
}
