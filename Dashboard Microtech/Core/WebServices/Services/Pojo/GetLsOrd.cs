﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.WebServices.Services.Pojo
{
    public class GetLsOrd
    {
            
        [JsonProperty(PropertyName = "Sales Description")]
        public string desc { get; set; }
        [JsonProperty(PropertyName = "Order Prefix")]
        public string pref { get; set; }
        [JsonProperty(PropertyName = "Order Number")]
        public string ordNum { get; set; }
        [JsonProperty(PropertyName = "Order Date")]
        public string ordDate { get; set; }
        [JsonProperty(PropertyName = "Order Status")]
        public int ordStatus { get; set; }
        [JsonProperty(PropertyName = "Customer ID")]
        public string customerId { get; set; }
        [JsonProperty(PropertyName = "Cod.cli sped")]
        public string customerSped { get; set; }
        [JsonProperty(PropertyName = "Nome cli sped")]
        public string customerName { get; set; }
        [JsonProperty(PropertyName = "Value Ordered")]
        public string valueOrd { get; set; }
        [JsonProperty(PropertyName = "Expecting Shipment Value")]
        public string shipValue { get; set; }
        [JsonProperty(PropertyName = "Valore Delivered")]
        public string deliveredValue { get; set; }


        public string customerCleanName = null;
        public string getDate()
        {
            DateTime parsedDate = DateTime.Parse(ordDate.Insert(4, "-").Insert(7, "-"));
            return parsedDate.ToString("dd/MM/yyyy");
        }
        public string getStatus()
        {
            switch (ordStatus)
            {
                case 0:
                    return "Ordine in corso";
                case 1:
                    return "Ordine in evasione";
                case 2:
                    return "Ordine saldato";
                default:
                    return "Ordine in corso";
            }
        }

        public List<GetLSVen> VenDocs = new List<GetLSVen>();
    }
}
