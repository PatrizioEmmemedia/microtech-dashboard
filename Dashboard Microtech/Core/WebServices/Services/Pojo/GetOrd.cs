﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microtech_Core.WebServices.Services.Pojo
{
    public class GetOrd
    {
        [JsonProperty(PropertyName="Causale Ordine Vendita")]
        public string causale { get; set; }
        [JsonProperty(PropertyName = "ID Cliente")]
        public string idCliente { get; set; }
        [JsonProperty(PropertyName = "Stato Testata")]
        public int statoTestata { get; set; }
        public string dta { get; set; }
        [JsonProperty(PropertyName = "Valore Ordinato")]
        public string ordVal { get; set; }
        [JsonProperty(PropertyName = "Valore Attesa Spedizione")]
        public string attesaVal { get; set; }
        [JsonProperty(PropertyName = "Valore Consegnato")]
        public string consegnatoVal { get; set; }
        [JsonProperty(PropertyName = "Righe Ordini")]
        public Order[] ordini { get; set; }
        
        public string getStatus()
        {
            switch (statoTestata)
            {
                case 0:
                    return "Ordine in corso";
                case 1:
                    return "Ordine in evasione";
                case 2:
                    return "Ordine saldato";
                default:
                    return "Ordine in corso";
            }
        }
    }

    public class Order
    {
        [JsonProperty(PropertyName = "ID Articolo")]
        public string idArt { get; set; }
        [JsonProperty(PropertyName = "Stato Riga")]
        public int status { get; set; }
        [JsonProperty(PropertyName = "Cod.cli sped")]
        public string codSped { get; set; }
        [JsonProperty(PropertyName = "Q.tà Ordinata")]
        public string ordQty { get; set; }
        [JsonProperty(PropertyName = "Q.tà Attesa Spedizione")]
        public string attesaQty { get; set; }
        [JsonProperty(PropertyName = "Q.tà Consegnata")]
        public string consegnaQty { get; set; }
        [JsonProperty(PropertyName = "Prezzo")]
        public string prezzo { get; set; }
        [JsonProperty(PropertyName = "Valore Ordinato")]
        public string ordVal { get; set; }
        [JsonProperty(PropertyName = "Valore Attesa Spedizione")]
        public string attesaVal { get; set; }
        [JsonProperty(PropertyName = "Valore Consegnato")]
        public string consegnaVal { get; set; }
        public string productName { get; set; }
        [JsonProperty(PropertyName = "Numero Riga")]
        public int numeroRiga { get; set; }
        public string sku { get; set; }

        public string getStatus()
        {
            switch (status)
            {
                case 0:
                    return "Ordine in corso";
                case 1:
                    return "Ordine in evasione";
                case 2:
                    return "Ordine saldato";
                default:
                    return "Ordine in corso";
            }
        }
    }
}
