﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace Microtech_Core
{
	internal class WebService
	{
		public string Url { get; set; }
		public string MethodName { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }
		public string SOAPAction { get; set; }
		public string WSSPasswordType { get; set; }

        public string URI { get; set; }

		public Dictionary<string, string> Params = new Dictionary<string, string>();

		public XDocument ResultXML;

		public string ResultString;

		public WebService()
		{
		}

		public WebService(string url, string methodName, string userName = "", string password = "",
			string soapAction = "", string wssPasswordType = "")
		{
			Url = url;
			MethodName = methodName;
			UserName = userName;
			Password = password;
			SOAPAction = soapAction;
			WSSPasswordType = wssPasswordType;
		}

		/// <summary>
		/// Invokes service
		/// </summary>
		public void Invoke()
		{
			Invoke(true);
		}

		/// <summary>
		/// Invokes service
		/// </summary>
		/// <param name="encode">Added parameters will encode? (default: true)</param>
		public void Invoke(bool encode)
		{
			string phrase = Guid.NewGuid().ToString();
			string tempPhrase = phrase.Replace("-", "");
			tempPhrase = tempPhrase.ToUpper();
			string userNameToken = "UsernameToken-" + tempPhrase;
			DateTime created = DateTime.Now;
			string createdStr = created.ToString("yyyy-MM-ddThh:mm:ss.fffZ");
			SHA1CryptoServiceProvider sha1Hasher = new SHA1CryptoServiceProvider();
			byte[] hashedDataBytes = sha1Hasher.ComputeHash(Encoding.UTF8.GetBytes(phrase));
			string nonce = Convert.ToBase64String(hashedDataBytes);
			string soapStr = "";

			if (WSSPasswordType == "PasswordText")
			{
				soapStr =
					@"<?xml version=""1.0"" encoding=""utf-8""?>
                <soap:Envelope xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"">
                    <soap:Header>
                        <wsse:Security soap:mustUnderstand=""true""
                                xmlns:wsse=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd""
                                xmlns:wsu=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"">
                            <wsse:UsernameToken wsu:Id=""";
				soapStr += userNameToken;
				soapStr += @""">
                            <wsse:Username>" + UserName + @"</wsse:Username>
                            <wsse:Password Type=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText"">" +
						   Password + @"</wsse:Password>
                            <wsse:Nonce EncodingType=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary"">" +
						   nonce + @"</wsse:Nonce>
                            <wsu:Created>" + createdStr + @"</wsu:Created>
                            </wsse:UsernameToken>
                        </wsse:Security>
                    </soap:Header>
                    <soap:Body>
                    <{0}>
                        {1}
                    </{0}>
                    </soap:Body>
                </soap:Envelope>";
			}
			else if (WSSPasswordType == "None")
			{
				soapStr =
					@"<?xml version=""1.0"" encoding=""utf-8""?>
                <soap:Envelope xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"">
                    <soap:Body>
                    <{0}>
                        {1}
                    </{0}>
                    </soap:Body>
                </soap:Envelope>";
			}

			HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
			req.Headers.Add("SOAPAction", SOAPAction);
			req.ContentType = "application/soap+xml;charset=\"utf-8\"";
			req.Accept = "application/soap+xml";
			req.Method = "POST";

			if (WSSPasswordType == "None")
			{
				NetworkCredential netCredential = new NetworkCredential(UserName, Password);
				byte[] credentialBuffer = new UTF8Encoding().GetBytes(UserName + ":" + Password);
				string auth = Convert.ToBase64String(credentialBuffer);
				req.Headers.Add("Authorization", "Basic " + auth);
			}

			ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(
				delegate (
					object
						sender,
					X509Certificate
						certificate,
					X509Chain
						chain,
					SslPolicyErrors
						sslPolicyErrors)
				{
					return
						true;
				});

			using (Stream stm = req.GetRequestStream())
			{
				string postValues = "";
				foreach (var param in Params)
				{
					if (encode)
					{
						postValues += string.Format("<{0}>{1}</{0}>", HttpUtility.UrlEncode(param.Key),
							HttpUtility.UrlEncode(param.Value));
					}
					else
					{
						postValues += string.Format("<{0}>{1}</{0}>", param.Key, param.Value);
					}
				}

				soapStr = string.Format(soapStr, MethodName, postValues);
				using (StreamWriter stmw = new StreamWriter(stm))
				{
					stmw.Write(soapStr);
				}
			}

			using (StreamReader responseReader = new StreamReader(req.GetResponse().GetResponseStream()))
			{
				string result = responseReader.ReadToEnd();
				ResultXML = XDocument.Parse(result);
				ResultString = result;
			}
		}

		public string InvokeGet(bool encode)
		{
			string querystringValues = "";
			foreach (var param in Params)
			{
				if (encode)
				{
					querystringValues += string.Format("{0}={1}", HttpUtility.UrlEncode(param.Key),
						HttpUtility.UrlEncode(param.Value));
				}
				else
				{
					querystringValues += string.Format("{0}={1}", param.Key, param.Value);
					querystringValues += "&";
				}
			}

			querystringValues = querystringValues.Remove(querystringValues.Length - 1, 1);

			HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url + "?" + querystringValues);
			req.Headers.Add("SOAPAction", "\"http://tempuri.org/" + MethodName + "\"");
			req.ContentType = "text/xml;charset=\"utf-8\"";
			req.Accept = "text/xml";
			req.Method = "GET";
            try
            {
                /*using (responseReader = new StreamReader(req.GetResponse().GetResponseStream()))
                {
                    string result = responseReader.ReadToEnd();
                    ResultString = result;
                    return ResultString;
                    // ResultXML = XDocument.Parse(result)
                }*/

                HttpClient client = new HttpClient();

                var uriString = Url + "?" + querystringValues;
                URI = uriString;
                var response = client.GetAsync(uriString).Result;

                using(HttpContent content = response.Content)
                {
                    Task<string> result = content.ReadAsStringAsync();
                    return result.Result;
                }
            }catch(WebException ex)
            {
                HttpClient client = new HttpClient();
                var response = client.GetAsync(Url + "?" + querystringValues).Result;

                using (HttpContent content = response.Content)
                {
                    Task<string> result = content.ReadAsStringAsync();
                    return result.Result;
                }
            }
            
            
		}

		/// <summary>
		/// Invokes service
		/// </summary>
		/// <param name="encode">Added parameters will encode? (default: true)</param>
		public void InvokePost(bool encode)
		{
			string soapStr = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + Environment.NewLine +
							 "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
                             Environment.NewLine + "               xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" " +
                             Environment.NewLine + " xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                             Environment.NewLine + " <soap:Body>" + Environment.NewLine +
							 " <{0} xmlns=\"http://tempuri.org/\">" + Environment.NewLine + " {1}" + Environment.NewLine +
							 " </{0}>" + Environment.NewLine + " </soap:Body>" + Environment.NewLine + " </soap:Envelope>";

			HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
			req.Headers.Add("SOAPAction", "\"http://tempuri.org/" + MethodName + "\"");
			req.ContentType = "text/xml;charset=\"utf-8\"";
			req.Accept = "text/xml";
			req.Method = "POST";

			using (Stream stm = req.GetRequestStream())
			{
				string postValues = "";
				foreach (var param in Params)
				{
					if (encode)
					{
						postValues += string.Format("<{0}>{1}</{0}>", HttpUtility.UrlEncode(param.Key),
							HttpUtility.UrlEncode(param.Value));
					}
					else
					{
						postValues += string.Format("<{0}>{1}</{0}>", param.Key, param.Value);
					}
				}

				soapStr = string.Format(soapStr, MethodName, postValues);
				using (StreamWriter stmw = new StreamWriter(stm))
				{
					stmw.Write(soapStr);
				}
			}

			using (StreamReader responseReader = new StreamReader(req.GetResponse().GetResponseStream()))
			{
				string result = responseReader.ReadToEnd();
				// ResultXML = XDocument.Parse(result)
				ResultString = result;
			}
		}

		public Task<string> InvokeGetAsync(bool encode)
		{
			try
			{
				string querystringValues = "";
				foreach (var param in Params)
				{
					if (encode)
					{
						querystringValues += string.Format("{0}={1}", HttpUtility.UrlEncode(param.Key),
							HttpUtility.UrlEncode(param.Value));
					}
					else
					{
						querystringValues += string.Format("{0}={1}", param.Key, param.Value);
						querystringValues += "&";
					}
				}

				querystringValues = querystringValues.Remove(querystringValues.Length - 1, 1);

				HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url + "?" + querystringValues);
				req.Headers.Add("SOAPAction", "\"http://tempuri.org/" + MethodName + "\"");
				req.ContentType = "text/xml;charset=\"utf-8\"";
				req.Accept = "text/xml";
				req.Method = "GET";
                
                try
                {
                    Task<WebResponse> task = Task.Factory.FromAsync(
                                                            req.BeginGetResponse,
                                                            asyncResult => req.EndGetResponse(asyncResult),
                                                            (object)null);
                    return task.ContinueWith(t => ReadStreamFromResponse(t.Result));
                }catch(WebException ex)
                {
                    return null;
                }
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}
		}

		private static string ReadStreamFromResponse(WebResponse response)
		{
			try
			{
				using (Stream responseStream = response.GetResponseStream())
				using (StreamReader sr = new StreamReader(responseStream))
				{
					string strContent = sr.ReadToEnd();
					return strContent;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}
		}
	}
}