﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.


function getCustomerCode() {
    let CLI;
    let customers = JSON.parse(sessionStorage.getItem("customers"));
    for (let i in customers) {
        let object = customers[i];
        if (object['CompanyName'] === $("#autoComplete").val()) {
            CLI = object["AS400_ID"];
        }
    }
    return CLI;
}
function generateProductFragment(jsonObject) {
   
    let openDOM = '<div class="products">\
    <table class="table search-products">\
    <thead>\
    <tr> <th>Nome</th> <th>SKU</th> <th>AS400_ID</th> <th>Azioni</th><tr>\
    </thead>\
    <tbody>\
    ';
    for (let index in jsonObject) {
        const object = jsonObject[index];
        const sku = object['SKU'];
        const AS400_ID = object['AS400_ID'];
        const name = object['Name'];
        let singleDOM = '<tr id="' + sku + '" class="single-product">';
        singleDOM += '<td><h6>' + name + "</h6></td>";
        singleDOM += '<td><p>' + sku + '</p></td>';
        singleDOM += '<td><p>' + AS400_ID + '</p></td>';
        singleDOM += '<td><button class="btn btn-success btn-cons add-to-list" data-target="' + sku + '" data-index="' + index +'">Aggiungi</button></td></tr>';
        openDOM += singleDOM;
    }
    let closeDOM = '</tbody></table></div>';
    openDOM += closeDOM;
    
    return openDOM;
    
}

function generatePartialProductFragment(jsonObject) {
    for (let index in jsonObject) {
        const object = jsonObject[index];
        let singleDOM = '<tr class="single-product">';
        singleDOM += '<td><p>' + object['SKU'] + '</p></td>'
        singleDOM += '<td><p>' + object['Name'] + '</p></td>';
        singleDOM += '<td><p></p></td>';
        singleDOM += '<td><p></p></td>';
        $('#products').append(singleDOM);
    }
    
}
function generateCustomerFragment() {
    const jsonObject = JSON.parse(sessionStorage.getItem("storedObjects"));
    clearUsers();
    for (let index in jsonObject) {
        const object = jsonObject[index];
        const name = object['Name'];
        const surname = object['Surname'];
        const address = object['Address'];
        const company = object['CompanyName'];
        const CAP = object['CAP'];
        const city = object['City'];
        const province = object['Province'];

        let singleDOM = '<tr class="item">';
        singleDOM += '<td>' + name + '</td>';
        singleDOM += '<td>' + surname + '</td>';
        singleDOM += '<td>' + address + '</td>';
        singleDOM += '<td>' + company + '</td>';
        singleDOM += '<td>' + CAP + '</td>';
        singleDOM += '<td>' + city + '</td>';
        singleDOM += '<td>' + province + '</td>';

        $('tbody').first().append(singleDOM);
    }

    
}
function generateSelectedFragment(index) {
    const jsonObject = JSON.parse(sessionStorage.getItem("storedObjects"));
    const object = jsonObject[index];
    const sku = object['SKU'];
    const name = object['Name'];
    const as400_id = object['AS400_ID'];
    const idcli = getCustomerCode();
   
    $.get("/products/getProductPrice?idart=" + as400_id + "&idcli=" + idcli, function (object) {

        const priceDetail = JSON.parse(object);
        const minimPrice = priceDetail['prz listino 01'];
        let DOM = '<tr class="single-item-selected" id="' + as400_id + '"><td>' + sku + '</td><td>' + name + '</td><td></td><td><input type="text" class="form-control" placeholder="Prezzo" id="prezzo"/></td><td><input type="text" class="form-control" id="qty" name="qty" placeholder="Quantità"/></td></tr>';
        $('#selected-product').append(DOM);
    }).fail(function (error) {
        console.log("Errore: " + error);
    });
    
}
function getProducts() {
    let sku = document.getElementById('sku').value;

    let typeOfSearch = $('input[name=type_of_search]:checked').val();

    let startitem = parseInt($('#page').first().text()) * 20 - 20;

    $.get('/products/getResultAsync?sku=' + sku + '&type=' + typeOfSearch + "&startitem=" + startitem, function (object) {
        $('#search-step').fadeOut(500);
        $('#show-step').fadeIn(500).delay(300);
        $('#products').html(generateProductFragment(JSON.parse(object)));
        sessionStorage.setItem("storedObjects", object);
        setupNavigation();
    }).fail(function (e) {
        console.log("Fallito " + e);
    });
}
function clearUsers() {
    $('.item').each(function (e) {
        $(this).remove();
    });
}

function getCustomers() {
    let city = $('#city').val();
    let province = $('#province').val();
    let url = new URL(window.location.href);

    $.get('/anagrafica/getCustomersAsync?type=0&provincia=' + province + '&startItem=' + parseInt($('#page').first().text() * 15) + '&citta=' + url.searchParams.get('citta'), function (object) {

        sessionStorage.setItem("storedObjects", object);
        generateCustomerFragment();
        setupNavigation();
    }).fail(function (e) {
        console.log("FALLITO " + e.responseText);
        });

}
function clearProducts() {
    $('.single-product').each(function (e) {
        $(this).remove();
    });
}
function sendOrder() {
    let CLI = getCustomerCode();
    let nOrder;
    let orderType = $('#orderType').val();
    
    $.get('/list/getlastorder', function (index) {
        nOrder = index;
        $('.single-item-selected').each(function (index) {
            //JUNK DATA
            let date = new Date();
            let splittedDate = date.toISOString().split('T')[0].split('-');
            let dateString = splittedDate[0] + splittedDate[1] + splittedDate[2];
            console.log("DATA: ", dateString);

            let CIG = $('#CIG').val();
            let CUP = $('#CUP').val();

            let azi = 'I';
            let pre = orderType;
            let ido = nOrder + 1;

            let dto = dateString;
            ///////////////////////////////////////////////////////////////////////////
            //SOLO PER LISTINI E PREVENTIVI! RICORDA DI MODIFICARE ANCHE IL FRONT END// 
            let dti = dateString;
            let newDate = date.setFullYear(date.getFullYear() + 1);
            let splittedDtf = new Date(newDate).toISOString().split('T')[0].split('-');
            let dtf = splittedDtf[0] + splittedDtf[1] + splittedDtf[2];
            //////////////////////////////////////////////////////////////////////////
            let nrc = 0;
            let nrg = index;
            let tpr = 'N';
            let art = $(this).attr('id');

            let qty = $(this).children().find('#qty').first().val();
            let prz = $(this).children().find('#prezzo').first().val();

            let tpm = 'O';
            let queryString = "/list/sendorder?\
            azi = " + azi + "\
            &pre = " + pre + "\
            &ido = " + ido + "\
            &dto = " + dto + "\
            &cli = " + CLI + "\
            &cig = " + CIG + "\
            &cup = " + CUP + "\
            &dti = " + dti + "\
            &dtf = " + dtf + "\
            &nrg = " + (nrg + 1) + "\
            &nrc = " + nrc + "\
            &tpr = " + tpr + "\
            &art = " + art + "\
            &qty = " + qty + "\
            &prz = " + prz + "\
            &tpm = " + tpm;
            $.get(queryString.trim().replace(/ /g, ''), function (result) {
                alert(result);
                
            }).fail(function (err) {
                console.log(err);
            });
        });
    });

}
function getCustomersByAgent() {
    let agentCode = $('#agentcode').val();
    $.get('/list/getcustomersasync?agentid=' + agentCode, function (object) {


        sessionStorage.setItem("customers", object);
        let customers = JSON.parse(object);
        let autoCustomers = [];
        for (let customer in customers) {
            //let autoCustomer = customers[customer]['Name'].replace(/ /g, "") + " " + customers[customer]['Surname'].replace(/ /g, "");
            let autoCustomer = customers[customer]['CompanyName'];
            autoCustomers.push(autoCustomer);
        }
        //sessionStorage.setItem("customersTags", autoCustomers);

        initAutoComplete(autoCustomers);
    });
}
function setupNavigation() {
    var pagination = parseInt($('#page').text());
    console.log(pagination);
    if (pagination === 1) {
        $('#prev').addClass("disabled").removeAttr('href');
    } else if (pagination > 1) {
        $('#prev').removeClass("disabled").attr('href', '#');
    }
    console.log($('.item').length);
    if ($('.single-product').length === 11 || $('.item').length === 15 || $('.single-product').length === 20 || $('.item').length === 20) {
        console.log("ALTRA PAGINA");
        $('#next').removeClass("disabled").attr('href', '#');
    } else {
        $('#next').addClass("disabled").removeAttr('href');
    }
    console.log($('.single-product').length);
}
function getCategories() {
    $.get('/products/getCategories', function (object) {
        for (const i in object) {
            $('#category').append('<option id="' + object[i]['pk']+ '">' + object[i]['Name'] + '</option>');
        }
    });

}
function getProductFullPage() {
    clearProducts();
    let url = new URL(window.location.href);
    let sku = url.searchParams.get("sku");
    let onlyAvailable = url.searchParams.get("onlyavailable");
    let typeOfSearch = url.searchParams.get("type");
 
    let startitem = parseInt($('#page').first().text()) * 20 - 20;

    $.get('/products/getResultAsync?sku=' + sku + '&type=' + typeOfSearch + "&startitem=" + startitem + "&itemPage=" + 25 + "&onlyAvailable=" + onlyAvailable, function (object) {
        generatePartialProductFragment(JSON.parse(object));
        setupNavigation();
    }).fail(function (e) {
        console.log("Fallito " + e);
    });
}

function initAutoComplete(array) {
    let customers = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        // `states` is an array of state names defined in "The Basics"
        local: array
    });
    $('#autoComplete').typeahead(null, {
        name: 'customers',
        source: customers
    });
}
$('#prev').click(function (e) {
    let pagination = parseInt($('#page').text());
    if (!$(this).hasClass("disabled")) {
        $('#page').text(pagination - 1);
        if ($(this).hasClass('product')) {
            clearProducts();
            if ($(this).hasClass('fullPage')) {
                getProductFullPage();
            } else {
                getProducts();
            }
            
        } else {
            clearUsers();
            getCustomers();
        }
        setupNavigation();
    }
});

$('#next').click(function (e) {
    let pagination = parseInt($('#page').text());
    if (!$(this).hasClass("disabled")) {
        $('#page').text(pagination + 1);
        
        if ($(this).hasClass('product')) {
            clearProducts();
            if ($(this).hasClass('fullPage')) {
                getProductFullPage();
            } else {
                getProducts();
            }
        } else {
            clearUsers();
            getCustomers();
        }
        setupNavigation();
    }
});
$('#close_modal').click(function (e) {
    $('#search-step').fadeIn().delay(200);
    $('#show-step').fadeOut().delay(200);
});
$('#more-options').click(function (e) {
    e.preventDefault();
    
    let child = $(this).children('.fa-angle-left').first();
    child.css({ 'transition': 'linear 0.2s'});
    if (!$(this).hasClass('expanded')) {
        $(this).addClass('expanded');
        $('.more-options').first().fadeIn();
        child.css({ 'transform': 'rotate(-90deg)' });
    } else {
        $(this).removeClass('expanded');
        $('.more-options').first().fadeOut();
        child.css({ 'transform': 'rotate(0deg)' });
    }
   
});
$('#delivery-customer-search').click(function (e) {
    e.preventDefault();
    let name = $('#nome').val();
    let surname = $('#cognome').val();
    let provincia = $('#provincia').val();
    let citta = $('#citta').val();
    let cap = $('#cap').val();
    let societa = $('#societa').val();
    window.location.href = "/anagrafica/result?name=" + name + "&surname=" + surname + "&provincia=" + provincia + "&citta=" + citta + "&type=" + 0 + "&cap=" + cap + "&societa=" + societa;
});
$('#invoice-customer-search').click(function (e) {
    e.preventDefault();
    let name = $('#nome').val();
    let surname = $('#cognome').val();
    let provincia = $('#provincia').val();
    let citta = $('#citta').val();
    let piva = $('#piva').val();
    let societa = $('#societa').val();
    window.location.href = "/anagrafica/result?name=" + name + "&surname=" + surname + "&provincia=" + provincia + "&citta=" + citta + "&type=" + 1 + "&piva=" + piva + "&societa=" + societa;

});
$('.submit').click(function (e) {
    e.preventDefault();
    console.log("test");
    if ($(this).data('target') === "lsord") {
        let cli = $('#cli').val();
        let name = $('#autoComplete').val();
        if (cli === "" && name !== "") {
            cli = getCustomerCode();
        }
        let pref = "ALL";

        window.location.href = $(this).attr('href') + "?" + "cli=" + cli + "&" + "pref=" + pref;
    } else if ($(this).data('target') === "srcprod") {
        let sku = $('#sku').val();
        let typeOfSearch = $('input[name=type_of_search]:checked').val();
        let onlyAvailable = $('#onlyAvailable').val();
        //console.log(typeOfSearch);
        window.location.href = $(this).attr('href') + "?" + "sku=" + sku + "&" + "type=" + typeOfSearch + "&startItem=0&onlyavailable=" + onlyAvailable;
    }
});

$('.product-submit').click(function (e) {
    e.preventDefault();
});

$('#modalToggle').click(function (e) {
    if ($('#cliente').val() === "Scegli...") {
        e.stopPropagation();
        alert("SCEGLIERE PRIMA UN CLIENTE");
    }
    
});

$('#search_product_async').click(function (e) {

    getProducts();

});

$('#products').on("click",".add-to-list", function (e) {
    e.preventDefault();
    console.log($(e.target).data('index'));
    generateSelectedFragment($(e.target).data('index'));
});

$('.quick-search').change(function (e) {
    let target = $(e.currentTarget).children("option:selected").attr('id');   
    window.location.href = "/products/result?type=4&sku=" + target + "&startItem=0";
});
$('#addOrder').click(function (e) {
    sendOrder();
});
$(document).ready(function (e) {

    if ($('#category').length) {
        getCategories();
    }
    setupNavigation();
    getCustomersByAgent();
});