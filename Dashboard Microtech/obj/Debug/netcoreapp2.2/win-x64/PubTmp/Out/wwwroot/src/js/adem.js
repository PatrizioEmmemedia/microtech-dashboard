/*
|--------------------------------------------------------------------------
| Basic
|--------------------------------------------------------------------------
*/

function hideScroll() {
	$('html, body').addClass('scroll-hidden');
}

function allowScroll() {
	$('html, body').removeClass('scroll-hidden');
}
function mobileSubMenu($items) {

	//$items.next('ul').hide();

	$items.on('click', function (e) {

		e.preventDefault();

		var $this = $(this);

		$this.next().slideToggle();

		$this.parent().toggleClass('show');
	});
}


/*
|--------------------------------------------------------------------------
| Methods
|--------------------------------------------------------------------------
*/

$(document).ready(function() {

	// Partials.js timeOut. Delete it in production.
	setTimeout(function() {
		$('.nav-btn').click(function () {

			$('body, html').scrollTop(0);

			$('body').toggleClass('nav-side-in');

			$(this).toggleClass('open');

			$('.header-main').toggleClass('in');

			if ($(this).hasClass('open')) {
				hideScroll();
			}
			else if (!$('.cat-main aside').hasClass('in')) {
				allowScroll();
			}
		});
		mobileSubMenu($('.nav-side .menu-item-has-children > a'));
		jcf.setOptions('Select', {
			wrapNative: false,
			wrapNativeOnMobile: true,
			maxVisibleItems:6
		});

		jcf.setOptions('Scrollable');
		jcf.replaceAll();

	}, 500);


});
