#pragma checksum "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Amministrazione\agenti.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "da38bd4acef1301ddd0ac0f6d7eb3e87c92c13c8"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Amministrazione_agenti), @"mvc.1.0.view", @"/Views/Amministrazione/agenti.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Amministrazione/agenti.cshtml", typeof(AspNetCore.Views_Amministrazione_agenti))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\_ViewImports.cshtml"
using Dashboard_Microtech;

#line default
#line hidden
#line 2 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\_ViewImports.cshtml"
using Dashboard_Microtech.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"da38bd4acef1301ddd0ac0f6d7eb3e87c92c13c8", @"/Views/Amministrazione/agenti.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"e502bc48e52d559fac72aa877ef90af185d1681e", @"/Views/_ViewImports.cshtml")]
    public class Views_Amministrazione_agenti : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 2 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Amministrazione\agenti.cshtml"
  
    ViewData["Title"] = "Amministrazione Agenti";
    Layout = "~/Views/Shared/_Layout.cshtml";

#line default
#line hidden
            BeginContext(107, 530, true);
            WriteLiteral(@"<div class=""row mt-60"">
    <div class=""col-12"">
        <div class=""card"">
            <div class=""body-card-w"">
                <table>
                    <thead>
                        <tr>
                            <th>Codice agente</th>
                            <th>Nome agente</th>
                            <th>Email</th>
                            <th>Telefono</th>
                            <th>Azioni</th>
                        </tr>
                    </thead>
                    <tbody>
");
            EndContext();
#line 21 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Amministrazione\agenti.cshtml"
                         foreach (var agent in @Model)
                        {

#line default
#line hidden
            BeginContext(720, 83, true);
            WriteLiteral("                            <tr>\r\n                                <td class=\"code\">");
            EndContext();
            BeginContext(804, 16, false);
#line 24 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Amministrazione\agenti.cshtml"
                                            Write(agent.Codice_ACG);

#line default
#line hidden
            EndContext();
            BeginContext(820, 56, true);
            WriteLiteral("</td>\r\n                                <td class=\"desc\">");
            EndContext();
            BeginContext(877, 17, false);
#line 25 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Amministrazione\agenti.cshtml"
                                            Write(agent.Descrizione);

#line default
#line hidden
            EndContext();
            BeginContext(894, 57, true);
            WriteLiteral("</td>\r\n                                <td class=\"email\">");
            EndContext();
            BeginContext(952, 11, false);
#line 26 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Amministrazione\agenti.cshtml"
                                             Write(agent.Email);

#line default
#line hidden
            EndContext();
            BeginContext(963, 61, true);
            WriteLiteral("</td>\r\n                                <td class=\"cellulare\">");
            EndContext();
            BeginContext(1025, 15, false);
#line 27 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Amministrazione\agenti.cshtml"
                                                 Write(agent.Cellulare);

#line default
#line hidden
            EndContext();
            BeginContext(1040, 590, true);
            WriteLiteral(@"</td>
                                <td>
                                    <div class=""dropdown dropdown-default"">
                                        <button class=""btn btn-sm dropdown-toggle"" type=""button"" data-toggle=""dropdown"" aria-haspopup=""true"" aria-expanded=""false"">Azioni</button>
                                        <div class=""dropdown-menu"">
                                            <a class=""dropdown-item edit-agent"" href=""#"" data-toggle=""modal"" data-target=""#modalSlideUp"">Modifica</a>
                                            <a class=""dropdown-item""");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 1630, "\"", 1694, 2);
            WriteAttributeValue("", 1637, "/amministrazione/statisticaAgenti?agent=", 1637, 40, true);
#line 33 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Amministrazione\agenti.cshtml"
WriteAttributeValue("", 1677, agent.Codice_ACG, 1677, 17, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1695, 289, true);
            WriteLiteral(@">Statistica</a>
                                            <a class=""dropdown-item delete-agent"" href=""#"">Elimina</a>
                                        </div>
                                    </div>

                                </td>
                            </tr>
");
            EndContext();
#line 40 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Amministrazione\agenti.cshtml"
                        }

#line default
#line hidden
            BeginContext(2011, 2013, true);
            WriteLiteral(@"                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class=""modal fade slide-up disable-scroll"" id=""modalSlideUp"" tabindex=""-1"" role=""dialog"" aria-labelledby=""modalSlideUpLabel"" aria-hidden=""false"">
    <div class=""modal-dialog modal-lg"">
        <div class=""modal-content-wrapper"">
            <div class=""modal-content"" style=""padding:25px;"">
                <h6 style=""padding-bottom: 10px;"">Modifica</h6>

                <div class=""form-group form-group-default"">
                    <label for=""code"">Codice Agente</label>
                    <input type=""text"" class=""form-control"" id=""code"" disabled />
                </div>
                <div class=""form-group form-group-default"">
                    <label for=""desc"">Descrizione</label>
                    <input type=""text"" class=""form-control"" id=""desc"" />
                </div>
                <div class=""form-group form-group-default"">
                    <label for=""emai");
            WriteLiteral(@"l"">E-mail</label>
                    <input type=""text"" class=""form-control"" id=""email"" />
                </div>
                <div class=""form-group form-group-default"">
                    <label for=""cellulare"">Cellulare</label>
                    <input type=""text"" class=""form-control"" id=""cellulare"" />
                </div>
                <div class=""form-group form-group-default"">
                    <label for=""budget"">Percentuale budget</label>
                    <input type=""text"" class=""form-control"" id=""budget"" />
                </div>
                <div class=""form-group form-group-default"">
                    <label for=""password"">Password</label>
                    <input type=""text"" class=""form-control"" id=""password"" />
                </div>
                <button type=""button"" class=""btn btn-success"" id=""btn-change-agent"">Aggiorna</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
