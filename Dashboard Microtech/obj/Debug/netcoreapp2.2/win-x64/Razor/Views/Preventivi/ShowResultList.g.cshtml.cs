#pragma checksum "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8a10aa2c03373c070f8b248d1acfb7044d030907"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Preventivi_ShowResultList), @"mvc.1.0.view", @"/Views/Preventivi/ShowResultList.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Preventivi/ShowResultList.cshtml", typeof(AspNetCore.Views_Preventivi_ShowResultList))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\_ViewImports.cshtml"
using Dashboard_Microtech;

#line default
#line hidden
#line 2 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\_ViewImports.cshtml"
using Dashboard_Microtech.Models;

#line default
#line hidden
#line 1 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#line 2 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
using Newtonsoft.Json;

#line default
#line hidden
#line 3 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
using Microtech_Core.Database.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8a10aa2c03373c070f8b248d1acfb7044d030907", @"/Views/Preventivi/ShowResultList.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"e502bc48e52d559fac72aa877ef90af185d1681e", @"/Views/_ViewImports.cshtml")]
    public class Views_Preventivi_ShowResultList : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 5 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
  
    ViewData["Title"] = "Storico preventivi per ";
    ViewData["Customer"] = @Model[0].name;
    Layout = "~/Views/Shared/_Layout.cshtml";

#line default
#line hidden
            BeginContext(299, 4, true);
            WriteLiteral("\r\n\r\n");
            EndContext();
#line 12 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
 if (@Model != null)
{

#line default
#line hidden
            BeginContext(328, 38, true);
            WriteLiteral("    <input type=\"hidden\" id=\"clientId\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 366, "\"", 389, 1);
#line 14 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
WriteAttributeValue("", 374, Model[0].delId, 374, 15, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(390, 252, true);
            WriteLiteral(" />\r\n    <input type=\"hidden\" id=\"agentcode\" value=\'\' />\r\n    <input type=\"hidden\" id=\"agentname\" value=\'\' />\r\n    <input type=\"hidden\" id=\"agentcell\" value=\"\" />\r\n    <input type=\"hidden\" id=\"agentemail\" value=\"\" />\r\n    <script>\r\n        var Model = ");
            EndContext();
            BeginContext(644, 69, false);
#line 20 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
                Write(Html.Raw(HttpContextAccessor.HttpContext.Session.GetString("Logged")));

#line default
#line hidden
            EndContext();
            BeginContext(714, 379, true);
            WriteLiteral(@";
        console.log(Model);
        document.getElementById(""agentcode"").value = Model[""Codice_ACG""];
        document.getElementById(""agentname"").value = Model[""Descrizione""].trimRight();
        document.getElementById(""agentcell"").value = Model[""Cellulare""].trimRight();
        document.getElementById(""agentemail"").value = Model[""Email""].trimRight();
    </script>
");
            EndContext();
            BeginContext(1095, 693, true);
            WriteLiteral(@"    <div class=""row mt-60"">
        <div class=""col-12"">
            <div class=""card"">
                <div class=""body-card-w"">
                    <h6 class=""mb-30"">Lista preventivi</h6>
                    <div class=""table-responsive"">
                        <table>
                            <tr>
                                <th>Stato</th>
                                <th>Numero</th>
                                <th>Data</th>
                                <th>Denominazione cliente</th>
                                <th>Importo</th>
                                <th>Azioni</th>
                            </tr>
                            <tbody>
");
            EndContext();
#line 44 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
                                 foreach (var i in @Model)
                                {

#line default
#line hidden
            BeginContext(1883, 86, true);
            WriteLiteral("                                    <tr>\r\n                                        <td>");
            EndContext();
            BeginContext(1970, 8, false);
#line 47 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
                                       Write(i.status);

#line default
#line hidden
            EndContext();
            BeginContext(1978, 51, true);
            WriteLiteral("</td>\r\n                                        <td>");
            EndContext();
            BeginContext(2030, 4, false);
#line 48 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
                                       Write(i.id);

#line default
#line hidden
            EndContext();
            BeginContext(2034, 51, true);
            WriteLiteral("</td>\r\n                                        <td>");
            EndContext();
            BeginContext(2086, 11, false);
#line 49 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
                                       Write(i.getDate());

#line default
#line hidden
            EndContext();
            BeginContext(2097, 73, true);
            WriteLiteral("</td>\r\n                                        <td class=\"deliverNumber\">");
            EndContext();
            BeginContext(2171, 6, false);
#line 50 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
                                                             Write(i.name);

#line default
#line hidden
            EndContext();
            BeginContext(2177, 51, true);
            WriteLiteral("</td>\r\n                                        <td>");
            EndContext();
            BeginContext(2229, 12, false);
#line 51 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
                                       Write(i.getValue());

#line default
#line hidden
            EndContext();
            BeginContext(2241, 742, true);
            WriteLiteral(@" €</td>
                                        <td>
                                            <div class=""dropdown dropdown-default"">
                                                <button class=""btn btn-sm dropdown-toggle"" type=""button"" data-toggle=""dropdown"" aria-haspopup=""true"" aria-expanded=""false"">
                                                    Azioni
                                                </button>
                                                <div class=""dropdown-menu"" style=""width: 91.3438px; position: absolute; transform: translate3d(0px, -169px, 0px); top: 0px; left: 0px; will-change: transform;"" x-placement=""top-start"">
                                                    <a class=""dropdown-item""");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 2983, "\"", 3025, 2);
            WriteAttributeValue("", 2990, "/preventivi/ShowResultPre?pre=", 2990, 30, true);
#line 58 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
WriteAttributeValue("", 3020, i.id, 3020, 5, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(3026, 15, true);
            WriteLiteral(">Dettagli</a>\r\n");
            EndContext();
#line 59 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
                                                     if ((string)Context.Request.Query["cli"] != "ALL")
                                                    {

#line default
#line hidden
            BeginContext(3201, 58, true);
            WriteLiteral("                                                        <a");
            EndContext();
            BeginWriteAttribute("href", " href=\'", 3259, "\'", 3348, 4);
            WriteAttributeValue("", 3266, "/List/Crea?type=WPR&ordId=", 3266, 26, true);
#line 61 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
WriteAttributeValue("", 3292, i.id, 3292, 5, false);

#line default
#line hidden
            WriteAttributeValue("", 3297, "&editMode=true&ordCli=", 3297, 22, true);
#line 61 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
WriteAttributeValue("", 3319, Context.Request.Query["cli"], 3319, 29, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(3349, 37, true);
            WriteLiteral(" class=\"dropdown-item\">Modifica</a>\r\n");
            EndContext();
#line 62 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
                                                    }

#line default
#line hidden
            BeginContext(3441, 137, true);
            WriteLiteral("                                                    <!--<a href=\"https://desk.microtech.eu/template.html?\" data-target=\"pre\" data-ordId=\"");
            EndContext();
            BeginContext(3579, 4, false);
#line 63 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
                                                                                                                                    Write(i.id);

#line default
#line hidden
            EndContext();
            BeginContext(3583, 15, true);
            WriteLiteral("\" data-ordCli=\'");
            EndContext();
            BeginContext(3599, 28, false);
#line 63 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
                                                                                                                                                        Write(Context.Request.Query["cli"]);

#line default
#line hidden
            EndContext();
            BeginContext(3627, 45, true);
            WriteLiteral("\' class=\"dropdown-item print\">Stampa</a>-->\r\n");
            EndContext();
#line 64 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
                                                     if ((string)Context.Request.Query["cli"] != "ALL")
                                                    {

#line default
#line hidden
            BeginContext(3832, 58, true);
            WriteLiteral("                                                        <a");
            EndContext();
            BeginWriteAttribute("href", " href=\'", 3890, "\'", 3980, 4);
            WriteAttributeValue("", 3897, "/List/Crea?type=WPR&ordId=", 3897, 26, true);
#line 66 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
WriteAttributeValue("", 3923, i.id, 3923, 5, false);

#line default
#line hidden
            WriteAttributeValue("", 3928, "&editMode=false&ordCli=", 3928, 23, true);
#line 66 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
WriteAttributeValue("", 3951, Context.Request.Query["cli"], 3951, 29, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(3981, 34, true);
            WriteLiteral(" class=\"dropdown-item\">Copia</a>\r\n");
            EndContext();
#line 67 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
                                                    }

#line default
#line hidden
            BeginContext(4070, 123, true);
            WriteLiteral("                                                    <a href=\"http://138.68.100.112/fatture/\" data-target=\"pre\" data-ordId=\"");
            EndContext();
            BeginContext(4194, 4, false);
#line 68 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
                                                                                                                      Write(i.id);

#line default
#line hidden
            EndContext();
            BeginContext(4198, 15, true);
            WriteLiteral("\" data-ordCli=\'");
            EndContext();
            BeginContext(4214, 28, false);
#line 68 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
                                                                                                                                          Write(Context.Request.Query["cli"]);

#line default
#line hidden
            EndContext();
            BeginContext(4242, 347, true);
            WriteLiteral(@"' class=""dropdown-item pdf"">
                                                        PDF
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
");
            EndContext();
#line 75 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
                                }

#line default
#line hidden
            BeginContext(4624, 172, true);
            WriteLiteral("                            </tbody>\r\n                        </table>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n");
            EndContext();
#line 83 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
    
    }
    else
    {

#line default
#line hidden
            BeginContext(4826, 31, true);
            WriteLiteral("    <p>Ordine non trovato</p>\r\n");
            EndContext();
#line 88 "C:\Users\patrizio.esposito\source\repos\Dashboard Microtech\Dashboard Microtech\Views\Preventivi\ShowResultList.cshtml"
    }

#line default
#line hidden
            BeginContext(4864, 4, true);
            WriteLiteral("\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public IHttpContextAccessor HttpContextAccessor { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
