﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

let canBeSent = true;


function getCustomerCode() {
    let CLI;
    let customers;
    if (sessionStorage.getItem('customers') != null && sessionStorage.getItem('customers') != "[]") {
        customers = JSON.parse(sessionStorage.getItem("customers"));
    } else {
        customers = JSON.parse(localStorage.getItem('customers'));
    }
    for (let i in customers) {
        let object = customers[i];
        if (object['CompanyName'] + " ( " + object['AS400_CODE'] + " )" === $("#autoComplete").val()) {
            
            CLI = object["AS400_ID"];

        }
    }
    return CLI;
}
function getCustomerNameById(id) {

    let name;
    let customers = JSON.parse(sessionStorage.getItem("customers"));
    if (customers == null || customers.length == 0) {
        customers = JSON.parse(localStorage.getItem('customers'));
    }
    for (let i in customers) {
        let object = customers[i];
        if (object['AS400_ID'] == id) {
            name = object['CompanyName'] + " ( " + object['AS400_CODE'] + " )"; 
        }
    }
    $('#autoComplete').val(name);
    if ($('#selected-product>tr').length == 0) {
        const products = JSON.parse($('#productsJson').val());


        if ($('#orderType').val() != "WPR" && $('#orderType').val() != "WLS") {
            if (!sessionStorage.getItem('generated')) {
                generateOrderCached(products);
                sessionStorage.setItem('generated', true);
            }
            
        } else {
            if(!sessionStorage.getItem('generated')){
                generateProductsCached(products);
                sessionStorage.setItem('generated', true);
            }
            
        }
    }

    return name;
}
function getCustomerName() {
    let CLI;
    let customers = JSON.parse(sessionStorage.getItem("customers"));
    for (let i in customers) {
        let object = customers[i];
        if (object['AS400_ID'] == $("#clientId").val()) {

            CLI = object["CompanyName"];
        }
    }
    return CLI;
}

function getCustomer() {
    let CLI;
    let customers = JSON.parse(sessionStorage.getItem("customers"));
    if (customers == null || customers.length == 0) {
        customers = JSON.parse(localStorage.getItem("customers"));
        
    }

    for (let i in customers) {
        let object = customers[i];
        if (object['CompanyName'] + " ( " + object['AS400_CODE'] + " )" === $("#autoComplete").val()) {
            CLI = object
        }
    }
    return CLI;
}

function getCustomerWithParams(as400_id) {
    let CLI;
    let customers = JSON.parse(sessionStorage.getItem("customers"));
    
    for (let i in customers) {
        let object = customers[i];
        if (object['AS400_ID'] == as400_id) {
            CLI = object
        }
    }
    return CLI;
}
function getInvoiceCode() {
    let CLI;
    let customers = JSON.parse(sessionStorage.getItem("customers"));
    let autocustomer = false;

    if (customers.length == 0) {
        customers = JSON.parse(localStorage.getItem("autoCustomers"));
        autocustomer = true;
    }

    console.log(customers.length);

    for (let i in customers) {
        let object = customers[i];
        if (autocustomer) {
            if (object === $('#autoComplete').val()) {
                $.get('/anagrafica/GetAS400Code?as400=' + object.split('(')[1].split(')')[0].trimLeft(), function (obj){
                    let CLI = JSON.parse(obj)[0]['InvoiceCustomer'];
                    queryString = "/anagrafica/result?name=" + CLI + "&type=1";
                    window.location.href = queryString;
                });
                return false;
            }
        }
        if (object['CompanyName'] + " ( " + object['AS400_CODE'] + " )" === $("#autoComplete").val()) {
            CLI = object["InvoiceCustomer"];
        }
    }
    return CLI;
}
function convertDate(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    var d = new Date(inputFormat);
    return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('/');
}
function generateProductFragment(jsonObject, startItem, endItem, length) {
   
    let openDOM = '<div class="products">\
    <table>\
    <thead>\
    <tr> <th>SKU</th> <th>Nome</th> <th>Disponibilità</th> <th>Confezionamento</th><th>Prezzo</th><th>Azioni</th><tr>\
    </thead>\
    <tbody id="products">\
    ';
    const products = [];

    for (let i = startItem; i < endItem; i++) {

        
        const object = jsonObject[i];

        if (object == null) {
            continue;
        }

        const sku = object['SKU'];
        const AS400_ID = object['AS400_ID'];
        

        $.get('/products/checksample?sku=' + sku, function (isSample) {

            if (!isSample && AS400_ID != null) {

                $.get("/products/getproductprice?idcli=3610&idart=" + AS400_ID, function (price) {



                    const priceObject = JSON.parse(price);


                    let product = {};

                    product['SKU'] = object['SKU'];
                    product['Name'] = object['Name'];
                    product['giacenza'] = priceObject['giacenza'];
                    product['lead time'] = priceObject['lead time'];
                    product['Confezionamento'] = object['Confezionamento'];
                    product['prz listino cliente'] = priceObject['prz listino cliente'];
                    product['index'] = i;
                    products.push(product);

                    console.log(products.length + " " + endItem + " " + startItem); 
                    if (products.length == (endItem - startItem) || products.length == length) {

                        generateFragment(products);
                    }

                });
            } else {
                length -= 1;
                startItem += 1;
                
            }
          
        });

    }
    let closeDOM = '</tbody></table></div>';
    openDOM += closeDOM;
    
    return openDOM;
    
}

function generateFragment(products) {
    products.sort(compareAvailability).reverse();
    for (let index in products) {
        let product = products[index];

        let singleDOM = '<tr class="single-product">';

        singleDOM += '<td><p>' + product['SKU'] + '</p></td>';
        singleDOM += '<td><p>' + product['Name'] + '</p></td>';

        if (parseFloat(product['giacenza']) > 0.1) {
            singleDOM += '<td><p style="color:#52d838"> ' + product['giacenza'].split('.')[0] + ' </p></td>';
        } else if (parseInt(product['lead time']) < 14 && parseInt(product['lead time']) > 0) {
            let date = new Date();
            date.setDate(date.getDate() + parseInt(product['lead time']));
            singleDOM += '<td><p style="color:#d81e1e"> ' + convertDate(date) + '</p></td>';
        } else {
            singleDOM += '<td><p style="color:#d81e1e">Per info chiamare 0816107435</p></td>';
        }
        if (product['Confezionamento'] === "") {
            product['Confezionamento'] = "1";
        }
        singleDOM += '<td>' + product['Confezionamento'] + '</td>';
        singleDOM += '<td class="price-customer">' + product['prz listino cliente'].toLocaleString('it-IT', { style: 'currency', currency: 'EUR' }) + '</td>';
        singleDOM += '<td><button class="btn btn-sm add-to-list" data-target="' + product['SKU'] + '" data-index="' + product['index'] + '">Aggiungi</button></td></tr>';
        $('tbody#products').append(singleDOM);
        setupNavigation();
    }
   
}

function generatePartialProductFragment(jsonObject) {
    for (let index in jsonObject) {
        const object = jsonObject[index];
        $.get("/products/getproductprice?idcli=3610&idart=" + object['AS400_ID'], function (price) {
            let singleDOM = '<tr class="single-product">';
            const priceObject = JSON.parse(price);
            singleDOM += '<td><p>' + object['SKU'] + '</p></td>';
            singleDOM += '<td><p>' + object['Name'] + '</p></td>';

            singleDOM += '<td><p>' + priceObject['prz listino C1'] + ' €</p></td>';
            if (object['Confezionamento'] === "") {
                object['Confezionamento'] = "1";
            }
            singleDOM += '<td><p> ' + object['Confezionamento'] + '</p></td>';
            if (parseFloat(priceObject['giacenza']) > 0.1) {
                singleDOM += '<td><p style="color:#52d838"> ' + priceObject['giacenza'] + ' </p></td>';
            } else if (parseInt(priceObject['giacenza']) != 00000000) {
                singleDOM += '<td><p style="color:#d81e1e"> ' + priceObject['data next arrivo'] + '</p></td>';
            } else {
                singleDOM += '<td><p style="color:#d81e1e">Per info chiamare 0816107435</p></td>';
            }
            $('#products').append(singleDOM);
            setupNavigation();
        });
    }
    
}
function generateCustomerFragment() {
    const jsonObject = JSON.parse(sessionStorage.getItem("storedObjects"));
    clearUsers();
    for (let index in jsonObject) {
        const object = jsonObject[index];
        const name = object['Name'];
        const surname = object['Surname'];
        const address = object['Address'];
        const company = object['CompanyName'];
        const CAP = object['CAP'];
        const city = object['City'];
        const province = object['Province'];

        let singleDOM = '<tr class="item">';
        singleDOM += '<td>' + name + '</td>';
        singleDOM += '<td>' + surname + '</td>';
        singleDOM += '<td>' + address + '</td>';
        singleDOM += '<td>' + company + '</td>';
        singleDOM += '<td>' + CAP + '</td>';
        singleDOM += '<td>' + city + '</td>';
        singleDOM += '<td>' + province + '</td>';

        $('tbody').first().append(singleDOM);
    }

    
}
function generateSelectedFragment(index) {
    const jsonObject = JSON.parse(sessionStorage.getItem("storedObjects"));
    const object = jsonObject[index];
    const sku = object['SKU'];
    const name = object['Name'];
    const as400_id = object['AS400_ID'];
     
    let idcli; 
    if ($('#orderType').val() == "WLS") {
        idcli = getCustomerByFattCode()['AS400_ID'];
    } else {
        idcli = getCustomerCode();
    }
    const supplier = object['Fornitori'];
    $.get("/products/getProductPrice?idart=" + as400_id + "&idcli="+idcli, function (object) {

        const priceDetail = JSON.parse(object);
        const minimPrice = priceDetail['prz listino C1'];
        $.getJSON('/sconti.json', function (obj) {
            let fornitore = supplier;


            if (fornitore != null) {
                fornitore = fornitore.split(' ')[0];
                let data = obj[fornitore];

                if (data == undefined) {
                    data = {};
                    data['Autorizzato'] = '0.4';
                }
                let DOM = '<tr class="single-item-selected" id="' + as400_id + '" data-forn="' + supplier + '" data-autorizzato="' + data['Autorizzato'] +'"><td id="sku">' + sku + '</td><td id="name">' + name + '</td><td><input type="text" class="form-control text-dark" value="' + minimPrice.toLocaleString('it-IT', { style: 'currency', currency: 'EUR' }) + '" id="prezzo" disabled/></td><td><input type="text" class="discount form-control" value="0" /></td><td><input type="text" class="form-control discounted" value="' + minimPrice.toLocaleString('it-IT', { style: 'currency', currency: 'EUR' }) + '"/></td><td><input type="text" class="form-control" id="qty" name="qty" placeholder="Quantità" value="1" /></td><td><input type="text" class="form-control total text-dark total" id="totalPrice" value="' + minimPrice.toLocaleString('it-IT', { style: 'currency', currency: 'EUR' }) + '" disabled/></td><td><a class="removeProduct" href="#"><i class="fas fa-times" style="color: #9F0000;"></i></a></td></tr>';
                $('#selected-product').append(DOM);


                let selected = [];
                $('#selected-product').children().each(function (obj) {
                    let product = {};

                    product['SKU'] = $(this).find('#sku').first().text().trimRight();
                    product['Name'] = $(this).find('#name').first().text();
                    product['unitPrice'] = $(this).children().find('input#prezzo').first().val();
                    product['discounted'] = $(this).children().find('input.discounted').first().val();
                    product['discount'] = $(this).children().find('input.discount').first().val();
                    product['qty'] = $(this).children().find('input#qty').first().val();
                    product['AS400_ID'] = $(this).attr('id');
                    product['Fornitori'] = $(this).data('forn');
                    selected.push(product);
                });

                localStorage.setItem('persistence', JSON.stringify(selected));
                localStorage.setItem('orderType', $('#orderType').val());
                localStorage.setItem('userEstim', $('#autoComplete').val());

                updateTotal();

            }
        });
        
   

    }).fail(function (error) {
        console.log("Errore: " + error);
    });
    
}
function CachedFragment(object) {
    
    
    const sku = object['SKU'];
    const name = object['Name'];
    const as400_id = object['AS400_ID'];
    let idcli;
    let orderType = $('#orderType').val();
    if (orderType == "WLS") {
        getCustomersByAgentFatt();

        let fattCode = getCustomerByFattCode();
        if (fattCode != null) {
            idcli = fattCode['ID'];
        } else {
            window.location.reload();
        }
        
    } else {
        idcli = getCustomerCode();
    }
    const supplier = object['Fornitori'];
    const discount = object['discount']; 
    const discounted = object['discounted'];
    const qty = object['qty'];
    $.get("/products/getProductPrice?idart=" + as400_id + "&idcli=" + idcli, function (object) {

        const priceDetail = JSON.parse(object);
        const minimPrice = priceDetail['prz listino C1'];
        let DOM = '<tr class="single-item-selected" id="' + as400_id + '" data-forn="' + supplier + '" ><td id="sku">' + sku + '</td><td id="name">' + name + '</td><td><input type="text" class="form-control text-dark" value="' + minimPrice.toLocaleString('it-IT', { style: 'currency', currency: 'EUR' }) + '" id="prezzo" disabled/></td><td><input type="text" class="discount form-control" value="' + discount + '" /></td><td><input type="text" class="form-control discounted" value="' + discounted.toLocaleString('it-IT', { style: 'currency', currency: 'EUR' }) + '" /></td><td><input type="text" class="form-control" id="qty" name="qty" placeholder="Quantità" value="' + qty + '" /></td><td><input type="text" class="form-control total text-dark total" id="totalPrice" value="' + minimPrice.toLocaleString('it-IT', { style: 'currency', currency: 'EUR' }) + '" disabled/></td><td><a class="removeProduct" href="#"><i class="fas fa-times" style="color: #9F0000;"></i></a></td></tr>';
        $('#selected-product').append(DOM);

        updateTotal();

    }).fail(function (error) {
        console.log("Errore: " + error);
    });

}

function generateOrderCached(products) {
    let rows = products["Righe Ordini"];
    let remaining = products["Valore Ordinato"];
    console.log(remaining);
    $('#saldo_rimanente').text(parseFloat(remaining.replace('-', '')).toFixed(2).replace('.', ',') + "€");
    $('#saldo_rimanente').attr('data-remaining', parseFloat(remaining.replace('-', '')));
    for (let index in rows) {
        let product = rows[index];
        
        const as400_id = product['ID Articolo'];
        const idcli = products['ID Cliente'];
        
        let minimPrice = product['Prezzo'];
        const qty = parseInt(product['Q.tà Ordinata']);
        

        $.get("/products/getProductPrice?idart=" + as400_id + "&idcli=" + idcli, function (object) {
            
            let priceDetail = JSON.parse(object);

            let przListino = priceDetail['prz listino C1'];
            const name = priceDetail['descrizione articolo'];
            const sku = priceDetail["codice articolo"];
            
            let realPrice = minimPrice / qty;
            let discount = parseFloat(100 - (100 * realPrice) / przListino).toFixed(2); // BIG MATH
            let DOM;
            if (sku == "CNTCRD") {
                
                minimPrice = product['Valore Ordinato'];
                DOM = '<tr class="single-item-selected cached" id="' + as400_id + '" data-row="1"><td id="sku">' + sku + '</td><td id="name">' + name + '</td><td><input type="text" class="form-control" value="' + minimPrice + '€" id="prezzo" disabled/></td><td><input type="text" class="discount form-control" value="" disabled /></td><td><input type="text" class="form-control discounted" value="'+minimPrice+'" disabled /></td><td><input type="text" class="form-control" id="qty" name="qty" placeholder="Quantità" value="0" disabled /></td><td><input type="text" class="form-control total" id="totalPrice" value="' + minimPrice + '€" disabled/></td><td></td></tr>';
            } else {
                console.log(product);
                DOM = '<tr class="single-item-selected cached" id="' + as400_id + '" data-row="' + product['Numero Riga'] + '"><td id="sku">' + sku + '</td><td id="name">' + name + '</td><td><input type="text" class="form-control" value="' + parseFloat(minimPrice).toFixed(2).replace('.',',') + '€" id="prezzo" disabled/></td><td><input type="text" class="discount form-control" value="' + discount + '" disabled /></td><td><input type="text" class="form-control discounted" value="' + parseFloat(minimPrice).toFixed(2).replace('.',',') + '€" disabled /></td><td><input type="text" class="form-control" id="qty" name="qty" placeholder="Quantità" value="' + qty + '" disabled /></td><td><input type="text" class="form-control total" id="totalPrice" value="' + (parseFloat(minimPrice) * parseInt(qty)) + '€" disabled/></td><td></td></tr>';
            }
            $('#selected-product').append(DOM);
            updateTotal();
            
        });

        $(document).ajaxComplete(function (e) {
            //updateBalance();
        });
    }
}
function generateProductsCached(products) {
    let arrayAdded = [];
    for (let index in products) {
        let product = products[index];
        
        const as400_id = product['Id Articolo'];
        const idcli = getCustomerCode();
        const name = product['Descr'];
        let minimPrice = product['Valore riga'];
        let qty = parseInt(product['Quantita']);

        const sku = product["Codice"];

        const urlParams = new URLSearchParams(window.location.search);


        let param = urlParams.get('editMode');

        param == "false" ? param = "" : param = "true"; 

        const editMode = Boolean(param);

        

        let DOM;
        $.get("/products/getProductPrice?idart=" + as400_id + "&idcli=" + 3610, function (object) {
            let priceDetail = JSON.parse(object);

            let przListino = priceDetail['prz listino C1'];

            let realPrice = minimPrice / qty;
            let discount = parseFloat(100 - (100 * realPrice) / przListino).toFixed(2);

            if (!editMode) {

                minimPrice = przListino.toFixed(2);
                discount = 0;
                discounted = przListino;
                realPrice = przListino;
                qty = 1;

            }

            DOM = '<tr class="single-item-selected cached" id="' + as400_id + '" data-row="' + product['Riga'] + '"><td id="sku">' + sku + '</td><td id="name">' + name + '</td><td><input type="text" class="form-control" value="' + przListino.toFixed(2).replace('.',',') + '€" id="prezzo" disabled/></td><td><input type="text" class="discount form-control" value="' + discount + '" /></td><td><input type="text" class="form-control discounted" value="' + realPrice.toFixed(2).replace('.', ',') + '" /></td><td><input type="text" class="form-control" id="qty" name="qty" placeholder="Quantità" value="' + qty + '" /></td><td><input type="text" class="form-control total" id="totalPrice" value="' + minimPrice.replace('.', ',') + '€" disabled/></td><td><a class="removeProduct" href="#"><i class="fas fa-times" style="color: #9F0000;"></i></a></td></tr>';
        }).done(function (e) {

            $('#selected-product').append(DOM);
            //$.unique($('.cached'));
            updateTotal();
            
        });
        
    }
}

function updateBalance() {
    let total = parseFloat($('#subtotale').text().replace(',', '.').replace('€', ''));
    let remaining = parseFloat($('#saldo_rimanente').attr('data-remaining'));

    remaining = remaining - total;

    $('#saldo_rimanente').text(remaining.toFixed(2).replace('.', ',') + " €");
}
function getProducts() {
    let sku = document.getElementById('sku').value;

    let typeOfSearch = $('input[name=type_of_search]:checked').val();

    let startitem = parseInt($('#page').first().text()) * 20 - 20;

    if (typeOfSearch == null) {
        typeOfSearch = 3;
    }

    let search = $('#search-filters').val();

    $.get('/products/getResultAsync?sku=' + sku + '&type=' + typeOfSearch + "&startitem=" + startitem + "&search=" + search, function (object) {
        $('#search-step').fadeOut(500);
        $('#show-step').fadeIn(500).delay(300);
        
        sessionStorage.setItem("storedObjects", object);
        let json = JSON.parse(object);

        $('#products').html(generateProductFragment(json, startitem, startitem + 20, json.length));
        
        
        setupNavigation();
    }).fail(function (e) {
        console.log("Fallito " + e);
    });
}
function getNextProducts() {
    let startitem = parseInt($('#page').first().text()) * 20 - 20;
    let json = JSON.parse(sessionStorage.getItem('storedObjects'));


    $('#products').html(generateProductFragment(json, startitem, startitem + 20));
}

function getPreviousProducts() {
    let startitem = parseInt($('#page').first().text()) * 20 - 20;
    let json = JSON.parse(sessionStorage.getItem('storedObjects'));


    $('#products').html(generateProductFragment(json, startitem, startitem + 20));
}
function clearUsers() {
    $('.item').each(function (e) {
        $(this).remove();
    });
}
function compareAvailability(a, b) {
    if (a['giacenza'] < b['giacenza'])
        return -1;
    if (a['giacenza'] > b['giacenza'])
        return 1;
    return 0;
}
function getCustomers() {
    let city = $('#city').val();
    let province = $('#province').val();
    let url = new URL(window.location.href);

    $.get('/anagrafica/getCustomersAsync?type=0&provincia=' + province + '&startItem=' + parseInt($('#page').first().text() * 15) + '&citta=' + url.searchParams.get('citta'), function (object) {

        sessionStorage.setItem("storedObjects", object);
        generateCustomerFragment();
        setupNavigation();
    }).fail(function (e) {
        console.log("FALLITO " + e.responseText);
        });

}
function clearProducts() {
    $('.single-product').each(function (e) {
        $(this).remove();
    });
}

function deleteRow(row, art, qty, prz) {
    let orderType = $('#orderType').val();
    let user = getCustomer();
    let CLI = user['AS400_ID'];
    let ido = $('#ordId').val();
    let date = new Date();
    let splittedDate = date.toISOString().split('T')[0].split('-');
    let dateString = splittedDate[0] + splittedDate[1] + splittedDate[2];
    let dto = dateString;
    let CIG = $('#CIG').val();
    let CUP = $('#CUP').val();
    let tpm = $('#tpm').val();

    let dti = dateString;
    let newDate = date.setFullYear(date.getFullYear() + 1);
    let splittedDtf = new Date(newDate).toISOString().split('T')[0].split('-');
    let dtf = splittedDtf[0] + splittedDtf[1] + splittedDtf[2];

    let queryString = "/list/sendorder?\
            azi = C \
            &pre = WPR\
            &ido = " + ido + "\
            &dto = " + dto + "\
            &cli = " + CLI + "\
            &cig = " + CIG + "\
            &cup = " + CUP + "\
            &dti = " + dti +"\
            &dtf = "+ dtf +"\
            &nrg = " + row + "\
            &nrc = 0\
            &tpr = N\
            &art = " + art + "\
            &qty = "+ qty +"\
            &prz = " + prz + "\
            &tpm = " + tpm;
    
    $.get(queryString.trim().replace(/ /g, '').replace('%C2%A0', ''), function (result) {

        alert("RIGA ELIMINATA CON SUCCESSO");

    });
}
function sendOrder(highest) {
    
    let user;
    let CLI;
    const urlParams = new URLSearchParams(window.location.search);

    let nOrder;
    let orderType = $('#orderType').val();
    if (orderType == "WLS") {
        user = getCustomerByFattCode();
        CLI = user['AS400_ID'];
    } else {
        user = getCustomer();
        CLI = user['AS400_ID'];
    }
    let pdfElements = {}
    let products = [];
    
    if (user['Name'] != null) {
        pdfElements['customer'] = user['Name'].trimRight() + " " + user['Surname'].trimRight();
        pdfElements['company'] = user['Descrizione'];
    } else {
        pdfElements['customer'] = user['Dipartimento'];
        pdfElements['company'] = "";
    }
    
    pdfElements['address'] = user['Address'] + " " + user['CAP'] + " " + user['City'] + " " + user['Province'];
    
   
    let count = 1;

    sessionStorage.setItem('list', '[]');

    $.get('/list/getlastorder?type=' + orderType, function (index) {
        nOrder = index + 1;
        $('.single-item-selected').each(function (i) {
            //JUNK DATA

            let item = {};
            

            

            let date = new Date();
            let splittedDate = date.toISOString().split('T')[0].split('-');
            let dateString = splittedDate[0] + splittedDate[1] + splittedDate[2];

            let CIG = $('#CIG').val();
            let CUP = $('#CUP').val();
            pdfElements['CIG'] = CIG;
            pdfElements['CUP'] = CUP;
            let azi = $('#azi').val();
            console.log(urlParams.get('editMode'));
            if ($('#azi').val() == null || !$(this).hasClass('cached')) {
                azi = "I";
            }

            let pre = orderType;
            
            let agent = $('#agentname').val() + " - " + $('#agentcell').val() + " " + $('#agentemail').val();
            pdfElements['agent'] = agent;
            let dto = dateString;
            ///////////////////////////////////////////////////////////////////////////
            //SOLO PER LISTINI E PREVENTIVI! RICORDA DI MODIFICARE ANCHE IL FRONT END// 
            let dti = dateString;
            let newDate = date.setFullYear(date.getFullYear() + 1);
            let splittedDtf = new Date(newDate).toISOString().split('T')[0].split('-');
            let dtf = splittedDtf[0] + splittedDtf[1] + splittedDtf[2];
            //////////////////////////////////////////////////////////////////////////
            let nrc = 0;
            
            let nrg = i;
            
            let tpr = 'N';
            let art = $(this).attr('id');
            let ido;
            let qty = $(this).children().find('#qty').first().val();
            item['qty'] = qty;
            let prz = $(this).find('#totalPrice').first().val();
            item['prz'] = prz.replace('.', '');
            item['name'] = $(this).find("#name").text();
            
            item['sku'] = $(this).find("#sku").text().trimRight();

            products.push(item);

            if ($('#ordId').val() == "" || urlParams.get('editMode') == 'false') {
                ido = nOrder + 1;

            } else {
                ido = $('#ordId').val();
            }

            
            if ((nrg == highest || nrg < highest) && azi == "I") {
                nrg = highest + 1;
                highest = nrg;
            }

            if (azi == "S") {
                nrg = $(this).data('row');
            }
            

            pdfElements['idOrd'] = ido; 
            let tpm = $('#tpm').val();
            let queryString = "/list/sendorder?\
            azi = " + azi + "\
            &pre = " + pre + "\
            &ido = " + ido + "\
            &dto = " + dto + "\
            &cli = " + CLI + "\
            &cig = " + CIG + "\
            &cup = " + CUP + "\
            &dti = " + dti + "\
            &dtf = " + dtf + "\
            &nrg = " + (nrg) + "\
            &nrc = " + nrc + "\
            &tpr = " + tpr + "\
            &art = " + art + "\
            &qty = " + qty + "\
            &prz = " + parseFloat(prz.replace('€', '').replace('.','').replace(',', '.')) / qty + "\
            &tpm = " + tpm;

            if (pre == "WLS") {

                let arrayProducts = [];

                let list = JSON.parse(localStorage.getItem('persistence'));

                let listino = {};


                listino['CustomerKey'] = getCustomerByFattCode()['PK'];
                listino['AS400_ID'] = getCustomerByFattCode()['AS400_ID'];
                listino['Agent'] = $('#agentcode').val();

                listino['ProductKey'] = art;
                listino['Price'] = prz.replace('€', '');

                arrayProducts.push(listino);


                $.ajax({
                    method: 'POST',
                    url: '/archive/insertlist',
                    data: JSON.stringify(arrayProducts),
                    contentType: "application/json",
                    success: function (e) {
                        localStorage.setItem('clientId', CLI);
                        localStorage.setItem('orderId', ido);
                        localStorage.removeItem('persistence');
                        localStorage.removeItem('userEstim');
                        localStorage.removeItem('orderType');
                        window.location.href = "/";

                    }
                });


            } 
            if (pre != "000") {
                $.get(queryString.trim().replace(/ /g, '').replace('%C2%A0', ''), function (result) {
                    //alert(result);
                    let res = JSON.parse(result);



                    if (res["code"] == "00") {

                        if (pre !== "WLS") {


                            if (count == $('.single-item-selected').length) {
                                pdfElements['products'] = products;
                                let openUrl = 'https://desk.microtech.eu/template.html?order='.concat(encodeURIComponent(JSON.stringify(pdfElements)));

                                let json = {};
                                json['printUrl'] = openUrl;
                                let message;
                                if ($('#orderType').val() != "WPR") {
                                    message = "Ordine aggiornato con successo.";
                                } else {
                                    message = "Preventivo inserito con successo.";
                                }

                                localStorage.removeItem('persistence');
                                localStorage.removeItem('userEstim');
                                localStorage.removeItem('orderType');
                                localStorage.setItem('clientId', CLI);
                                localStorage.setItem('orderId', ido);
                                window.location.href = "/";

                            }
                            count += 1;
                        }
                        
                    } else {
                        alert("Errore nell'elaborazione della richiesta. Contattare l'amministratore o riprovare più tardi.");
                        console.log(res);
                        $('#addOrder').attr('disabled', true);
                        return;
                    }

                }).fail(function (err) {
                    console.log(err);
                });
            } else if (azi != "S") {
                let newItems = []
                $('.single-item-selected').each(function(index, obj){
                    if(!$(this).hasClass('cached')){
                        newItems.push(parseFloat($(this).find('#totalPrice').val().replace('€','').replace('.','').replace(',','.')));
                    }
                });

                const sum = newItems.reduce((psum, a) => psum + a); 
                const remaining = parseFloat($('#saldo_rimanente').attr('data-remaining'));
                if (sum > remaining) {
                    ademAlert.error({
                        message: 'Somma eccede il saldo rimanente'
                    });

                    return;
                }else{
                    $.get(queryString.trim().replace(/ /g, '').replace('%C2%A0', ''), function (result) {
                        let res = JSON.parse(result);
                        console.log(res);
                        if(res["code"] == "00"){
                            localStorage.removeItem('persistence');
                            localStorage.removeItem('userEstim');
                            localStorage.setItem('clientId', CLI);
                            localStorage.setItem('orderId', ido);
                            window.location.href = "/";
                        }
                    });
                }
            }
           

        });
    });
   
    
}
function getAllCustomers() {
    sessionStorage.removeItem('/anagrafica/getallcustomers');
    $.get('/anagrafica/getallcustomers', function (obj) {
        
        autoCustomers = [];
        
        let customers = JSON.parse(obj);

        localStorage.setItem("customers", obj);
        for (let customer in customers) {
                let autoCustomer = customers[customer]['CompanyName'] + " ( " + customers[customer]['AS400_CODE'] + " )";
                autoCustomers.push(autoCustomer);
        }
        localStorage.setItem('autoCustomers', JSON.stringify(autoCustomers));
        return autoCustomers;
    });
}
function getCustomersByAgent() {
    let agentCode = $('#agentcode').val();
    $.get('/list/getcustomersasync?agentid=' + agentCode, function (object) {


        sessionStorage.setItem("customers", object);
        let customers = JSON.parse(object);
        let autoCustomers = [];
        for (let customer in customers) {
            //let autoCustomer = customers[customer]['Name'].replace(/ /g, "") + " " + customers[customer]['Surname'].replace(/ /g, "");
            if (parseInt(customers[customer]['AS400_CODE']) > 300000 && customers[customer]['Active'] == 1) {
                let autoCustomer = customers[customer]['CompanyName'] + " ( " + customers[customer]['AS400_CODE'] + " )";
                autoCustomers.push(autoCustomer);
            }
        }
        //sessionStorage.setItem("customersTags", autoCustomers);
        if (window.location.href.indexOf('editMode=true') > -1 && window.location.pathname !="/archive/SearchOrderClients"){
            $('#autoComplete').val(getCustomerNameById($('#ordCli').val()));
            $('#autoComplete').attr('disabled', 'disabled');
        };
        initAutoComplete(autoCustomers);
        
    });
}
function getCustomersByAgentFatt() {
    if (localStorage.getItem('customers') != "[]" && localStorage.getItem('customers') != null) {
        console.log("HEY");
        let customers = JSON.parse(localStorage.getItem('customers'));
        let autoCustomers = [];
        for (let index in customers) {
            let customer = customers[index];
            if (parseInt(customer.AS400_CODE) < 300000) {
                let autoCustomer = customer['CompanyName'] + " ( " + customer['AS400_CODE'] + " )";
                console.log(autoCustomer);
                autoCustomers.push(autoCustomer);
            }
        }
        initAutoComplete(autoCustomers);
    } else {
        $.get('/anagrafica/getinvoicecustomers', function (object) {

            sessionStorage.setItem("customers", object);
            let customers = JSON.parse(object);
            let autoCustomers = [];
            for (let customer in customers) {
                let autoCustomer = customers[customer]['Dipartimento'] + " ( " + customers[customer]['Fatturazione'] + " )";
                autoCustomers.push(autoCustomer);
            }
            //sessionStorage.setItem("customersTags", autoCustomers);
            // $('#autoComplete').val(getCustomerNameById($('#ordCli').val()));
            initAutoComplete(autoCustomers);

        });
        
    }
    
}
function getCustomerByFattCode() {
    let CLI;
    let customers;
    if (localStorage.getItem('customers') != null && localStorage.getItem('customers') != "[]") {
        customers = JSON.parse(localStorage.getItem('customers'));
    } else {
        customers = JSON.parse(sessionStorage.getItem("customers"));
    }

    
    for (let i in customers) {
        let object = customers[i];

        if (object['CompanyName'] + " ( " + object['AS400_CODE'] + " )" === $("#autoComplete").val()) {
            CLI = object;
            return CLI;
         
        }
    }

    return CLI;
}
function setupNavigation() {
    var pagination = parseInt($('#page').text());

    if (pagination === 1) {
        $('#prev').addClass("disabled").removeAttr('href');
    } else if (pagination > 1) {
        $('#prev').removeClass("disabled").attr('href', '#');
    }

    if ($('.single-product').length > 18 || $('.item').length > 18) {
        $('#next').removeClass("disabled").attr('href', '#');
    } else {
        $('#next').addClass("disabled").removeAttr('href');
    }
}
function getCategories() {
    $.get('/products/getCategories', function (object) {
        for (const i in object) {
            $('#category').append('<option id="' + object[i]['pk']+ '">' + object[i]['Name'] + '</option>');
        }
    });

}
function filterByCausal() {
    $('tbody>tr').each(function (i, obj) {
        $(obj).find('td').each(function (i, td) {
            
            if (i == 4) {
                if ($(td).text().indexOf($('#causal').val() + " ") == -1) {
                    $(td).parent().fadeOut(0);
                } else {
                    $(td).parent().fadeIn(0);
                }
               
            }
        });
        
    });
}
function getProductFullPage() {
    clearProducts();
    let url = new URL(window.location.href);
    let sku = url.searchParams.get("sku");
    let onlyAvailable = url.searchParams.get("onlyavailable");
    let typeOfSearch = url.searchParams.get("type");
    let search = url.searchParams.get("search");
    let startitem = parseInt($('#page').first().text()) * 20 - 20;

    $.get('/products/getResultAsync?sku=' + sku + '&type=' + typeOfSearch + "&startitem=" + startitem + "&itemPage=" + 25 + "&onlyAvailable=" + 1 + "&search=" + search, function (object) {
        generatePartialProductFragment(JSON.parse(object));
        setupNavigation();
    }).fail(function (e) {
        console.log("Fallito " + e);
    });
}

function initAutoComplete(array) {
    let customers = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        // `states` is an array of state names defined in "The Basics"
        local: array
    });
    $('#autoComplete').typeahead({
        minLength: 4,
    }, {
        name: 'customers',
        source: customers,
        limit: 15000,   
        
        });
    
    if (window.location.pathname == "/archive/SearchOrderClients") {
        $('#autoComplete').removeAttr('disabled');
    }
}
function updateTotal() {
    let total = 0;

    $('.total').each((i, obj) => {
        console.log($('#orderType').val() != "000");
        if ($('#orderType').val() === "000") {
            if (!$(obj).closest('tr').hasClass('cached')) {
                let discounted = parseFloat($(obj).parent().parent().find('.discounted').first().val().replace('.', '').replace(',', '.'));

                let qty = parseInt($(obj).parent().parent().find('#qty').first().val());

                let calculated = discounted * qty;

                if (!isNaN(calculated)) {
                    $(obj).val(calculated.toLocaleString('it-IT', { style: 'currency', currency: 'EUR' }));
                }

                let cac = parseFloat($(obj).val().replace('.', '').replace(',', '.'));

                total = total + cac;
            }
        } else {
            let discounted = parseFloat($(obj).parent().parent().find('.discounted').first().val().replace('.', '').replace(',', '.'));

            let qty = parseInt($(obj).parent().parent().find('#qty').first().val());

            let calculated = discounted * qty;

            if (!isNaN(calculated)) {
                $(obj).val(calculated.toLocaleString('it-IT', { style: 'currency', currency: 'EUR' }));
            }

            let cac = parseFloat($(obj).val().replace('.', '').replace(',', '.'));

            total = total + cac;
        }
        
       
    });

    $('#subtotale').text(total.toLocaleString('it-IT', { style: 'currency', currency: 'EUR' }));
    jcf.replaceAll();
}



$(document).on('click', '.editDiscount', function (e) {
    $('.active').removeClass('active');
    $(this).closest('tr').addClass('active');

    $('#fornitore').val($('.active').children('td').first().text());
    $('#listino').val($('.active').children('td:nth-child(2)').text());
    $('#sconto_libero').val($('.active').children('td:nth-child(3)').text());
    $('#sconto_autorizzato').val($('.active').children('td:nth-child(4)').text());
    $('#oltre_40').val($('.active').children('td:nth-child(5)').text());

    
});



$('#saveDiscount').click(function (e) {


    let discounts = JSON.parse($('#discountList').val());

    let discount = discounts[$('#fornitore').val()];

    if (discount === undefined) discount = {};


    discount['Listino'] = $('#listino').val();
    discount['Libero'] = $('#sconto_libero').val();
    discount['Autorizzato'] = $('#sconto_autorizzato').val();
    discount['Over'] = $('#oltre_40').val() === "Sì" ? 'true' : 'false';

    discounts[$('#fornitore').val()] = discount;

    $('#discountList').val(JSON.stringify(discounts));

    $.ajax({
        type: 'POST',
        url: '/amministrazione/savediscount',
        data: JSON.stringify({ json: discounts }),
        success: function (obj) {
            window.location.reload();
        },
        contentType: 'application/json',
        dataType: 'json'
    });

})

$('.delete-agent').click(function (e) {
    //$(this).parent().parent().parent().parent().find('.code').text()
    const code = $(this).parent().parent().parent().parent().find('.code').text();

    $.get('/amministrazione/deleteagent?code=' + code, function (obj) {

        ademAlert.success({
            message: 'Agente eliminato con successo'
        });
       
    });
});
$('#search-filters').on('change', function (e) {
    let type = $(this).val();
    if (type == 2) {
        $('.search-category').fadeIn();
    } else {
        $('.search-category').fadeOut();
    }
});
$('#prev').click(function (e) {
    let pagination = parseInt($('#page').text());
    if (!$(this).hasClass("disabled")) {
        $('#page').text(pagination - 1);
        if ($(this).hasClass('product')) {
            clearProducts();
            if ($(this).hasClass('fullPage')) {
                getProductFullPage();
            } else {
                getPreviousProducts();
            }
            
        } else {
            clearUsers();
            getCustomers();
        }
        setupNavigation();
    }
});

$('#next').click(function (e) {
    let pagination = parseInt($('#page').text());
    if (!$(this).hasClass("disabled")) {
        $('#page').text(pagination + 1);
        
        if ($(this).hasClass('product')) {
            clearProducts();
            if ($(this).hasClass('fullPage')) {
                getProductFullPage();
            } else {
                getNextProducts();
            }
        } else {
            clearUsers();
            getCustomers();
        }
        setupNavigation();
    }
});
$('#close_modal').click(function (e) {
    $('#search-step').fadeIn().delay(200);
    $('#show-step').fadeOut().delay(200);
    $('#page').text('1');
    $('#sku').val('');
});
$('#more-options').click(function (e) {
    e.preventDefault();
    
    let child = $(this).children('.fa-angle-left').first();
    child.css({ 'transition': 'linear 0.2s'});
    if (!$(this).hasClass('expanded')) {
        $(this).addClass('expanded');
        $('.more-options').first().fadeIn();
        child.css({ 'transform': 'rotate(-90deg)' });
    } else {
        $(this).removeClass('expanded');
        $('.more-options').first().fadeOut();
        child.css({ 'transform': 'rotate(0deg)' });
    }
   
});
$('#delivery-customer-search').click(function (e) {
    e.preventDefault();

    let societa = $('#autoComplete').val().split(' (')[0];
    let type = $("#search input[type='radio']:checked").val();
    let queryString;
    console.log(type);
   
    if (type != 0) {
        
        const CLI = getInvoiceCode();
        if (cli === false) {
            return;
        }
        queryString = "/anagrafica/result?name=" + CLI + "&type=" + type
    } else {
        queryString = "/anagrafica/result?name=" + societa + "&type=" + type
    }
    if (type == "undefined" || societa.length === 0) {
        alert("Tutti i campi sono obbligatori.");
    } else {
        window.location.href = queryString;
    }
    
});
$('#invoice-customer-search').click(function (e) {
    e.preventDefault();
    let name = $('#nome').val();
    let surname = $('#cognome').val();
    let provincia = $('#provincia').val();
    let citta = $('#citta').val();
    let piva = $('#piva').val();
    let societa = $('#societa').val();
    let correctName = $('#autoComplete').val();
    let CLI = getInvoiceCode();
    window.location.href = "/anagrafica/result?name=" + name + "&surname=" + surname + "&provincia=" + provincia + "&citta=" + citta + "&type=" + 1 + "&piva=" + piva + "&cli="+CLI;

});
$('.typeahead').on('typeahead:selected', function (evt, item) {
    $(this).attr('disabled', true);
    $(this).val(item);
});
$('.submit').click(function (e) {
    e.preventDefault();

    if ($(this).data('target') === "lsord") {

        let dateBeg = $('#myDatepicker').find('input').val();
        let dateEnd = $('#myDateEnd').find('input').val();
        let product = $('#product').val();
        let idord = $('#ord').val();

        if (product == "") {
            let cli = getCustomerCode();
            let name = $('#autoComplete').val();
            let ido;
            let pref = "ALL";
            if (name == "" && idord == "" && product == "") {
                alert("Inserire cliente o codice prodotto");
            } else {
                if (name != "") {
                    ido = "000000";
                } else {
                    name = "";
                    ido = $('#ord').val();
                }
                if ($(this).attr('href').indexOf("?") == -1) {
                    window.location.href = $(this).attr('href') + "?" + "cli=" + cli + "&" + "pref=" + pref + '&dateBeg=' + dateBeg + '&dateEnd=' + dateEnd + "&ido=" + ido;
                } else {
                    window.location.href = $(this).attr('href') + "&" + "cli=" + cli + "&" + "pref=" + pref + '&dateBeg=' + dateBeg + '&dateEnd=' + dateEnd + "&ido=" + ido;
                }
            }
        } else {
            let cli = $('#autoComplete').val();
            if (cli == "" || cli == null) {
                cli = "ALL";
            } else {
                cli = getCustomerCode();
            }
            let productCode = $('#product').val();

            $.get('/products/getresultasync?sku=' + productCode + '&type=' + 0 + '&startItem=0', function (obj) {
                let object = JSON.parse(obj)[0];
                let as400_id = object['AS400_ID'];

                window.location.href = "/archive/listbyproduct?cli=" + cli + "&art=" + as400_id + '&product=true&dateBeg=' + dateBeg + '&dateEnd=' + dateEnd;
            });
        }

    } else if ($(this).data('target') === "srcprod") {
        let sku = $('#sku').val();
        let typeOfSearch = $('input[name=type_of_search]:checked').val();
        if (typeOfSearch == null) {
            typeOfSearch = 3;
        }
        let search = $('#search-filters').val();


        window.location.href = $(this).attr('href') + "?" + "sku=" + sku + "&" + "type=" + typeOfSearch + "&startItem=0&onlyavailable=" + 1 + "&search=" + search + "&p=1";
    } else if ($(this).data('target') === "prord") {
        let cli = getCustomerCode();
        let name = $('#autoComplete').val();
        let prod = $('#prod').val();
        let pre = $('#pre').val();
        let dateBeg = $('#myDatepicker').find('input').val();
        let dateEnd = $('#myDateEnd').find('input').val();


        if (name == "" && prod == "" && pre == "") {
            alert("Scegliere un cliente o inserire codice prodotto");
        } else {

            if (pre != "") {
                window.location.href = '/preventivi/showresultpre?pre=' + pre;
            } else {
                if (name != "" && prod == "") {
                    window.location.href = $(this).attr('href') + "?" + "cli=" + cli + '&dateBeg=' + dateBeg + '&dateEnd=' + dateEnd;
                } else if (name == "" && prod != "") {

                    $.get('/products/getresultasync?sku=' + prod + '&type=' + 0 + '&startItem=0', function (obj) {
                        let json = JSON.parse(obj)[0];

                        window.location.href = '/preventivi/ShowListByArt?cli=ALL&art=' + json['AS400_ID'] + '&dateBeg=' + dateBeg + '&dateEnd=' + dateEnd;
                    });
                } else if (name != "" && prod != "") {

                    $.get('/products/getresultasync?sku=' + prod + '&type=' + 0 + '&startItem=0', function (obj) {
                        let json = JSON.parse(obj)[0];
                        window.location.href = '/preventivi/ShowListByArt?cli=' + cli + '&art=' + json['AS400_ID'] + '&dateBeg=' + dateBeg + '&dateEnd=' + dateEnd;
                    });
                }
            }
        }

    } else if ($(this).data('target') === "dlsord") {
        let cli = getCustomerCode();
        let name = $('#autoComplete').val();

        if (name == "") {
            window.location.href = "/archive/showlistbyagent";
        } else {
            window.location.href = $(this).attr('href') + "&cli=" + cli + "&agent=" + $('#agentcode').val();
        }
    } 
});

$('.product-submit').click(function (e) {
    e.preventDefault();
});

$('.docDetail').click(function (e) {
    let target = e.target;

    const doc = $(target).data('doc');
    const aad = $(target).data('aad');


});
$(document).on('click', '.sendMail', function (e) {
    let customer = getCustomer();
    const email = customer['Email'].trimRight();
    const openUrl = $(this).attr('data-open-url');
    let json = {};
    json['printUrl'] = openUrl;
    json['email'] = email;
    json['name'] = customer["Name"].trimRight() + " " + customer["Surname"].trimRight();

    $.ajax({
        type: "POST",
        url: "/list/sendMail",
        data: JSON.stringify(json),
        dataType: 'json',
        success: function (e) {
            //window.location.href = openUrl;
            ademAlert.success({
                message: 'Email inviata con successo'
            });
            
        },
    });

});
$('#modalToggle').click(function (e) {
    if ($('#cliente').val().length == 0) {
        e.stopPropagation();
        alert("SCEGLIERE PRIMA UN CLIENTE");
    }
    
});

$('#search_product_async').click(function (e) {
    if ($('#autoComplete').val() != "") {
        getProducts();
    } else {
        alert("Inserisci il nome cliente");
        e.stopPropagation();
    }

});

$('#products').on("click",".add-to-list", function (e) {
    e.preventDefault();
    
    if (parseFloat($(this).parent().parent().find('.price-customer').text().trim().replace('€', '')) <= 0.10) {
        alert("Prezzo da aggiornare");
        $('#search-step').fadeIn().delay(200);
        $('#show-step').fadeOut().delay(200);
        $('#page').text('1');
        $('#modalSlideUp').modal('hide');
        $('#sku').val("");
    } else {
        generateSelectedFragment($(e.target).data('index'));
        
        $('#search-step').fadeIn().delay(200);
        $('#show-step').fadeOut().delay(200);
        $('#page').text('1');
        $('#modalSlideUp').modal('hide');
        $('#sku').val("");

        $('tr.single-product').each(function (obj) {
            
        });
        sessionStorage.setItem('products', JSON.stringify($('tr.single-product')));
    }
});

$('.quick-search').change(function (e) {
    let target = $(e.currentTarget).children("option:selected").attr('id');   
    window.location.href = "/products/result?type=4&sku=" + target + "&startItem=0&p=1";
});
$('#addOrder').click(function (e) {
    const urlParams = new URLSearchParams(window.location.search);

    let highestIndex = 0;
    $('.single-item-selected').each(function (index, obj) {

        $(obj).find('.discounted').trigger('mouseout');

        
        if (parseInt($(this).data('row')) > highestIndex) {
            highestIndex = parseInt($(this).data('row'));
           
        }
    });
    if (canBeSent) {
        sendOrder(highestIndex);
    }

    canBeSent = true;
});
$('.logout').click(function (e) {
    localStorage.clear();
});
$('.edit-agent').click(function (e) {
    const parent = $(this).parent().parent().parent().parent();
    console.log(parent);
    const agent = parent.find(".code").text();
    const desc = parent.find(".desc").text();
    const email = parent.find(".email").text();
    const telefono = parent.find(".cellulare").text();

    $('#modalSlideUp').find('#code').val(agent);
    $('#modalSlideUp').find('#desc').val(desc);
    $('#modalSlideUp').find('#email').val(email);
    $('#modalSlideUp').find('#cellulare').val(telefono);
});
$('#btn-change-agent').click(function (e) {
    const code = $('#modalSlideUp').find('#code').val();
    const desc = $('#modalSlideUp').find('#desc').val();
    const email = $('#modalSlideUp').find('#email').val();
    const telefono = $('#modalSlideUp').find('#cellulare').val(); 
    const password = $('#modalSlideUp').find('#password').val();

    $.get('/amministrazione/editagent?code=' + code + '&desc=' + desc + '&email=' + email + '&telefono=' + telefono + '&password=' + password, function (e) {
        window.location.reload();
    });
});
$('#orderType').on('change', function (e) {
    $(this).attr('disabled', true);
});
$('.products-selected').on('click', '.removeProduct', function(e){
    e.preventDefault();
    let row = $(this).parent().parent();
    let id = row.attr('id');
    let nrg = row.data('row');
    let qty = row.children().find('#qty').first().val();
    let prz = parseFloat(row.children().find('.total').first().val().replace('€', '').replace('.', '').replace(',', '.')) / qty;
    if ($('#azi').val() == "S") {
        deleteRow(nrg, id, qty, prz);
    }
    let items = JSON.parse(localStorage.getItem('persistence'));

    $(items).each(function (ind) {

        if ($(this)[0]['AS400_ID'] == id) {
            items.splice(ind);
            localStorage.setItem('persistence', JSON.stringify(items));
        }
    });
    $(this).parent().parent().remove();
    updateTotal();

    
});
$('.go-back').click(function (e) {
    e.preventDefault();
    window.history.back();
});
$('.print').click(function (e) {
    e.preventDefault();
    const elementClicked = $(this);
    const target = $(this).data().target;
    const cli = $(this).data().ordcli;
    const id = $(this).data().ordid;

    $.get('/preventivi/getpreasync?pre=' + id, function (obj) {
        let prev = JSON.parse(obj);
        let pdfElements = {};

        
        let user = getCustomerWithParams(cli);
        if (user == null) {
            $.get('/anagrafica/getuserbyid?AS400_ID=' + cli).then(function (user) {
                user = JSON.parse(user);
                let name = user['Name'].trimRight() + " " + user['Surname'].trimRight();
                let products = [];

                for (let ind in prev) {
                    let item = {};
                    pre = prev[ind];

                    item['qty'] = pre['Quantita'].split('.')[0];
                    item['sku'] = pre['Codice'];
                    item['name'] = pre['Descr'];
                    item['prz'] = pre['Valore riga'];
                    products.push(item);

                }

                pdfElements['customer'] = name;
                pdfElements['address'] = user['Address'] + " " + user['CAP'] + " " + user['City'] + " " + user['Province'];
                pdfElements['company'] = user['Descrizione'];
                pdfElements['idOrd'] = prev[0]['Id interno'];
                pdfElements['products'] = products;
                let agent = $('#agentname').val() + " - " + $('#agentcell').val() + " " + $('#agentemail').val();
                pdfElements['agent'] = agent;

                window.location.href = 'https://desk.microtech.eu/template.html?order='.concat(encodeURIComponent(JSON.stringify(pdfElements)));
            });
        } else {
            let name = user['Name'].trimRight() + " " + user['Surname'].trimRight();
            let products = [];

            for (let ind in prev) {
                let item = {};
                pre = prev[ind];

                item['qty'] = pre['Quantita'].split('.')[0];
                item['sku'] = pre['Codice'];
                item['name'] = pre['Descr'];
                item['prz'] = pre['Valore riga'];
                products.push(item);

            }

            pdfElements['customer'] = name;
            pdfElements['address'] = user['Address'] + " " + user['CAP'] + " " + user['City'] + " " + user['Province'];
            pdfElements['company'] = user['Descrizione'];
            pdfElements['idOrd'] = prev[0]['Id interno'];
            pdfElements['products'] = products;
            let agent = $('#agentname').val() + " - " + $('#agentcell').val() + " " + $('#agentemail').val();
            pdfElements['agent'] = agent;

            window.location.href = 'https://desk.microtech.eu/template.html?order='.concat(encodeURIComponent(JSON.stringify(pdfElements)));
        }
        

    });
});
$('.pdf-list').click(function (e) {
    e.preventDefault();

    $.get('/archive/listdetailsasync?cli=' + $('#clientCode').val(), function (obj) {
        $.get('/anagrafica/getuserbyid?AS400_ID=' + $('#clientCode').val(), function (customer) {
            let prev = JSON.parse(obj);
            let user = customer;
            let pdfElements = {};
            user = JSON.parse(user);
            let name = user['Name'].trimRight() + " " + user['Surname'].trimRight();
            let products = [];

            for (let ind in prev) {
                let item = {};
                pre = prev[ind];
                console.log(pre);
                item['qty'] = '/';
                item['sku'] = pre['SKU'];
                item['name'] = escape(pre['Name']);
                item['price'] = pre['Price'].replace('.', '').replace(',', '.');
                item['valore'] = pre['Price'].replace('.', '').replace(',', '.');
                console.log(item);
                products.push(item);

            }

            pdfElements['customer'] = name;
            pdfElements['address'] = user['Address'] + " " + user['CAP'] + " " + user['City'] + " " + user['Province'];
            pdfElements['company'] = user['Descrizione'];
            pdfElements['idOrd'] = user['AS400_CODE'];
            pdfElements['products'] = products;

            let agent = JSON.parse($('#agent').val());
            let agentInfo = agent['Descrizione'] + " - " + agent['Cellulare'] + " " + agent['Email'];
            pdfElements['agent'] = agentInfo;

            console.log(pdfElements);
            $.ajax({
                type: 'POST',
                url: '/preventivi/getInvoice',
                data: JSON.stringify(pdfElements),
                contentType: "application/json; charset=utf-8",
                dataType: 'json'
            }).then(function (obj) {
                let pathArray = obj['path'].split('/');
                let fileName = pathArray[pathArray.length - 1];
                setTimeout(function () { window.location.href = 'https://desk.microtech.eu/fatture/' + fileName; }, 800);

            });
        });
    });
});


$(document).on('click', '.pdf', function (e) {
    e.preventDefault();

    const elementClicked = $(this);
    const target = $(this).data().target;
    const cli = $(this).attr('data-ordcli');
    const id = $(this).attr('data-ordid');


    $.get('/preventivi/getpreasync?pre=' + id, function (obj) {
        let prev = JSON.parse(obj);
        let pdfElements = {};


        let user = getCustomerWithParams(cli);
        if (user == null) {
            $.get('/anagrafica/getuserbyid?AS400_ID=' + cli).then(function (user) {

                user = JSON.parse(user);
                let name = user['Name'].trimRight() + " " + user['Surname'].trimRight();
                let products = [];

                for (let ind in prev) {
                    let item = {};

                    pre = prev[ind];
                    item['qty'] = pre['Quantita'].split('.')[0];
                    item['sku'] = pre['Codice'];
                    item['name'] = escape(pre['Descr']);
                    
                    $.get('/products/GetProductPieces?as400_id=' + pre['Id Articolo'], function (quantity) {
                        products[ind]['pz'] = quantity;                       
                    });
                    item['price'] = pre['Prezzo'];
                    item['valore'] = pre['Valore riga'];
                    products.push(item);
                }

                pdfElements['customer'] = name;
                pdfElements['address'] = user['Address'] + " " + user['CAP'] + " " + user['City'] + " " + user['Province'];
                pdfElements['company'] = user['CompanyName'].replace(user['Name'].trimRight(), '').replace(user['Surname'].trimRight(), '');
                pdfElements['idOrd'] = prev[0]['Id interno'];
                pdfElements['products'] = products;

                //EMAIL AGENTI SPECIALI
                let email = $('#agentemail').val();

                if (email == "c01@microtech.eu") email = "m.papagno@microtech.eu";
                if (email == "c02@microtech.eu") email = "mt.papagno@microtech.eu";
                if (email == "s01@microtech.eu") email = "e.cavallaro@microtech.eu";
                if (email == "s02@microtech.eu") email = "l.damico@microtech.eu";

                let agent = $('#agentname').val() + " - " + $('#agentcell').val() + " " + email;
                pdfElements['agent'] = agent;

                let url = '/preventivi/getInvoice?order='.concat(encodeURIComponent(JSON.stringify(pdfElements)));

                let timeout = null;
                let first = true;
            
                $(document).ajaxStop(function () {

                    if (first) {
                        timeout = setTimeout(function () {
                            $.ajax({
                                type: 'POST',
                                url: '/preventivi/getInvoice',
                                data: JSON.stringify(pdfElements),
                                contentType: "application/json; charset=utf-8",
                                dataType: 'json'
                            }).then(function (obj) {
                                let pathArray = obj['path'].split('/');
                                let fileName = pathArray[pathArray.length - 1];
                                window.location.href = 'https://desk.microtech.eu/fatture/' + fileName;
                                first = false;
                            });
                        }, 2500);
                    }
                   
                    
                })
                

            });
        } else {
            let name = user['Name'].trimRight() + " " + user['Surname'].trimRight();
            let products = [];

            for (let ind in prev) {
                let item = {};
                pre = prev[ind];

                item['qty'] = pre['Quantita'].split('.')[0];
                item['sku'] = pre['Codice'];
                item['name'] = escape(pre['Descr']);
                item['price'] = pre['Prezzo'];
                item['valore'] = pre['Valore riga'];
                console.log(item);
                products.push(item);

            }

            pdfElements['customer'] = name;
            pdfElements['address'] = user['Address'] + " " + user['CAP'] + " " + user['City'] + " " + user['Province'];
            pdfElements['company'] = user['Descrizione'];
            pdfElements['idOrd'] = prev[0]['Id interno'];
            pdfElements['products'] = products;
            let agent = $('#agentname').val() + " - " + $('#agentcell').val() + " " + $('#agentemail').val();
            pdfElements['agent'] = agent;

            let url = '/preventivi/getInvoice?order='.concat(encodeURIComponent(JSON.stringify(pdfElements)));

            $.ajax({
                type: 'POST',
                url: '/preventivi/getInvoice',
                data: JSON.stringify(pdfElements),
                contentType: "application/json; charset=utf-8",
                dataType: 'json'
            }).then(function (obj) {
                let pathArray = obj['path'].split('/');
                let fileName = pathArray[pathArray.length - 1];
                setTimeout(function () { window.location.href = 'https://desk.microtech.eu/fatture/' + fileName; }, 800);

            });
        }
        

    });
});
$(document).on('input', '.discount', function (e) {
    let object = $(this);
    //console.log(object.closest('tr'));
    if (!$(this).is(':disabled')) {
        if ($(this).val() == "") {
            $(this).parent().parent().find('.discounted').first().val($(this).parent().parent().find('#prezzo').val());
            jcf.replaceAll();
        }
        jcf.replaceAll();
        if (parseInt($(this).val()) > 30 && parseInt($(this).val()) < 41 && $('#agentcode').val() != "C01" && $('#agentcode').val() != "C02") {
            if (window.location.search.indexOf("type=WPR") > -1) {
                ademAlert.warning({
                    message: 'SCONTO ELEVATO RISPETTO ALLA MEDIA SCONTI (MARGINE AZIENDALE BASSO)'
                });



            } else {
                if ($('#agentcode').val() != "C01" && $('#agentcode').val() != "C02") {
                    $(this).val(0);

                    ademAlert.error({
                        message: 'Prezzo non autorizzato'
                    });

                    canBeSent = false;
                }
            }
            jcf.replaceAll();
        } else if (parseInt($(this).val()) > 40 && $('#agentcode').val() != "C01" && $('#agentcode').val() != "C02") {

            $(this).val(0);
            jcf.replaceAll();
            ademAlert.error({
                message: 'Prezzo non autorizzato'
            });
            updateTotal();

            canBeSent = false;

        }
        let discount = $(this).val();

        let unitPrice = parseFloat($(this).parent().parent().find('#prezzo').val().replace('.', '').replace(',', '.'));
        console.log(unitPrice);
        let discounted = unitPrice - ((unitPrice / 100) * parseFloat($(this).val()));
        console.log($(this).val());
        if (isNaN(discounted)) {
            discounted = unitPrice;
        }
        if (discounted != null) {
            $(this).parent().parent().find('.discounted').first().val(discounted.toLocaleString('it-IT', { style: 'currency', currency: 'EUR' }));
        } else {
            $('.discounted').val(unitPrice);
        }
        if ((parseFloat($(this).closest('tr').attr('data-autorizzato')) * 100) < discount && $('#agentcode').val() != "C01" && $('#agentcode').val() != "C02") {
            ademAlert.error({
                message: 'Prezzo non autorizzato'
            });

            object.val(parseFloat($(this).closest('tr').attr('data-autorizzato')));
            let unitPrice = parseFloat(object.parent().parent().find('#prezzo').val().replace(',', '.'));
            let discounted = unitPrice - ((unitPrice / 100) * parseFloat($(this).closest('tr').attr('data-autorizzato')));

            if (discounted != null) {
                object.parent().parent().find('.discounted').first().val(discounted.toLocaleString('it-IT', { style: 'currency', currency: 'EUR' }));
                updateTotal();
            } else {
                $('.discounted').val(unitPrice.toLocaleString('it-IT', { style: 'currency', currency: 'EUR' }));
                updateTotal();
            }
        }

        updateTotal();
    }
   
});
$(document).on('input', '.discounted', function (e) {
    if (!$(this).is(':disabled')) {
        let discount = 100 - (parseFloat($(this).val().replace('.','').replace(',','.')) * 100 / parseFloat($(this).parent().parent().find('#prezzo').val().replace('.', '').replace(',', '.')));
        $(this).parent().parent().find('.discount').val(parseFloat(discount).toFixed(2));
        updateTotal();
    }
});
$(document).on('mouseout', '.discount', function (e) {
    if (!$(this).is(':disabled')) {
        if ($(this).val() == "") {
            $(this).val(0);
        }

        $('.discounted').trigger('mouseout');
    }

});

$(document).on('mouseout', '.discounted', function (e) {
    if (!$(this).is(':disabled')) {
        $(this).closest('tr').find('.discount').trigger('input');
        let object = $(this);
        let discount = $(this).parentsUntil('tr').find('.discount').val();
        if (parseFloat($(this).parent().parent().find('.discount').val()) > 30 && parseFloat($(this).parent().parent().find('.discount').val()) < 40 && $('#agentcode').val() != "C01" && $('#agentcode').val() != "C02") {
            if (window.location.search.indexOf("type=WPR") > -1) {

                ademAlert.warning({
                    message: 'SCONTO ELEVATO RISPETTO ALLA MEDIA SCONTI (MARGINE AZIENDALE BASSO)'
                });

                updateTotal();
            } else {
                if ($('#agentcode').val() != "C01" && $('#agentcode').val() != "C02") {

                    ademAlert.error({
                        message: 'Prezzo non autorizzato'
                    });
                    $(this).parentsUntil('tr').find('.discount').val(0);
                    updateTotal();

                    canBeSent = false;
                }

            }

            jcf.replaceAll();


        } else if (parseFloat($(this).parent().parent().find('.discount').val()) > 40 && $(this).attr('disabled') != "disabled" && $('#agentcode').val() != "C01" && $('#agentcode').val() != "C02") {

            ademAlert.error({
                message: 'Prezzo non autorizzato'
            });

            $(this).parentsUntil('tr').find('.discount').val(0);
            $(this).val($(this).parent().parent().find('#prezzo').val());

            updateTotal();
            jcf.replaceAll();

            canBeSent = false;


        }

        if ((parseFloat($(this).closest('tr').attr('data-autorizzato')) * 100) < discount && $('#agentcode').val() != "C01" && $('#agentcode').val() != "C02") {
            ademAlert.error({
                message: 'Prezzo non autorizzato'
            });
            $(this).parentsUntil('tr').find('.discount').val(0);
            console.log($(this).parentsUntil('tr').find('.discount'));
            let unitPrice = parseFloat($(this).parentsUntil('tr').find('#prezzo').val().replace(',', '.'));
            let discounted = unitPrice - ((unitPrice / 100) * parseFloat($(this).closest('tr').attr('data-autorizzato')));

            if (discounted != null) {
                object.parent().parent().find('.discounted').first().val(discounted.toLocaleString('it-IT', { style: 'currency', currency: 'EUR' }));
                updateTotal();
            } else {
                $('.discounted').val(unitPrice.toLocaleString('it-IT', { style: 'currency', currency: 'EUR' }));
                updateTotal();
            }
        }
        jcf.replaceAll();
    }
});


$(document).on('input', '#qty', function (e) {
    let discounted = parseFloat($(this).parent().parent().find('.discounted').first().val());

    $(this).parent().parent().find('#totalPrice').val((discounted * parseFloat($(this).val())) + "€");
    updateTotal();
    jcf.replaceAll();
});


$(document).ready(function (e) {

    sessionStorage.removeItem('generated');
    if ($('#orderType').val() == "WLS") {
        getCustomersByAgentFatt();
    } else {
        if (($('#agentcode').val() == "C01" || $('#agentcode').val() == "C02" || $('#agentcode').val() == "S01" || $('#agentcode').val() == "S02" || $('#agentcode').val() == "M01" || $('#agentcode').val() == "M03") && localStorage.getItem('customers') == null) {
            getAllCustomers();
        } else {
            if (localStorage.getItem('customers') == null) {
                getCustomersByAgent();
            } else {
                initAutoComplete(JSON.parse(localStorage.getItem('autoCustomers')));
            }
            
        }
       
    }
    
    if (window.location.pathname == "/List/Crea") {
        if ($('#ordCli').val() == "undefined") {
            let products = JSON.parse($('#productsJson').val());
            console.log($('#ordCli').val(products['ID Cliente']));
        }
        getCustomersByAgent();
        if (sessionStorage.getItem("customers")) {
            $('#autoComplete').val(getCustomerNameById($('#ordCli').val()));
        }
        getCategories();
    }

    if (window.location.pathname == "/list/crea") {
        if (localStorage.getItem('persistence') != null && window.location.href.indexOf('editMode=true') == -1) {
            //PERSISTENCE START POINT
            

            let persistence = JSON.parse(localStorage.getItem('persistence'));

            for (let i in persistence) {
                let obj = persistence[i];
                if ($('#orderType').val() == localStorage.getItem('orderType')) {

                    $('#autoComplete').val(localStorage.getItem('userEstim'));
                    $('#autoComplete').attr('disabled', 'disabled');
                    if ($('#autoComplete').val() != "") {
                        CachedFragment(obj);
                    }

                } else {
                    localStorage.removeItem('persistence');
                    localStorage.removeItem('userEstim');
                    localStorage.removeItem('orderType');
                }
                
            }

        }
    }
    
    if ($('#category').length) {
        getCategories();
    }
    if (window.location.pathname == "/Preventivi/ShowResultList") {

        $('#title').append(getCustomerName());
    };

    if (window.location.pathname == "/archive/ShowResultList") {
        if ($('#type').val() == "WOP") {
            $('tbody>tr').each((index, obj) => {
                $(obj).children().each((i, o) => {
                    if (i == 4 && $(o).text().indexOf("BA ") == -1) {

                        $(o).parent().fadeOut(0);
                    }
                });
                
            });
        }
    }

    if (window.location.pathname == "/" || window.location.pathname == "/login/Login") {
        localStorage.removeItem('persistence');
        localStorage.removeItem('userEstim');


        getCustomersByAgent();
        if (localStorage.getItem('clientId')) {
            let openUrl = localStorage.getItem('printData');

            
            let clientId = localStorage.getItem('clientId');
            let ordId = localStorage.getItem('orderId');
            let message;

            if(localStorage.getItem('orderType')){
                message = 'Ordine modificato con successo.';
            }else{
                message = 'Preventivo inserito con successo.';
            }


            ademAlert.success({
                message: message
            });

            
            localStorage.removeItem('clientId');
            localStorage.removeItem('orderId');
            localStorage.removeItem('orderType');
        }
    }

    if (window.location.pathname == "/archive/ListDetails") {
        getCustomersByAgent();
        let customerName = getCustomerNameById($('#clientCode').val());
        $('#title').append(customerName);
    }
    setupNavigation();
    
    $('#myDatepicker').datepicker({
        format: 'dd/mm/yyyy'
    });
    $('#myDateEnd').datepicker({
        format: 'dd/mm/yyyy'
    });

    $('#search-filters').change(function (e) {

        if ($(this).val() == 1) {
            $('.radio-inline').fadeOut(150);
        } else {
            $('.radio-inline').fadeIn(150);
        }
    });
});
$(window).on('load', function () {
    if (window.location.pathname == "/Archive/ShowResultOrder") {
        getCustomersByAgent();
        let as400_id = $('#cliente').attr('data-target');
        let user = getCustomerWithParams(as400_id);
        
        $('#cliente').text(user["AS400_CODE"]);
    }

});